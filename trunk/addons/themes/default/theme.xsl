<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    version="1.0" 
    xmlns="http://www.w3.org/1999/xhtml" 
    xmlns:html="http://www.w3.org/1999/xhtml" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:exsl="http://exslt.org/common" 
    extension-element-prefixes="exsl" 
    exclude-result-prefixes="exsl xsl html">
    
    <xsl:output encoding="UTF-8" indent="no" method="xml" omit-xml-declaration="yes"/>
    <xsl:strip-space elements="*"/>
    <xsl:param name="view"/>
    <xsl:param name="base"/>
    <xsl:param name="folder"/>
    <xsl:param name="element"/>
    <xsl:param name="page" />
    <xsl:variable name="current_theme" select="/folderblog/settings/theme"/>
    <xsl:variable name="theme" select="/folderblog/settings/themes/theme[@dirname=$current_theme]"/>
    <xsl:variable name="base_with_slash">
        <xsl:choose>
            <xsl:when test="substring($base,-1) != '/'"><xsl:value-of select="$base"/>/</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$base"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <!-- store some settings in variables -->
    <xsl:variable name="thumb_maxsize"><xsl:value-of select="/folderblog/settings/thumb_maxsize"/></xsl:variable>
    <xsl:variable name="image_maxwidth"><xsl:value-of select="/folderblog/settings/image_maxwidth" /></xsl:variable>
    <xsl:variable name="crop_images"><xsl:value-of select="/folderblog/settings/crop_images" /></xsl:variable>
    <xsl:variable name="square_thumbs"><xsl:value-of select="/folderblog/settings/square_thumbs" /></xsl:variable>
    <xsl:variable name="show_thumbs"><xsl:value-of select="/folderblog/settings/show_thumbs" /></xsl:variable>
    
    <!-- first of all get all elements below the currently chosen folder and store it in a fragment -->
    <!-- so we can do sorting here once, and is applied anywhere -->
    <xsl:variable name="myElements">
        <xsl:for-each select="/folderblog//folder[path=$folder]//element">
            <xsl:sort data-type="number" order="descending" select="uploaded"/>
            <xsl:copy-of select="." />
        </xsl:for-each>
    </xsl:variable>
    
    <!-- also store all folders -->
    <xsl:variable name="myFolders">
        <xsl:for-each select="/folderblog//folder">
            <xsl:sort data-type="text" order="ascending" select="path"/>
            <xsl:copy-of select="." />
        </xsl:for-each>
    </xsl:variable>
    
    <!-- also store translations -->
    <xsl:variable name="translations">
        <xsl:for-each select="/folderblog/language/term">
            <xsl:sort data-type="text" order="ascending" select="@id"/>
            <xsl:copy-of select="." />
        </xsl:for-each>
    </xsl:variable>
    
    <xsl:template match="/">
        <xsl:choose>
            <xsl:when test="$view = 'index' and $element != '' and not(exsl:node-set($myElements)/element[slug=$element])">
                <xsl:apply-templates select="folderblog" mode="error404"/>
            </xsl:when>
            <xsl:when test="$view = 'index'">
                <xsl:apply-templates select="folderblog" mode="index"/>
            </xsl:when>
            <xsl:when test="$view = 'page'">
                <xsl:apply-templates select="folderblog" mode="index"/>
            </xsl:when>
            <xsl:when test="$view = 'archive'">
                <xsl:apply-templates select="folderblog" mode="archive"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="folderblog" mode="error404"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="folderblog" mode="error404">
        <html lang="{$language}">
            <xsl:call-template name="head">
                <xsl:with-param name="title"><xsl:apply-templates select="/folderblog/language/term[@id='ERROR']" /> 404</xsl:with-param>
                <xsl:with-param name="description" select="/folderblog/settings/title"/>
            </xsl:call-template>
            <body class="{$view} no-js" data-folder="{$folder}" data-view="{$view}">
                <div id="page">
                    <div id="content">
                        <xsl:call-template name="header"/>
                        <div class="subcolumns">
                            <article class="c66l">
                                <div class="box">
                                    <h2><xsl:apply-templates select="/folderblog/language/term[@id='ERROR']" /> 404</h2>
                                    <p><xsl:apply-templates select="/folderblog/language/term[@id='ERROR_MSG']" /><br/><br/></p>
                                    <p><xsl:apply-templates select="/folderblog/language/term[@id='ERROR_OPTS']" /><br/><br/></p>
                                    <ul>
                                        <li><xsl:apply-templates select="/folderblog/language/term[@id='GO_TO_THE']" /><xsl:text> </xsl:text><a href="{$base_with_slash}"><xsl:apply-templates select="/folderblog/language/term[@id='HOMEPAGE']" /></a><xsl:text> </xsl:text><xsl:apply-templates select="/folderblog/language/term[@id='OF']" /><xsl:text> </xsl:text><xsl:value-of select="/folderblog/settings/title" />.</li>
                                        <li><xsl:apply-templates select="/folderblog/language/term[@id='CHOOSE_ALBUM']" /></li>
                                        <li><xsl:apply-templates select="/folderblog/language/term[@id='TRY_SEARCH']" /></li>
                                    </ul>
                                </div>
                            </article>
                            <xsl:call-template name="sidebar" />
                            <hr class="clear"/>
                        </div>
                        <hr class="clear"/>
                        <xsl:call-template name="footer"/>
                    </div>
                </div>
                <xsl:call-template name="js"/>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="folderblog" mode="archive">
        <html lang="en">
            <xsl:call-template name="head">
                <xsl:with-param name="title"><xsl:apply-templates select="/folderblog/language/term[@id='ARCHIVE']" /> - <xsl:value-of select="/folderblog/settings/title"/></xsl:with-param>
                <xsl:with-param name="description" select="/folderblog/settings/title"/>
            </xsl:call-template>
            <body class="{$view} no-js" data-folder="{$folder}" data-view="{$view}">
                <div id="page">
                    <div id="content">
                        <xsl:call-template name="header"/>
                        <div class="subcolumns">
                            <article class="c66l">
                                <div class="box">
                                    <h2><xsl:apply-templates select="/folderblog/language/term[@id='ARCHIVE']" /></h2>
                                    <xsl:variable name="height">
                                        <xsl:choose>
                                            <xsl:when test="/folderblog/settings/square_thumbs"><xsl:value-of select="/folderblog/settings/thumb_maxsize"/></xsl:when>
                                            <xsl:otherwise>
                                                <xsl:value-of select="round((height * /folderblog/settings/image_maxwidth) div number(/folderblog/settings/thumb_maxsize))"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:variable>
                                    <ul class="archive">
                                        <xsl:for-each select="exsl:node-set($myElements)/element">
                                            <li>
                                                <a href="{$base_with_slash}e/{./slug}/">
                                                    <!-- @todo handle different types -->
                                                    <img src="{$base_with_slash}thumb/{$thumb_maxsize}x{$thumb_maxsize}{path}" width="{$thumb_maxsize}" height="{$height}">
                                                        <xsl:variable name="alt">
                                                            <xsl:if test="string-length(title)>0">
                                                                <xsl:value-of select="title"/>
                                                            </xsl:if>
                                                            <xsl:if test="string-length(title)>0 and string-length(description)>0"> - </xsl:if>
                                                            <xsl:if test="string-length(description)>0">
                                                                <xsl:value-of select="description"/>
                                                            </xsl:if>
                                                            <xsl:text> </xsl:text>
                                                        </xsl:variable>
                                                        <xsl:attribute name="alt">
                                                            <xsl:value-of select="$alt"/>
                                                        </xsl:attribute>
                                                        <xsl:attribute name="data-alt">
                                                            <xsl:value-of select="$alt"/>
                                                        </xsl:attribute>
                                                    </img>
                                                </a>
                                            </li>
                                        </xsl:for-each>
                                    </ul>
                                    <hr class="clear"/>
                                </div>
                            </article>
                            <xsl:call-template name="sidebar" />
                            <hr class="clear"/>
                        </div>
                        <hr class="clear"/>
                        <xsl:call-template name="footer"/>
                    </div>
                </div>
                <span data-folder="{$folder}" id="folder">
                    <xsl:text> </xsl:text>
                </span>
                <xsl:call-template name="js"/>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="folderblog" mode="index">
        <html lang="en">
            <xsl:call-template name="head">
                <xsl:with-param name="title">
                    <xsl:choose>
                        <xsl:when test="$element = '' and string-length($folder)>0">
                            <!-- get the folder title -->
                            <xsl:value-of select="concat(exsl:node-set($myFolders)/folder[path=$folder]/title, ' - ', /folderblog/settings/title)"/>
                        </xsl:when>
                        <xsl:when test="string-length($element)>0">
                            <xsl:choose>
                                <xsl:when test="exsl:node-set($myElements)/element[slug=$element]/title = ''"> <xsl:apply-templates select="/folderblog/language/term[@id='UNTITLED']" /></xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="exsl:node-set($myElements)/element[slug=$element]/title"/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:value-of select="concat(' - ', /folderblog/settings/title)"/>
                            <xsl:text> </xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text> </xsl:text>
                            <!-- should not apply -->
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:with-param>
                <xsl:with-param name="description" select="/folderblog/settings/title"/>
            </xsl:call-template>
            <body class="{$view} no-js" data-folder="{$folder}" data-view="{$view}">
                <div id="page">
                    <div id="content">
                        <xsl:call-template name="header"/>
                        <div class="subcolumns">
                            <article class="c66l">
                                <div class="box">
                                    <xsl:apply-templates select="//folder[path=$folder]" mode="normal" />
                                </div>
                            </article>
                            <xsl:call-template name="sidebar" />
                            <hr class="clear"/>
                        </div>
                        <xsl:call-template name="footer"/>
                    </div>
                </div>
                <span data-folder="{$folder}" id="folder">
                    <xsl:text> </xsl:text>
                </span>
                <xsl:call-template name="js"/>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="folder" mode="normal">
        <div id="all">
            <xsl:for-each select="exsl:node-set($myElements)/element">
                <xsl:variable name="match">
                    <xsl:choose>
                        <xsl:when test="$element = ''">
                            <xsl:choose>
                                <xsl:when test="position() = 1">1</xsl:when>
                                <xsl:otherwise/>
                            </xsl:choose>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:if test="slug = $element">1</xsl:if>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="$match = 1">
                        <div class="element{position()}" id="element{position()}">
                            <h2>
                                <xsl:choose>
                                    <xsl:when test="$element = ''">
                                        <xsl:choose>
                                            <xsl:when test="title = ''"><xsl:apply-templates select="exsl:node-set($translations)/term[@id='UNTITLED']" /></xsl:when>
                                            <xsl:otherwise>
                                                <xsl:value-of select="title"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:when>
                                    <xsl:when test="exsl:node-set($myElements)/element[slug=$element]/title = ''"><xsl:apply-templates select="exsl:node-set($translations)/term[@id='UNTITLED']" /></xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="exsl:node-set($myElements)/element[slug=$element]/title"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                                <xsl:text> </xsl:text>
                            </h2>
                            <xsl:variable name="mypos" select="position()"/>
                            <xsl:variable name="prevslug">
                                <xsl:value-of select="exsl:node-set($myElements)/element[number($mypos - 1)]/slug" />
                            </xsl:variable>
                            <xsl:variable name="nextslug">
                                <xsl:value-of select="exsl:node-set($myElements)/element[$mypos + 1]/slug" />
                            </xsl:variable>
                            <xsl:apply-templates mode="visible" select=".">
                                <xsl:with-param name="pos" select="position()"/>
                                <xsl:with-param name="nextslug" select="$nextslug"/>
                                <xsl:with-param name="prevslug" select="$prevslug"/>
                            </xsl:apply-templates>
                            <p class="description">
                                <xsl:value-of select="concat(description,' ')"/>
                            </p>
                        </div>
                    </xsl:when>
                    <xsl:otherwise>
                        <div id="element{position()}">
                            <xsl:apply-templates mode="hidden" select="." />
                        </div>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </div>
    </xsl:template>
    <xsl:template match="element[type='image']" mode="visible">
        <xsl:param name="nextslug"/>
        <xsl:param name="prevslug"/>
        <xsl:param name="pos"/>
        
        <xsl:variable name="height">
            <xsl:choose>
                <xsl:when test="/folderblog/settings/crop_images=1"><xsl:value-of select="/folderblog/settings/image_maxheight"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="round((height * $image_maxwidth) div width)"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="path">
            <xsl:value-of select="concat($base_with_slash,substring-after(path,'/'))"/>
        </xsl:variable>
        <div id="image-container" style="height:{$height}px;width:{$image_maxwidth}px">
            <a class="previous " href="{$base_with_slash}e/{$prevslug}/">
                <xsl:if test="$pos &lt;= 1">
                    <xsl:attribute name="class">previous hidden</xsl:attribute>
                </xsl:if>
                <em><xsl:apply-templates select="exsl:node-set($translations)/term[@id='PREVIOUS']" /></em>
            </a>
            <a class="next " href="{$base_with_slash}e/{$nextslug}/">
                <xsl:if test="$pos &gt; count(exsl:node-set($myElements)/element) -1">
                    <xsl:attribute name="class">next hidden</xsl:attribute>
                </xsl:if>
                <em>
                    <xsl:apply-templates select="exsl:node-set($translations)/term[@id='NEXT']" />
                </em>
            </a>
            <xsl:variable name="alt">
                <xsl:if test="string-length(title)>0">
                    <xsl:value-of select="title"/>
                </xsl:if>
                <xsl:if test="string-length(title)>0 and string-length(description)>0"> - </xsl:if>
                <xsl:if test="string-length(description)>0">
                    <xsl:value-of select="description"/>
                </xsl:if>
            </xsl:variable>
            <img src="{$base_with_slash}image/{$image_maxwidth}x{$height}{path}" width="{$image_maxwidth}" class="visible" data-slug="{slug}" height="{$height}" id="image">
                <xsl:attribute name="alt">
                    <xsl:value-of select="$alt"/>
                </xsl:attribute>
            </img>
            <xsl:variable name="title">
                <xsl:choose>
                    <xsl:when test="string-length(title)>0">
                        <xsl:value-of select="title"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates select="exsl:node-set($translations)/term[@id='UNTITLED']" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <span class="hidden" data-description="{description}" data-height="{$height}" data-path="{path}" data-slug="{slug}" data-src="{$base_with_slash}image/{$image_maxwidth}x{$height}{path}" data-title="{$title}" data-type="image">
                <xsl:attribute name="data-alt">
                    <xsl:value-of select="$alt"/>
                </xsl:attribute>
                <xsl:text> </xsl:text>
            </span>
            <div id="spinner"><xsl:text> </xsl:text></div>
        </div>
    </xsl:template>
    <xsl:template match="element[type='image']" mode="hidden">
        <xsl:param name="pos"/>
        <xsl:variable name="height">
            <xsl:choose>
                <xsl:when test="$crop_images=1">
                    <xsl:value-of select="$image_maxheight"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="round((height * $image_maxwidth) div width)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="path">
            <xsl:value-of select="concat($base_with_slash,substring-after(path,'/'))"/>
        </xsl:variable>
        <xsl:variable name="title">
            <xsl:choose>
                <xsl:when test="string-length(title)>0">
                    <xsl:value-of select="title"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates select="exsl:node-set($translations)/term[@id='UNTITLED']" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <span class="hidden" data-description="{description}" data-height="{$height}" data-path="{$path}" data-slug="{slug}" data-src="{$base_with_slash}image/{$image_maxwidth}x{$height}{$path}" data-title="{$title}" data-type="image">
            <xsl:attribute name="data-alt">
                <xsl:if test="string-length(title)>0">
                    <xsl:value-of select="title"/>
                </xsl:if>
                <xsl:if test="string-length(title)>0 and string-length(description)>0"> - </xsl:if>
                <xsl:if test="string-length(description)>0">
                    <xsl:value-of select="description"/>
                </xsl:if>
            </xsl:attribute>
            <xsl:text> </xsl:text>
        </span>
    </xsl:template>
    <xsl:template name="head">
        <xsl:param name="title"/>
        <xsl:param name="description"/>
        <head>
            <title>
                <xsl:value-of select="$title"/>
            </title>
            <meta content="{$description}" name="description"/>
            <meta content="{translate(translate($title,'-',''),' ',',')}" name="keywords"/>
            <meta charset="UTF-8"/>
            <meta content="Folderblog" name="generator"/>
            <link href="{$base_with_slash}addons/themes/{/folderblog/settings/theme}/img/favicon.ico" rel="icon" type="image/x-icon"/>
            <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{$base_with_slash}addons/themes/{/folderblog/settings/theme}/img/apple-touch-icon-144-precomposed.png" />
            <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{$base_with_slash}addons/themes/{/folderblog/settings/theme}/img/apple-touch-icon-114-precomposed.png" />
            <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{$base_with_slash}addons/themes/{/folderblog/settings/theme}/img/apple-touch-icon-72-precomposed.png" />
            <link rel="apple-touch-icon-precomposed" href="{$base_with_slash}addons/themes/{/folderblog/settings/theme}/img/apple-touch-icon-57-precomposed.png" />
            <script type="text/javascript">document.body.className = document.body.className.replace('no-js','js');</script>
            <xsl:call-template name="css"/>
        </head>
    </xsl:template>
    
    <xsl:template name="footer">
        <footer>
            <xsl:value-of select="/folderblog/settings/title" /><xsl:text> </xsl:text><xsl:apply-templates select="/folderblog/language/term[@id='POWERED_BY']" /><xsl:text> </xsl:text><a href="http://erikpoehler.com/folderblog/" title="Folderblog">Folderblog</a>
        </footer>
    </xsl:template>
    
    <xsl:template name="header">
        <header>
            <h1>
                <a href="{$base_with_slash}" title="{/folderblog/settings/title}">
                    <xsl:value-of select="/folderblog/settings/title"/>
                </a>
            </h1>
            <p><a>
                <xsl:attribute name="title">
                    <xsl:apply-templates select="/folderblog/language/term[@id='HOMEPAGE']" />
                </xsl:attribute>
                <xsl:attribute name="href">
                    <xsl:choose>
                        <xsl:when test="/folderblog/settings/home_link = '' or /folderblog/settings/home_link = 'http://'"><xsl:value-of select="$base_with_slash"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="/folderblog/settings/home_link"/></xsl:otherwise>
                    </xsl:choose>
                </xsl:attribute><xsl:apply-templates select="/folderblog/language/term[@id='HOMEPAGE']" /></a>
                <xsl:if test="/folderblog/settings/home_link != '' and /folderblog/settings/home_link != 'http://'"> ∙ <a href="{$base_with_slash}" title="Index">Index</a></xsl:if>
                 ∙ <a href="{$base_with_slash}archive/"><xsl:apply-templates select="/folderblog/language/term[@id='ARCHIVE']" /></a>
            </p>
        </header>
    </xsl:template>
    <xsl:template name="css">
        <xsl:choose>
            <xsl:when test="/folderblog/settings/minify_css='1'">
                <link media="all" rel="stylesheet" type="text/css">
                    <xsl:attribute name="href"><xsl:value-of select="$base_with_slash"/>lib/php/min/?f=<xsl:for-each select="$theme/styles/style"><xsl:value-of select="$base_with_slash"/>addons/themes/<xsl:value-of select="$current_theme"/>/<xsl:value-of select="."/><xsl:if test="position() &lt; count($theme/styles/style)">,</xsl:if></xsl:for-each></xsl:attribute>
                </link>
            </xsl:when>
            <xsl:otherwise>
                <xsl:for-each select="$theme/styles/style">
                    <link href="{$base_with_slash}addons/themes/{$current_theme}/{.}" media="all" rel="stylesheet" type="text/css"/>
                </xsl:for-each>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template name="js">
        <script defer="defer" type="text/javascript">var base='<xsl:value-of select="$base_with_slash"/>';</script>
        <xsl:choose>
            <xsl:when test="/folderblog/settings/minify_js='1'">
                <script defer="defer" type="text/javascript">
                    <xsl:attribute name="src">
                        <xsl:value-of select="$base_with_slash"/>lib/php/min/?f=<xsl:for-each select="$theme/scripts/script"><xsl:value-of select="$base_with_slash"/>addons/themes/default/<xsl:value-of select="."/><xsl:if test="position() &lt; count($theme/scripts/script)">,</xsl:if></xsl:for-each>
                    </xsl:attribute>
                    <xsl:comment/>
                </script>
            </xsl:when>
            <xsl:otherwise>
                <xsl:for-each select="$theme/scripts/script">
                    <script defer="defer" src="{$base_with_slash}addons/themes/{$current_theme}/{.}" type="text/javascript">
                        <xsl:comment>
                        </xsl:comment>
                    </script>
                </xsl:for-each>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:if test="string-length(/folderblog/settings/google_analytics)>0">
            <script type="text/javascript" async="" src="http://www.google-analytics.com/ga.js"><xsl:text> </xsl:text></script>
            <script defer="defer" type="text/javascript">var _gaq=_gaq||[];_gaq.push(["_setAccount","<xsl:value-of select="/folderblog/settings/google_analytics" />"]);_gaq.push(["_trackPageview"]);(function(){var b=document.createElement("script");b.type="text/javascript";b.async=true;b.src=("https:"==document.location.protocol?"https://ssl":"http://www")+".google-analytics.com/ga.js";var a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(b,a)})();</script>
        </xsl:if>
    </xsl:template>
    <xsl:template name="sidebar">
        <aside class="c33r">
            <!-- <xsl:value-of select="$page" />
            <xsl:value-of select="$element" /> -->
            <xsl:if test="$view != 'archive'">
                <div class="box latest">
                    <h2>
                        <xsl:value-of select="exsl:node-set($myFolders)/folder[path=$folder]/title"/>
                    </h2>
                    <nav>
                        <ul id="latest">
                            <xsl:variable name="height">
                                <xsl:choose>
                                    <xsl:when test="/folderblog/settings/square_thumbs">
                                        <xsl:value-of select="/folderblog/settings/thumb_maxsize"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="round((height * $image_maxwidth) div $thumb_maxsize)"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:variable>
                            <!-- Pagination config -->
                            <!-- <xsl:value-of select="$element" /><br /> -->
                            <xsl:variable name="activePos">
                                <xsl:for-each select="exsl:node-set($myElements)/element">
                                    <xsl:if test="slug=$element"><xsl:value-of select="position()" /></xsl:if>
                                </xsl:for-each>
                            </xsl:variable>
                            <xsl:variable name="pageNumber">
                                <xsl:choose>
                                    <xsl:when test="$element='' and $page = ''">1</xsl:when>
                                    <xsl:when test="$page != '' and $element=''"><xsl:value-of select="$page"/></xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="ceiling($activePos div number($show_thumbs))" />
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:variable>
                            <xsl:variable name="recordsPerPage" select="number(/folderblog/settings/show_thumbs)"/>
                            <xsl:variable name="numberOfRecords" select="count(exsl:node-set($myElements)/element)"/>
                            
                            <xsl:for-each select="exsl:node-set($myElements)/element">
                                <!-- @todo handle different types -->
                                <xsl:variable name="path">
                                    <xsl:value-of select="concat($base_with_slash,substring-after(path,'/'))"/>
                                </xsl:variable>
                                <li class="hidden">
                                    <xsl:attribute name="class">
                                        <xsl:choose>
                                            <xsl:when test="$element=''">
                                                <xsl:choose>
                                                    <xsl:when test="position() &lt;= $recordsPerPage * number($pageNumber -1) + $recordsPerPage">hallo visible</xsl:when>
                                                    <xsl:otherwise>hidden</xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:choose>
                                                    <xsl:when test="not(ceiling(position() div $recordsPerPage) = $pageNumber)">hidden</xsl:when>
                                                    <xsl:otherwise>visible</xsl:otherwise>
                                                </xsl:choose>
                                                <xsl:if test="slug=$element">active</xsl:if>
                                                <xsl:if test="$element='' and position() = 1">active</xsl:if>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                        <xsl:if test="slug=$element"> active</xsl:if>
                                        <xsl:if test="$element='' and position() = 1"> active</xsl:if>
                                    </xsl:attribute>
                                    <xsl:attribute name="data-position">
                                        <xsl:value-of select="position()"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="data-page">
                                        <xsl:value-of select="ceiling(position() div $recordsPerPage)"/>
                                    </xsl:attribute>
                                    
                                    <a href="{$base_with_slash}e/{slug}/">
                                        <img src="{$base_with_slash}thumb/{$thumb_maxsize}x{$thumb_maxsize}{path}" height="{$height}" width="{$thumb_maxsize}">
                                            <xsl:attribute name="alt">
                                                <xsl:if test="string-length(title)>0">
                                                    <xsl:value-of select="title"/>
                                                </xsl:if>
                                                <xsl:if test="string-length(title)>0 and string-length(description)>0"> - </xsl:if>
                                                <xsl:if test="string-length(description)>0">
                                                    <xsl:value-of select="description"/>
                                                </xsl:if>
                                            </xsl:attribute>
                                        </img>
                                    </a>
                                </li>
                                <!-- </xsl:if> -->
                            </xsl:for-each>
                            <hr class="clear"/>
                            <xsl:if test="$numberOfRecords &gt; $recordsPerPage">
                                <xsl:call-template name="pagination">
                                    <xsl:with-param name="pageNumber" select="$pageNumber"/>
                                    <xsl:with-param name="folder" select="$folder"/>
                                    <xsl:with-param name="recordsPerPage" select="$recordsPerPage"/>
                                    <xsl:with-param name="numberOfRecords" select="$numberOfRecords"/>
                                    <xsl:with-param name="element" select="$element"/>
                                    <xsl:with-param name="page" select="$page"/>
                                </xsl:call-template>
                            </xsl:if>
                        </ul>
                        <hr class="clear"/>
                    </nav>
                </div>
            </xsl:if>
            <xsl:if test="count(exsl:node-set($myFolders)/folder)>1">
                <div class="box">
                    <h2><xsl:apply-templates select="exsl:node-set($translations)/term[@id='ALBUMS']" /></h2>
                    <nav>
                        <ul id="albums">
                            <xsl:for-each select="/folderblog/folder">
                                <xsl:sort data-type="text" select="title"/>
                                <xsl:apply-templates mode="sidebar" select="."/>
                            </xsl:for-each>
                        </ul>
                    </nav>
                    <hr class="clear"/>
                </div>
            </xsl:if>
        </aside>
    </xsl:template>
    <xsl:template match="folder" mode="sidebar">
        <xsl:param name="base"/>
        <li>
            <span class="inner">
                <a href="{$base_with_slash}{substring-after(path,'/')}/" title="{title}"><xsl:if test="$folder = path"><xsl:attribute name="class">active</xsl:attribute></xsl:if><img alt="{title}" height="14" src="{$base_with_slash}lib/img/folder.png" style="vertical-align:-2px;margin-right:4px;" width="18"/><strong><xsl:value-of select="title"/></strong><xsl:text> </xsl:text>(<xsl:value-of select="count(.//element)"/>)</a>
            </span>
            <xsl:if test="*[folder]">
                <ol>
                    <xsl:apply-templates mode="sidebar" select="folders/folder"/>
                </ol>
            </xsl:if>
        </li>
    </xsl:template>
    
    <xsl:template name="pagination">
        <xsl:param name="folder"/>
        <xsl:param name="pageNumber"/>
        <xsl:param name="recordsPerPage"/>
        <xsl:param name="numberOfRecords"/>
        <xsl:param name="element"/>
        <xsl:param name="page"/>
        <div class="pagination" id="pagination">
            <div class="wrapper">
                <xsl:call-template name="for.loop">
                    <xsl:with-param name="i" select="$pageNumber - 3"/>
                    <xsl:with-param name="page" select="$pageNumber"/>
                    <xsl:with-param name="count" select="ceiling(count(exsl:node-set($myElements)/element) div $recordsPerPage)"/>
                    <xsl:with-param name="linklimit">12</xsl:with-param>
                    <xsl:with-param name="element" select="$element"/>
                </xsl:call-template>
            </div>
        </div>
    </xsl:template>
    
    <xsl:template name="for.loop">
        <xsl:param name="i"/>
        <xsl:param name="count"/>
        <xsl:param name="page"/>
        <xsl:param name="linklimit"/>
        <xsl:param name="element"/>
        <xsl:if test="$i &lt;= $count and $i > 0">
            <span>
                <xsl:if test="$page != $i">
                    <a data-page="{$i}" href="{concat($base_with_slash,'e/',$element,'/page/',$i,'/')}">
                        <xsl:attribute name="href"><xsl:value-of select="$base_with_slash"/><xsl:if test="$folder !='' and $folder != '/folders'"><xsl:value-of select="substring-after($folder,'/')"/>/</xsl:if><xsl:if test="$element !=''"><xsl:value-of select="concat('e/',$element,'/')"/></xsl:if>page/<xsl:value-of select="$i"/>/ </xsl:attribute>
                        <xsl:value-of select="$i"/>
                    </a>
                </xsl:if>
                <xsl:if test="$page = $i">
                    <a class="active" data-page="{$i}" href="{concat($base_with_slash,'page/',$i,'/')}">
                        <xsl:attribute name="href"><xsl:value-of select="$base_with_slash"/><xsl:if test="$folder !='' and $folder != '/folders'"><xsl:value-of select="substring-after($folder,'/')"/>/</xsl:if><xsl:if test="$element !=''"><xsl:value-of select="concat('e/',$element,'/')"/></xsl:if>page/<xsl:value-of select="$i"/>/ </xsl:attribute>
                        <xsl:value-of select="$i"/>
                    </a>
                </xsl:if>
            </span>
        </xsl:if>
        <xsl:if test="$i &lt;= $count and $linklimit &gt; 0">
            <xsl:call-template name="for.loop">
                <xsl:with-param name="i">
                    <xsl:value-of select="$i + 1"/>
                </xsl:with-param>
                <xsl:with-param name="count">
                    <xsl:value-of select="$count"/>
                </xsl:with-param>
                <xsl:with-param name="page">
                    <xsl:value-of select="$page"/>
                </xsl:with-param>
                <xsl:with-param name="linklimit">
                    <xsl:value-of select="$linklimit -1"/>
                </xsl:with-param>
                <xsl:with-param name="element" select="$element"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="term">
        <xsl:value-of select="." />
    </xsl:template>
</xsl:stylesheet>