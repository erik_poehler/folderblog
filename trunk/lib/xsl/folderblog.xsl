<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" exclude-result-prefixes="xsl xsi html xsd" xmlns="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:html="http://www.w3.org/1999/xhtml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <xsl:output method="xml" indent="no" encoding="UTF-8" omit-xml-declaration="yes" />
    <xsl:include href="./common.xsl"/>
    <xsl:strip-space elements="*" />
    <xsl:param name="view" />
    <xsl:param name="action" />
    <xsl:param name="element" />
    <xsl:param name="loggedinUser" />
    <xsl:param name="base" />
    <xsl:param name="debug" select="'0'" />
    <xsl:variable name="base_with_slash">
        <xsl:choose>
            <xsl:when test="substring($base,-1) != '/'"><xsl:value-of select="$base" />/</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$base" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

    <xsl:template match="/">
        <html lang="en">
        <head>
            <title>
                <xsl:choose>
                    <xsl:when test="$view = 'admin'">Folderblog <xsl:value-of select="/folderblog/@version" /></xsl:when>
                    <xsl:when test="$view='image' or $view='audio' or $view='video'"><xsl:value-of select="/folderblog/settings/title" /> | <xsl:value-of select="//element[file='$element']/title" /></xsl:when>
                    <xsl:otherwise><xsl:value-of select="/folderblog/settings/title" /><xsl:text> </xsl:text></xsl:otherwise>
                    <xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
                </xsl:choose>
            </title>
            <link href="{$base_with_slash}lib/img/favicon.ico" rel="icon" type="image/x-icon" />
            <link rel="stylesheet" type="text/css" href="{$base_with_slash}lib/css/styles.css" />
            <link rel="stylesheet" type="text/css" href="{$base_with_slash}lib/css/fileuploader.css" />
        </head>
        <body class="{$view}">
            <xsl:if test="$debug='1'">
            <pre id="debug">
            User: <xsl:value-of select="$loggedinUser" />
            Base: <xsl:value-of select="$base" />
            View: <xsl:value-of select="$view" />
            Action: <xsl:value-of select="$action" />
            Element: <xsl:value-of select="$element" />
            </pre>
            </xsl:if>
            <xsl:choose>
                <!-- <xsl:when test="$view='' or $view='index'"><xsl:call-template name="index" /></xsl:when> -->
                <xsl:when test="$view='login'"><xsl:call-template name="login" /></xsl:when>
                <xsl:when test="$view='admin'"><xsl:call-template name="admin" /></xsl:when>
                <xsl:when test="$view='setup'"><xsl:call-template name="setup" /></xsl:when>
                <xsl:otherwise><xsl:call-template name="error404" /></xsl:otherwise>
            </xsl:choose>
            <span class="credits"><a href="http://erikpoehler.com/folderblog/">Folderblog <xsl:value-of select="/folderblog/@version" /></a></span>
            <script type="text/javascript"><xsl:comment>
                var base = "<xsl:value-of select="$base_with_slash" />";
            </xsl:comment></script>
            <script type="text/javascript" src="{$base_with_slash}lib/js/jquery.js"><xsl:comment></xsl:comment></script>
            <script type="text/javascript" src="{$base_with_slash}lib/js/jquery.field.js"><xsl:comment></xsl:comment></script>
            <script type="text/javascript" src="{$base_with_slash}lib/js/jquery.alphanumeric.js"><xsl:comment></xsl:comment></script>
            <script type="text/javascript" src="{$base_with_slash}lib/js/folderblog.js"><xsl:comment></xsl:comment></script>
        </body>
        </html>
    </xsl:template>
    
    <xsl:template name="login">
        <form id="login" action="{$base_with_slash}{$view}/" method="post" class="box">
            <h1>folderblog <span class="light">login</span></h1>
            <div>
                <xsl:variable name="username">
                    <xsl:apply-templates select="/folderblog/language/term[@id='USERNAME']" />
                </xsl:variable>
                <xsl:variable name="password">
                    <xsl:apply-templates select="/folderblog/language/term[@id='PASSWORD']" />
                </xsl:variable>
                <label class="screenreader" for="username"><xsl:value-of select="$username" /></label>
                <input type="text" autocomplete="off" id="username" name="username" placeholder="{$username}" maxlength="200" length="20" class="text username" />
                <label class="screenreader" for="password"><xsl:value-of select="$password" /></label>
                <input type="text" autocomplete="off" id="password" name="password" placeholder="{$password}" maxlength="200" length="20" class="password" />
                <input type="submit" class="submit">
                    <xsl:attribute name="value">
                        <xsl:apply-templates select="/folderblog/language/term[@id='LOGIN']" />
                    </xsl:attribute>
                </input>
            </div>
            <br />
            <a href="{$base_with_slash}"><xsl:apply-templates select="/folderblog/language/term[@id='BACK_TO_HOMEPAGE']" /></a>
        </form>
    </xsl:template>
    
    <xsl:template name="setup">
        <form id="setup" action="{$base_with_slash}" method="post" class="box">
            <h1>folderblog <span class="light">setup</span></h1>
            <p><strong>Welcome to Folderblog 4.0.</strong><br />You are seeing this page, because you are opening your Folderblog installation for the first time. You will be able to change the password later or add new users.
            <br />Please choose a title for your Folderblog and set a password for Folderblog's main user <strong><samp>superuser</samp></strong> now.</p>
            
            <div>
                <label for="title"><strong>Choose a title</strong></label><br />
                <input type="text" autocomplete="off" id="title" name="title" maxlength="200" length="20" class="text title" value="" /><br />
                
                <label for="superuser"><strong>Password for superuser</strong></label><br />
                <input type="text" autocomplete="off" id="superuser" name="superuser" maxlength="200" length="20" class="text superuser" value="" /><br />
                
                <label for="admin"><strong>Repeat password for superuser</strong></label><br />
                <input type="text" id="repeat" name="repeat" maxlength="200" length="20" class="text superuser" value="" /><br /><br />
                <input type="hidden" name="action" value="setup" />
                
                <p>You're settings will be saved and you'll be redirected to the login screen where you can sign in using username superuser and the password you just chose.</p>
                <input type="submit" value="Save" class="submit" />
            </div>
        </form>
    </xsl:template>
    
    <xsl:template name="error404">
        <span>I am the error page.</span>
    </xsl:template>
    
    <xsl:template name="editfolder">
        <div class="folderdetails">
            <div class="inner">
                <a href="#" class="close">Cancel</a>
                <form action="{$base_with_slash}{$view}/#folders" method="post" id="folder{position()}">
                    <ul>
                        <li>
                            <label for="title">Title</label>
                            <input type="text" class="text" name="title" id="title" value="{title}" />
                        </li>
                        <li>
                            <label for="description">Description</label>
                            <textarea name="description" id="description"><xsl:value-of select="description" /><xsl:text> </xsl:text></textarea>
                        </li>
                        <li>
                            <input type="hidden" name="action" value="editfolder" />
                            <input type="hidden" name="path" value="{path}" />
                            <input type="submit" class="submit" value="Save" />
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </xsl:template>
    
    <xsl:template match="folder">
        <li>
            <span class="inner">
            <img src="{$base_with_slash}lib/img/folder.png" alt="Folder" style="vertical-align:-2px;margin-right:4px;" /><strong><samp><xsl:value-of select="path" /></samp><xsl:text> </xsl:text></strong><xsl:value-of select="title" /> <xsl:text> </xsl:text>(<xsl:value-of select="count(elements/element)" /> Elements)<xsl:text> </xsl:text><a class="edit" href="#edit">Edit</a><br />
            </span>
            <xsl:call-template name="editfolder" />
            <xsl:if test="*[folder]">
                <ol>
                    <xsl:apply-templates select="folders/folder" />
                </ol>
            </xsl:if>
        </li>
    </xsl:template>
    
    <xsl:template name="admin">
        <div class="box">
            <h1>folderblog <span class="light">admin</span></h1>
            <span class="warn">Please enable Javascript in your Browser to make full use of Foderblogs Functionality. Non-Javascript use is on your own risk.</span>
            <div class="user"><xsl:if test="$loggedinUser!=''"><xsl:apply-templates select="/folderblog/language/term[@id='WELCOME']" /><xsl:text> </xsl:text> <xsl:value-of select="/folderblog/users/user[@name=$loggedinUser]/@nicename" /></xsl:if>! <a href="{$base_with_slash}logout/"><xsl:apply-templates select="/folderblog/language/term[@id='LOGOUT']" /></a> | <a href="{$base_with_slash}" target="_blank"><xsl:apply-templates select="/folderblog/language/term[@id='GO_TO_WEBSITE']" /></a></div>
            <div class="tabs">
                <ul class="tabs">
                    <li><a href="#folders"><xsl:apply-templates select="/folderblog/language/term[@id='FOLDERS']" /></a></li>
                    <li><a href="#browser"><xsl:apply-templates select="/folderblog/language/term[@id='BROWSER']" /></a></li>
                    <li><a href="#media"><xsl:apply-templates select="/folderblog/language/term[@id='UPLOADER']" /></a></li>
                    <!-- <li><a href="#comments"><xsl:apply-templates select="/folderblog/language/term[@id='COMMENTS']" /></a></li>
                    <li><a href="#tags"><xsl:apply-templates select="/folderblog/language/term[@id='TAGS']" /></a></li> -->
                    <li><a href="#settings"><xsl:apply-templates select="/folderblog/language/term[@id='SETTINGS']" /></a></li>
                    <li><a href="#users"><xsl:apply-templates select="/folderblog/language/term[@id='USERS']" /></a></li>
                </ul>
            </div>
            <div class="panes">
                <div class="folders">
                    <h2><xsl:apply-templates select="/folderblog/language/term[@id='FOLDERS_INTRO']" /></h2>
                    <p><xsl:apply-templates select="/folderblog/language/term[@id='FOLDERS_EXPLANATION']" /> <xsl:text> </xsl:text><a href="#media"><xsl:apply-templates select="/folderblog/language/term[@id='HERE']" /></a>.</p>
                    <h2><xsl:apply-templates select="/folderblog/language/term[@id='EDIT_FOLDERS']" /></h2>
                    <ol class="folders">
                        <xsl:for-each select="/folderblog/folder">
                            <xsl:sort select="path"/>
                            <xsl:apply-templates select="." />
                        </xsl:for-each>
                    </ol>
                    <hr class="clear" style="margin-top:2em;"/>
                    <div style="width:50%;float:left;">
                        <h2><xsl:apply-templates select="/folderblog/language/term[@id='CREATE_NEW_FOLDER']" /></h2>
                        <form action="{$base_with_slash}{$view}/#folders" method="post" id="addfolder">
                            <ol>
                                <li>
                                    <label for="parent"><xsl:apply-templates select="/folderblog/language/term[@id='CREATE_BELOW']" /></label>
                                    <select name="parent" id="parent">
                                        <option value="/folders">/folders (<xsl:apply-templates select="/folderblog/language/term[@id='DEFAULT']" />)</option>
                                        <xsl:for-each select="/folderblog/folder/folders//folder">
                                            <xsl:sort select="path"/>
                                                <option value="{path}"><xsl:value-of select="path" /></option>
                                        </xsl:for-each>
                                    </select>
                                    <label for="title"><xsl:apply-templates select="/folderblog/language/term[@id='TITLE']" /></label>
                                    <input type="text" autocomplete="off" class="text" name="title" id="title" value="" />
                                    <label><xsl:apply-templates select="/folderblog/language/term[@id='PHYSICAL_NAME']" /></label>
                                    <input type="text" class="text" id="physical_path" value="/folders" readonly="readonly" />
                                    <br />
                                    <label for="description"><xsl:apply-templates select="/folderblog/language/term[@id='DESCRIPTION']" /></label>
                                    <textarea name="description" id="description"><xsl:text> </xsl:text></textarea>
                                </li>
                            </ol>
                            <input type="hidden" name="action" value="addfolder" />
                            <input type="submit" class="big submit">
                                <xsl:attribute name="value">
                                    <xsl:apply-templates select="/folderblog/language/term[@id='ADD_FOLDER']" />
                                </xsl:attribute>
                            </input>
                        </form>
                    </div>
                    <div style="width:50%;margin-left:50%;">
                        <h2><xsl:apply-templates select="/folderblog/language/term[@id='DELETE_FOLDER']" /></h2>
                        <form id="deletefolder" method="post" action="{$base_with_slash}{$view}/#folders">
                            <p><xsl:apply-templates select="/folderblog/language/term[@id='DELETE_FOLDER_EXPLANATION']" /></p>
                            <ol>
                                <li>
                                    <label for="path"><xsl:apply-templates select="/folderblog/language/term[@id='CHOOSE_FOLDER_FOR_DELETION']" /></label>
                                    <select name="path" id="path">
                                        <option value=""><xsl:apply-templates select="/folderblog/language/term[@id='CHOOSE_FOLDER']" /></option>
                                        <xsl:for-each select="/folderblog/folder/folders//folder">
                                            <xsl:sort select="path"/>
                                            <option value="{path}"><xsl:value-of select="concat(path,' - ',count(./elements//element),' ')" /> <xsl:apply-templates select="/folderblog/language/term[@id='ELEMENTS']" /></option>
                                        </xsl:for-each>
                                    </select>
                                </li>
                            </ol>
                            <input type="hidden" name="action" value="deletefolder" />
                            <input type="submit" class="submit">
                                <xsl:attribute name="onclick">return confirm(<xsl:apply-templates select="/folderblog/language/term[@id='CONFIRM_DELETE']" />);</xsl:attribute>
                                <xsl:attribute name="value">
                                    <xsl:apply-templates select="/folderblog/language/term[@id='DELETE']" />
                                </xsl:attribute>
                            </input>
                        </form>
                    </div>
                    <hr class="clear" />
                </div>
                <div class="browser">
                    <h2><xsl:apply-templates select="/folderblog/language/term[@id='MEDIA_BROWSER']" /></h2>
                    <div class="content" id="mediabrowser"><xsl:text> </xsl:text></div>
                </div>
                <div class="media">
                    <h2><xsl:apply-templates select="/folderblog/language/term[@id='UPLOAD_NEW']" /></h2>
                    <p><xsl:apply-templates select="/folderblog/language/term[@id='UPLOAD_INTRO']" /></p>
                    <form id="addmedia" action="{$base_with_slash}{$view}/#media" method="post" enctype="multipart/form-data">
                        <ol>
                            <li>
                                <label for="folder"><xsl:apply-templates select="/folderblog/language/term[@id='CHOOSE_UPLOAD_DIR']" />:</label>
                                <select name="path" id="path">
                                    <xsl:for-each select="/folderblog//folder">
                                        <xsl:sort select="path"/>
                                        <option value="{path}"><xsl:value-of select="path" /></option>
                                    </xsl:for-each>
                                </select>
                            </li>
                            <li>
                                <label for="uploadmedia"><xsl:apply-templates select="/folderblog/language/term[@id='CHOOSE_FILES']" /></label>
                                <div id="uploadmedia"><xsl:text> </xsl:text></div>
                                <input class="file" type="file" style="display:none;" />
                            </li>
                            <li>
                                <input type="hidden" name="action" value="addmedia" />
                                <input id="upload-button" type="submit" class="submit">
                                    <xsl:attribute name="value">
                                        <xsl:apply-templates select="/folderblog/language/term[@id='UPLOAD_FILES']" />
                                    </xsl:attribute>
                                </input>
                            </li>
                        </ol>
                    </form>
                </div>
                <!-- 
                <div class="comments">
                    <hr class="clear" />
                    <h2>Manage Comments</h2>
                    <p>Not implemented yet. Remember this is still beta...</p>
                </div>
                <div class="tags">
                    <hr class="clear" />
                    <h2>Manage Tags</h2>
                    <p>Not implemented yet. Remember this is still beta...</p>
                </div>
                 -->
                <div class="settings">
                    <hr class="clear" />
                    <form id="" method="post" action="{$base_with_slash}{$view}/#settings">
                        <div style="float:left; width:50%;">
                            <h2><xsl:apply-templates select="/folderblog/language/term[@id='GENERAL_SETTINGS']" /></h2>
                            <ol style="float:left;">
                                <li>
                                    <label for="title"><xsl:apply-templates select="/folderblog/language/term[@id='TITLE']" /></label>
                                    <input type="text" autocomplete="off" id="title" name="title" maxlength="300" length="20" class="text username" value="{/folderblog/settings/title}" />
                                </li>
                                <!-- 
                                <li>
                                    <label for="comment_email">><xsl:apply-templates select="/folderblog/language/term[@id='COMMENT_NOTFICIATION_EMAIL']" /></label>
                                    <input type="text" autocomplete="off" id="comment_email" name="comment_email" maxlength="300" length="20" class="text username" value="{/folderblog/settings/comment_email}" />
                                </li>
                                 -->
                                <li>
                                    <label for="home_link"><xsl:apply-templates select="/folderblog/language/term[@id='CUSTOMIZE_HOME']" /></label>
                                    <input type="text" autocomplete="off" id="home_link" name="home_link" maxlength="300" length="20" class="text home_link" value="http://">
                                        <xsl:attribute name="value">
                                            <xsl:choose>
                                                <xsl:when test="string-length(/folderblog/settings/home_link) > 0 and not(/folderblog/settings/home_link='') and not(/folderblog/settings/home_link='http://')">
                                                    <xsl:value-of select="/folderblog/settings/home_link" />
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:attribute>
                                        <xsl:attribute name="placeholder">
                                            <xsl:choose>
                                                <xsl:when test="string-length(/folderblog/settings/home_link) > 0 and not(/folderblog/settings/home_link='') and not(/folderblog/settings/home_link='http://')"></xsl:when>
                                                <xsl:otherwise>http://</xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:attribute>
                                    </input>
                                    <p><small style="display:block;font-size:85%;width:250px;"><xsl:apply-templates select="/folderblog/language/term[@id='CUSTOMIZE_HOME_HELP']" /></small></p>
                                </li>
                                
                                <!-- 
                                <li>
                                    <input type="checkbox" name="enable_comments" id="enable_comments" value="1" class="checkbox">
                                        <xsl:if test="/folderblog/settings/enable_comments='1'">
                                            <xsl:attribute name="checked">checked</xsl:attribute>
                                        </xsl:if>
                                    </input>
                                    <label for="enable_comments" class="inline"><xsl:apply-templates select="/folderblog/language/term[@id='ACTIVATE_COMMENTS']" /></label>
                                </li>
                                
                                <li>
                                    <input type="checkbox" id="require_captcha" name="require_captcha" class="checkbox" value="1">
                                        <xsl:if test="/folderblog/settings/require_captcha='1'">
                                            <xsl:attribute name="checked">checked</xsl:attribute>
                                        </xsl:if>
                                    </input>
                                    <label for="require_captcha" class="inline"><xsl:apply-templates select="/folderblog/language/term[@id='REQUIRE_CAPTCHA']" /></label>
                                </li>
                                
                                <li>
                                    <input type="checkbox" id="moderate_comments" name="moderate_comments" class="checkbox" value="1">
                                        <xsl:if test="/folderblog/settings/moderate_comments='1'">
                                            <xsl:attribute name="checked">checked</xsl:attribute>
                                        </xsl:if>
                                    </input>
                                    <label for="moderate_comments" class="inline"><xsl:apply-templates select="/folderblog/language/term[@id='MODERATE_COMMENTS']" /></label>
                                </li>
                                 -->
                                <li>
                                    <label for="theme" class="inline"><xsl:apply-templates select="/folderblog/language/term[@id='ACTIVE_THEME']" /></label>
                                    <select id="theme" name="theme">
                                        <xsl:for-each select="/folderblog/settings/themes/theme">
                                            <option value="{@dirname}">
                                            <xsl:if test="/folderblog/settings/theme=@dirname">
                                                <xsl:attribute name="selected">selected</xsl:attribute>
                                            </xsl:if><xsl:value-of select="name" /> v<xsl:value-of select="version" /></option>
                                        </xsl:for-each>
                                    </select>
                                </li>
                                
                                <li>
                                    <label for="preferred_language" class="inline"><xsl:apply-templates select="/folderblog/language/term[@id='PREFERRED_LANGUAGE']" /></label>
                                    <select id="preferred_language" name="preferred_language">
                                        <option value="browser"><xsl:if test="/folderblog/settings/preferred_language='browser'">
                                                <xsl:attribute name="selected">selected</xsl:attribute>
                                            </xsl:if><xsl:apply-templates select="/folderblog/language/term[@id='DETECT_BROWSER_LANGUAGE']" /></option>
                                        <option value="en"><xsl:if test="/folderblog/settings/preferred_language='en'">
                                                <xsl:attribute name="selected">selected</xsl:attribute>
                                            </xsl:if>English</option>
                                        <option value="de"><xsl:if test="/folderblog/settings/preferred_language='de'">
                                                <xsl:attribute name="selected">selected</xsl:attribute>
                                            </xsl:if>Deutsch</option>
                                        <option value="es"><xsl:if test="/folderblog/settings/preferred_language='es'">
                                                <xsl:attribute name="selected">selected</xsl:attribute>
                                            </xsl:if>Español</option>
                                    </select>
                                </li>
                                
                                <li>
                                    <label for="default_language" class="inline"><xsl:apply-templates select="/folderblog/language/term[@id='DEFAULT_LANGUAGE']" /></label>
                                    <select id="default_language" name="default_language">
                                        <option value="en"><xsl:if test="/folderblog/settings/default_language='en'">
                                                <xsl:attribute name="selected">selected</xsl:attribute>
                                            </xsl:if>English</option>
                                        <option value="de"><xsl:if test="/folderblog/settings/default_language='de'">
                                                <xsl:attribute name="selected">selected</xsl:attribute>
                                            </xsl:if>Deutsch</option>
                                    </select>
                                </li>
                                
                                <li>
                                    <input type="checkbox" id="minify_css" name="minify_css" class="checkbox" value="1">
                                        <xsl:if test="/folderblog/settings/minify_css='1'">
                                            <xsl:attribute name="checked">checked</xsl:attribute>
                                        </xsl:if>
                                    </input>
                                    <label for="minify_css" class="inline">Minify Stylesheets</label>
                                </li>
                                
                                <li>
                                    <input type="checkbox" id="minify_js" name="minify_js" class="checkbox" value="1">
                                        <xsl:if test="/folderblog/settings/minify_js='1'">
                                            <xsl:attribute name="checked">checked</xsl:attribute>
                                        </xsl:if>
                                    </input>
                                    <label for="minify_js" class="inline">Minify Javascripts</label>
                                </li>
                                
                                <li>
                                    <label for="google_analytics">Google Analytics</label>
                                    <input type="text" id="google_analytics" name="google_analytics" class="text" value="{/folderblog/settings/google_analytics}" placeholder="UA-" />
                                </li>
                                <!-- 
                                <li>
                                    <input type="checkbox" id="profile" name="profile" class="checkbox" value="1">
                                        <xsl:if test="/folderblog/settings/profile='1'">
                                            <xsl:attribute name="checked">checked</xsl:attribute>
                                        </xsl:if>
                                    </input>
                                    <label for="profile" class="inline">Show profiling/debugging info</label>
                                </li>
                                 -->
                            </ol>
                            <hr class="clear" />
                        </div>
                        <div style="margin-left:50%;">
                            <h2><xsl:apply-templates select="/folderblog/language/term[@id='APPEARANCE']" /></h2>
                            <ol style="float:left;">
                                <li>
                                    <label for="sorting"><xsl:apply-templates select="/folderblog/language/term[@id='SORT_ELEMENTS']" /></label>
                                    <select name="sorting" id="sorting" class="select">
                                        <option value="no">
                                            <xsl:if test="/folderblog/settings/sorting = 'no'">
                                                <xsl:attribute name="selected">selected</xsl:attribute>
                                            </xsl:if>
                                            <xsl:apply-templates select="/folderblog/language/term[@id='NOSORT']" />
                                        </option>
                                        <option value="uploaded">
                                            <xsl:if test="/folderblog/settings/sorting = 'uploaded'">
                                                <xsl:attribute name="selected">selected</xsl:attribute>
                                            </xsl:if>
                                            <xsl:apply-templates select="/folderblog/language/term[@id='UPLOADED']" />
                                        </option>
                                        <option value="filename">
                                            <xsl:if test="/folderblog/settings/sorting = 'filename'">
                                                <xsl:attribute name="selected">selected</xsl:attribute>
                                            </xsl:if>
                                            <xsl:apply-templates select="/folderblog/language/term[@id='FILENAME']" />
                                        </option>
                                    </select>
                                </li>
                                
                                <li>
                                    <label for="order"><xsl:apply-templates select="/folderblog/language/term[@id='SORT_ORDER']" /></label>
                                    <select name="order" id="order" class="select">
                                        <option value="descending">
                                            <xsl:if test="/folderblog/settings/order = 'descending'">
                                                <xsl:attribute name="selected">selected</xsl:attribute>
                                            </xsl:if>
                                            <xsl:apply-templates select="/folderblog/language/term[@id='DESCENDING']" /><xsl:text> </xsl:text>
                                        </option>
                                        <option value="ascending">
                                            <xsl:if test="/folderblog/settings/order = 'ascending'">
                                                <xsl:attribute name="selected">selected</xsl:attribute>
                                            </xsl:if>
                                            <xsl:apply-templates select="/folderblog/language/term[@id='ASCENDING']" /><xsl:text> </xsl:text>
                                        </option>
                                    </select>
                                </li>
                                 
                                <li>
                                    <label for="thumb_maxsize"><xsl:apply-templates select="/folderblog/language/term[@id='THUMB_MAXSIZE']" /><xsl:text> </xsl:text></label>
                                    <input type="text" autocomplete="off" id="thumb_maxsize" name="thumb_maxsize" maxlength="300" length="20" class="text" value="{/folderblog/settings/thumb_maxsize}" /> px
                                </li>
                                
                                <li>
                                    <label for="show_thumbs" class="inline"><xsl:apply-templates select="/folderblog/language/term[@id='THUMB_COUNT']" /></label>
                                    <select name="show_thumbs" id="show_thumbs" class="select">
                                        <xsl:call-template name="options">
                                            <xsl:with-param name="count" select="20"/>
                                            <xsl:with-param name="start" select="1"/>
                                        </xsl:call-template>
                                    </select>
                                </li>
                                
                                <li>
                                    <input type="checkbox" id="square_thumbs" name="square_thumbs" class="checkbox" value="1">
                                        <xsl:if test="/folderblog/settings/square_thumbs='1'">
                                            <xsl:attribute name="checked">checked</xsl:attribute>
                                        </xsl:if>
                                    </input>
                                    <label for="square_thumbs" class="inline"><xsl:apply-templates select="/folderblog/language/term[@id='SQUARE_THUMBS']" /></label>
                                </li>
                                
                                <li>
                                    <label for="image_maxwidth"><xsl:apply-templates select="/folderblog/language/term[@id='IMAGE_MAX_WIDTH']" /><xsl:text> </xsl:text></label>
                                    <input type="text" autocomplete="off" id="image_maxwidth" name="image_maxwidth" maxlength="300" length="20" class="text" value="{/folderblog/settings/image_maxwidth}" /> px
                                </li>
                                
                                <li>
                                    <label for="image_maxheight"><xsl:apply-templates select="/folderblog/language/term[@id='IMAGE_MAX_HEIGHT']" /><xsl:text> </xsl:text></label>
                                    <input type="text" autocomplete="off" id="image_maxheight" name="image_maxheight" maxlength="300" length="20" class="text" value="{/folderblog/settings/image_maxheight}" /> px
                                </li>
                                
                                <li>
                                    <input type="checkbox" id="crop_images" name="crop_images" class="checkbox" value="1">
                                        <xsl:if test="/folderblog/settings/crop_images='1'">
                                            <xsl:attribute name="checked">checked</xsl:attribute>
                                        </xsl:if>
                                    </input>
                                    <label for="crop_images" class="inline"><xsl:apply-templates select="/folderblog/language/term[@id='CROP_IMAGES']" /></label>
                                </li>
                            </ol>
                        </div>
                        
                        <hr class="clear" />
                        <br />
                        <input type="hidden" name="action" value="settings" />
                        <input class="submit big" type="submit">
                            <xsl:attribute name="value">
                                <xsl:apply-templates select="/folderblog/language/term[@id='SAVE_CHANGES']" />
                            </xsl:attribute>
                        </input>
                    </form>
                </div>
                <div class="users">
                    <hr class="clear" />
                    <h2><xsl:apply-templates select="/folderblog/language/term[@id='MANAGE_USERS']" /></h2>
                    <form id="users" method="post" action="{$base_with_slash}{$view}/#users">
                        <ol>
                            <xsl:for-each select="/folderblog/users/user">
                                <li>
                                    
                                    <dl>
                                        <dt>
                                            <label for="username"><xsl:apply-templates select="/folderblog/language/term[@id='USER']" /></label>
                                            <input type="text" readonly="readonly" class="text readonly" id="username" value="{@name}" />
                                        </dt>
                                        <dd>
                                            <label for="nicename{position()}"><xsl:apply-templates select="/folderblog/language/term[@id='DISPLAY_NAME']" /></label>
                                            <input type="text" class="text" name="user[{position()}][nicename]" id="nicename{position()}" value="{@nicename}">
                                                <xsl:if test="@name='superuser' and $loggedinUser != 'superuser'">
                                                    <xsl:attribute name="readonly">readonly</xsl:attribute>
                                                </xsl:if>
                                            </input>
                                        </dd>
                                        <dd>
                                            <label for="user[][new_password]"><xsl:apply-templates select="/folderblog/language/term[@id='NEW_PASS']" />:</label>
                                            <input type="text" class="text" name="user[{position()}][new_password]" id="new_password{position()}" value="">
                                                <xsl:if test="@name='superuser' and $loggedinUser != 'superuser'">
                                                    <xsl:attribute name="readonly">readonly</xsl:attribute>
                                                </xsl:if>
                                            </input>
                                        </dd>
                                        <dd>
                                            <label for="new_password_repeat{position()}"><xsl:apply-templates select="/folderblog/language/term[@id='REPEAT_PASS']" />:</label>
                                            <input type="text" class="text" name="user[{position()}][new_password_repeat]" id="new_password_repeat{position()}" value="">
                                                <xsl:if test="@name='superuser' and $loggedinUser != 'superuser'">
                                                    <xsl:attribute name="readonly">readonly</xsl:attribute>
                                                </xsl:if>
                                            </input>
                                        </dd>
                                        <dd>
                                            <label for="old_password{position()}"><xsl:apply-templates select="/folderblog/language/term[@id='OLD_PASS']" />:</label>
                                            <input type="text" class="text" name="user[{position()}][old_password]" id="old_password{position()}" value="">
                                                <xsl:if test="@name='superuser' and $loggedinUser != 'superuser'">
                                                    <xsl:attribute name="readonly">readonly</xsl:attribute>
                                                </xsl:if>
                                            </input>
                                        </dd>
                                    </dl>
                                </li>
                            </xsl:for-each>
                        </ol>
                        <hr class="clear" />
                        <input type="hidden" name="action" value="users" />
                        <input type="submit" class="big submit">
                            <xsl:attribute name="value">
                                <xsl:apply-templates select="/folderblog/language/term[@id='SAVE_CHANGES']" />
                            </xsl:attribute>
                        </input>
                    </form>
                    
                    <hr class="clear" style="margin-top:3em;" />
                    
                    <div style="width:50%; float:left; clear:both; ">
                        <h2><xsl:apply-templates select="/folderblog/language/term[@id='ADD_USER']" /></h2>
                        <form id="adduser" method="post" action="{$base_with_slash}{$view}/#users">
                            <ol>
                                <li>
                                    <label for="username"><xsl:apply-templates select="/folderblog/language/term[@id='USER']" /></label>
                                    <input type="text" class="text" name="username" id="username" value="" />
                                </li>
                                <li>
                                    <label for="nicename"><xsl:apply-templates select="/folderblog/language/term[@id='DISPLAY_NAME']" /></label>
                                    <input type="text" class="text" name="nicename" id="nicename" value="" />
                                </li>
                                <li>
                                    <label for="nicename"><xsl:apply-templates select="/folderblog/language/term[@id='PASS']" /></label>
                                    <input type="text" class="text" name="password" id="password" value="" />
                                </li>
                            </ol>
                            <input type="hidden" name="action" value="adduser" />
                            <input type="submit" class="big submit">
                                <xsl:attribute name="value">
                                    <xsl:apply-templates select="/folderblog/language/term[@id='ADD_NEW_USER']" />
                                </xsl:attribute>
                            </input>
                        </form>
                    </div>
                    
                    <div style="margin-left:50%;">
                        <h2><xsl:apply-templates select="/folderblog/language/term[@id='DELETE_USER']" /></h2>
                        <form id="deleteuser" method="post" action="{$base_with_slash}{$view}/#users">
                            <ol>
                                <li>
                                    <label for="deleteuser"><xsl:apply-templates select="/folderblog/language/term[@id='CHOOSE_USER']" /></label>
                                    <select name="user" id="user">
                                        <option value=""><xsl:apply-templates select="/folderblog/language/term[@id='CHOOSE_USER_SHORT']" /></option>
                                        <xsl:for-each select="/folderblog/users/user[@name != 'superuser']">
                                            <option value="{@name}"><xsl:value-of select="concat(@name,' - ',@nicename)" /></option>
                                        </xsl:for-each>
                                    </select>
                                </li>
                            </ol>
                            <input type="hidden" name="action" value="deleteuser" />
                            <input type="submit" class="submit">
                                <xsl:attribute name="value">
                                    <xsl:apply-templates select="/folderblog/language/term[@id='DELETE']" />
                                </xsl:attribute>
                                <xsl:attribute name="onclick">return confirm('<xsl:apply-templates select="/folderblog/language/term[@id='CONFIRM_USER_DELETE']" />');</xsl:attribute>
                            </input>
                        </form>
                    </div>
                    <hr class="clear" />
                </div>
            </div>
        </div>
    </xsl:template>
    
    <xsl:template name="options">
        <xsl:param name="count" select="1"/>
        <xsl:param name="start" select="1"/>
        <xsl:if test="$count > 0">
            <option>
                <xsl:attribute name="value"><xsl:value-of select="$start" /></xsl:attribute>
                <xsl:if test="$start = /folderblog/settings/show_thumbs">
                    <xsl:attribute name="selected">selected</xsl:attribute>
                </xsl:if>
                <xsl:value-of select="$start" />
            </option>
            <xsl:call-template name="options">
                <xsl:with-param name="count" select="$count - 1" />
                <xsl:with-param name="start" select="$start + 1" />
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>