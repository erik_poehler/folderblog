<?php
/**
 *
 * @package Folderbog 4.0
 * @author Erik Pöhler
 * @version 4.0
 * @see http://erikpoehler.com/folderblog/
 *
 */

if(!defined('FB_PROFILE')) define('FB_PROFILE','off');

final class Folderblog {
    public $xml;

    /**
     *
     * constructor
     */
    public function __construct() {
        if (FB_PROFILE === "on") { file_put_contents($_SERVER["DOCUMENT_ROOT"] . FB_BASE . '/php-profiling.txt', microtime()." - read - folderblog.xml - " .basename(__FILE__)." (16)\n", FILE_APPEND); }
        $dom = new DOMDocument();
        $dom->load($_SERVER["DOCUMENT_ROOT"] . FB_BASE . '/lib/data/folderblog.xml');
        $this->xml = $dom->saveXML();
    }

    /**
     *
     * starting the frontend and deciding on what to show
     */
    public function run() {
        new ActionHandler();
        $frontend = new Frontend();
        $frontend->show();
    }

    /**
     *
     * singleton pattern so that the data file can be stored (in memcache)
     */
    public static function getInstance() {
        if (class_exists("Memcache")) {
            $memcache = new Memcache;
            $isMemcacheAvailable = @$memcache->connect('127.0.0.1', 11211);
            if ($isMemcacheAvailable) {
                $folderblog = $memcache->get('Folderblog'.FB_UID);
                if($folderblog instanceof Folderblog) {
                    // if it's there, use it
                    return $folderblog;
                } else {
                    // otherwise set it
                    $folderblog = new Folderblog();
                    $memcache->set('Folderblog'.FB_UID, $folderblog);
                    return $folderblog;
                }
            } else {
                return new Folderblog();
            }
        } else {
            return new Folderblog();
        }
    }
}