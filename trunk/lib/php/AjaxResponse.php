<?php
/**
 *
 * @package Folderbog 4.0
 * @author Erik Pöhler
 * @version 4.0
 * @see http://erikpoehler.com/folderblog/
 *
 */
class AjaxResponse {
    /**
     *
     * @var boolean $success
     */
	public $success = false;
	/**
     *
     * @var string $code
     */
	public $code = "";
	/**
	 *
	 * @var mixed $data
	 */
	public $data = null;
	/**
	 *
	 * @var string $message
	 */
    public $message = '';
    /**
     *
     * @var boolean $public
     */
    public $public = false;

    /**
     *
     * @param bool $success
     * @param string $message
     * @param mixed $data
     * @return void
     */
    public function __construct($success = null, $message = null, $data = null, $public = null) {
        session_start();
        if (strlen($_SESSION["Folderblog".FB_UID]["loggedinUser"])>0) {
            // user is logged in, proceed
            if (!is_null($success)) $this->success = (bool) $success;
            if (!is_null($message)) $this->message = (string) $message;
            if (!is_null($data)) $this->data = $data;
            if (!is_null($public)) $this->public = (bool) $public;
        } else {
            // not logged in
            if ($this->public === false) {
                // this ajax is supposedly private/for admin section only
                header("Content-Type: text/xml; charset=utf8");
                echo $this->setCode("login")->setMessage('You are not logged in')->createResponse();
                exit;
            }
        }
    }

    /**
     * setter to decide if successful or error
     * @param bool $success
     * @return AjaxResponse
     */
    public function setSuccess($success = false) {
        $this->success = $success;
        return $this;
    }

	/**
     * sets a code which is easier to use than parsing the text response
     * @param bool $success
     * @return AjaxResponse
     */
    public function setCode($code) {
        $this->code = $code;
        return $this;
    }

    /**
     * sets a response message
     * @param string $message
     * @return AjaxResponse
     */
    public function setMessage($message = '') {
        $this->message = $message;
        return $this;
    }

    /**
     * sets the response data
     * @param mixed $data
     * @return AjaxResponse
     */
    public function setData($data = false) {
        switch (gettype($data)) {
            case "string":
                $this->data = $data;
                break;
            case "object":
                // allow for XML tree to be added
                if ($data instanceof DOMDocument) {
                    $this->data = $data;
                    break;
                } else {
                    // disallow any other type of objects...
                    $this->setSuccess(false)->setMessage(get_class($data)." not supported yet.");
                    continue;
                }
            default:
                $this->success = false;
                $this->message = gettype($data)." not implemented yet";
                break;
        }
        return $this;
    }

    /**
     * creates a standardized XML response
     * @todo make sure xml trees can be responded too
     * @return string
     */
    public function createResponse() {
        $xml = new DOMDocument("1.0", "UTF-8");
        $xml->loadXML("<response />");
        $success = ($this->success) ? "true" : "false";
        $xml->documentElement->setAttribute("code", $this->code);
        $xml->documentElement->setAttribute("isSuccess", $success);
        if (!is_null($this->data)) {
            switch(gettype($this->data)) {
                case "string":
                    $data = $xml->createElement("data", $this->data);
                    $xml->documentElement->appendChild($data);
                    break;
                case "object":
                    if ($this->data instanceof DOMDocument) {
                        /**
                         * would be nice if this would work... unfortunately it does not.
                         * @see https://bugs.php.net/bug.php?id=55294
                         */
                        $node = $xml->importNode($this->data->documentElement, true);
                        $data = $xml->createElement("data");
                        $data->appendChild($node);
                        $xml->documentElement->appendChild($data);
                    }
                    break;
            }
        }
        if (strlen($this->message)>0) {
            $message = $xml->createElement("message", $this->message);
            $xml->documentElement->appendChild($message);
        }
//         $str = $xml->saveXML($xml->documentElement, LIBXML_NOEMPTYTAG);
        $str = $xml->saveXML();
        /* this is so fucking ugly... */
        return str_replace(array('default:',' xmlns:default="http://www.w3.org/1999/xhtml"',' xmlns="http://www.w3.org/1999/xhtml"'),'',$str);
    }
}