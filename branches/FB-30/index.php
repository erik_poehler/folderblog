<?php
function _e($str) {
    echo $str;
}
?><!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <title>Erik Pöhler</title>
        <meta content="Website of Erik Pöhler - Senior PHP Developer" name="description" />
        <meta content="Internationalization, Localization, i18n, l10n, multilingual, bilingual, PHP, Zend Framework, ZF2, erik, pohler, pöhler, developer, ecommerce, magento, wordpress" name="keywords" />
        <meta charset="UTF-8" />
        <meta content="Erik Pöhler" name="generator" />
        <meta content="Erik Pöhler" name="author" />
        <meta content="<?php _e('LANGISO'); ?>" name="language" />
        <meta property="og:type" content="personal" />
        <meta property="og:title" content="Erik Pöhler - PHP Developer" />
        <meta property="og:description" content="<?php _e('META_DESCRIPTION'); ?>" />
        <meta property="og:url" content="http://erikpoehler.com/" />
        <meta property="og:image" content="http://erikpoehler.com/erik.png" />
        <meta property="og:locale" content="<?php _e('LOCALE'); ?>" />
        <meta property="og:site_name" content="erikpoehler.com" />
        <meta name="twitter:site" content="@rss2" />
        <!-- <meta property="fb:app_id" content="403152586446756" /> -->
        <link rel="icon" href="/favicon.ico" rel="icon" type="image/x-icon" />
        <link rel="stylesheet" href="/css/styles.css" media="all" type="text/css" />
        <link rel="stylesheet" href="/css/webfonts.css" media="all" type="text/css" />
    </head>
    <body class="index">
        <div id="page">
            <div id="content">
                <header style="overflow: hidden;">
                    <p style="height:2.5em;line-height:2.5em;">
                        <a href="http://erikpoehler.com/">Homepage</a> &bull; <a href="http://erikpoehler.com/">Blog</a>
                    </p>
                </header>
                <div class="subcolumns">
                    <article class="c66l">
                        <div class="box">
                            <div style="padding-top:10px;text-align:center;">
                                <img src="images/logo-large-only-folder.png" style="vertical-align:middle;"/>
                                <h2 style="font-size:2.5em;">Hi, my name is Erik Pöhler</h2>
                                <p>I am 33 year old web developer Erik Pöhler from Germany. I make websites. My specialties are Frontend Performance, eCommerce, PHP&nbsp;Frameworks, XML&nbsp;&&nbsp;XSLT, Web Services, Databases and jQuery. I live and work in Mexico City since 2010. I am fluent in these languages: HTML, CSS, PHP, JS, XSLT, MySQL, German, Spanish and English.<br /><br /></p>
                                <a href="download.php" class="btn" style="height:40px;text-align:left;padding:5px 10px 5px 65px ; margin-right:10px; position:relative">
                                    <i style="position:absolute; left:12px; top:0; height:50px; line-height:50px; font-size:2.5em" class="icon-download-alt"></i>
                                    <?php
                                    if (is_file(dirname(__FILE__)."/count.txt")) {
                                        $count = file_get_contents('count.txt');
                                    } else {
                                        $count = 204;
                                    }
                                    ?>
                                    Download my CV<br />
                                    (PDF, 750 KB)
                                </a>
                                <hr class="seperate" />
                            </div>
                            <h3>Open-Source Projects</h3>
                            <p>I am comitted to open-source software since it got me into programming back in 2005. Check out the projects that I am working on:</p>
                            <ol id="requirements" style="margin-bottom:1em;">
                                <li><i class="icon-asterisk"></i> <a href="#">Folderblog 4.0</a> is a flexible image gallery and photolog platform written in PHP, which utilizes XML as a data-storage and XSLT to generate HTML pages. It has a backend for easy image upload and album management and has a theming engine to switch designs easily.</li>
                                <li><i class="icon-asterisk"></i> <a href="#">ZendGlot 1.0</a> is a simplified rework of ReGlot. While ReGlot was procedural code, ZendGlot is OOP and is based on Zend Framework 2.1.x and Twitter Bootstrap.</li>
                                <li><i class="icon-asterisk"></i> <a href="#">WebSVN 2.5</a> is a PHP-based browser for Subversion repositories (both: local and remote).</li>
                            </ol>
                            <hr class="seperate" />
                            <h3></h3>
                            <ol id="features">
                                <li><i class="icon-asterisk"></i> <?php _e('BATCH');?></li>
                                <li><i class="icon-asterisk"></i> <?php _e('THEMES');?></li>
                                <li><i class="icon-asterisk"></i> <?php _e('SEO');?></li>
                                <li><i class="icon-asterisk"></i> <?php _e('IMGEDIT');?></li>
                                <li><i class="icon-asterisk"></i> <?php _e('UNLALBUMS');?></li>
                                <li><i class="icon-asterisk"></i> <?php _e('USRS');?></li>
                                <li><i class="icon-asterisk"></i> <?php _e('NODB');?></li>
                                <li><i class="icon-asterisk"></i> <?php _e('NOJS');?></li>
                                <li><i class="icon-asterisk"></i> <?php _e('CACHING');?></li>
                                <li><i class="icon-asterisk"></i> <?php _e('I18N');?></li>
                            </ol>
                            <hr class="seperate" />
                            <h3><?php _e('START'); ?></h3>
                            <ol id="gettingstarted">
                                <li><strong class="blue" style="font-style:normal;font-size:1.1em;">1.</strong> <?php _e('INS1'); ?></li>
                                <li><strong class="blue" style="font-style:normal;font-size:1.1em;">2.</strong> <?php _e('INS2'); ?></li>
                                <li><strong class="blue" style="font-style:normal;font-size:1.1em;">3.</strong> <?php _e('INS3'); ?>
                                </li>
                                <li><strong class="blue" style="font-style:normal;font-size:1.1em;">4.</strong> <?php _e('INS4'); ?></li>
                                <li><strong class="blue" style="font-style:normal;font-size:1.1em;">5.</strong> <?php _e('INS5'); ?></li>
                                <li><strong class="blue" style="font-style:normal;font-size:1.1em;">6.</strong> <?php _e('INS6'); ?></li>
                                <li><strong class="blue" style="font-style:normal;font-size:1.1em;">7.</strong> <?php _e('INS7'); ?></li>
                            </ol>
                            <hr class="seperate" />
                            <h3><?php _e('TITLE_THEMES'); ?></h3>
                            <p><?php _e('THEMES_DESC'); ?></p>
                            <hr class="seperate" />
                            <h3><?php _e('LICENSE'); ?></h3>
                            <p>
                                <?php _e('LICTXT'); ?>
                            </p>
                            <hr class="seperate" />
                            <h3><?php _e('SUPPORTTITLE'); ?></h3>
                            <p><?php _e('SUPPORTTXT'); ?></p>
                            <hr class="seperate" />
                            <h3><?php _e('THANKS'); ?></h3>
                            <p><?php _e('THANKSTXT'); ?></p>
                        </div>
                    </article>
                    <aside class="c33r">
                        <div class="box">
                            <h3>Downloads</h3>
                            <nav>
                                <ul id="links">
                                    <li><a href="http://erikpoehler.com/zendglot/"><i class="icon-beaker"></i> ZendGlot 1.0</a></li>
                                    <li><a href="http://erikpoehler.com/folderblog/"><i class="icon-beaker"></i> Folderblog 4.0</a></li>
                                    <li><a href="http://erikpoehler.com/websvn/"><i class="icon-beaker"></i> WebSVN 2.5</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="box">
                            <h3>Latest Blog Articles</h3>
                            <nav>
                                <ul id="blog">
                                    <li><strong>25.3.2013</strong> <a href="http://erikpoehler.com/zendglot/">Adding custom classes to Zend Framework as controller plugins</a></li>
                                    <li><strong>23.3.2013</strong> <a href="http://erikpoehler.com/folderblog/">Why Folderblog 4.0 is better than other gallery scripts</a></li>
                                    <li><strong>20.3.2013</strong> <a href="http://erikpoehler.com/websvn/">Howto create a new theme for Folderblog</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="box">
                            <h3>Links</h3>
                            <nav>
                                <ul id="links">
                                    <li><a href="https://www.xing.com/profile/erikpoehler/"><i class="icon-eye-open"></i> Xing</a></li>
                                    <li><a href="http://linkedin.com/in/erikpoehler/" class="dl"><i class="icon-download-alt"></i> LinkedIn</a></li>
                                    <li><a href="http://stackoverflow.com/user/iroybot/"><i class="icon-beaker"></i> StackOverflow</a></li>
                                    <li><a href="http://iroybot.deviantart.com/"><i class="icon-beaker"></i> DeviantArt</a></li>
                                    <li><a href=""><i class="icon-hand-up"></i> <?php _e('JIRA'); ?></a></li>
                                </ul>
                            </nav>
                        </div>
                    </aside>
                </div>
                <footer style="clear:both;">
                    <p><a href="http://erikpoehler.com/" title="Erik Pöhler">Erik Pöhler</a> 2013</p>
                </footer>
            </div>
        </div>
    </body>
</html>
