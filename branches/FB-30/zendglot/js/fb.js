function str_rot13(str) {
    return (str + '').replace(/[a-z]/gi, function(s) {
        return String.fromCharCode(s.charCodeAt(0) + (s.toLowerCase() < 'n' ? 13 : -13));
    });
}

function base64_decode(data) {
    var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    var o1, o2, o3, h1, h2, h3, h4, bits, i = 0, ac = 0, dec = "", tmp_arr = [];
    if (!data) {
        return data;
    }
    data += '';
    do { // unpack four hexets into three octets using index points in b64
        h1 = b64.indexOf(data.charAt(i++));
        h2 = b64.indexOf(data.charAt(i++));
        h3 = b64.indexOf(data.charAt(i++));
        h4 = b64.indexOf(data.charAt(i++));
        bits = h1 << 18 | h2 << 12 | h3 << 6 | h4;
        o1 = bits >> 16 & 0xff;
        o2 = bits >> 8 & 0xff;
        o3 = bits & 0xff;
        if (h3 == 64) {
            tmp_arr[ac++] = String.fromCharCode(o1);
        } else if (h4 == 64) {
            tmp_arr[ac++] = String.fromCharCode(o1, o2);
        } else {
            tmp_arr[ac++] = String.fromCharCode(o1, o2, o3);
        }
    } while (i < data.length);
    dec = tmp_arr.join('');
    return dec;
}

// obfuscating
var raw = document.getElementById("raw").innerHTML;
var decoded = str_rot13(base64_decode(raw));
document.getElementById("support").href = "mailto:" + decoded
        + "?subject=Support Request";
document.getElementById("getfeatured").href = "mailto:" + decoded
        + "?subject=Please feature my Folderblog";

// jquery
$(document).ready(function(){
    $("a.dl").click(function(){
        _gaq.push(['_trackPageview', '/folderblog/download.php']);
    });
    
    $('#links li a')
    .mouseover(function(){
        $(this).animate({backgroundColor:"#eee"},200).find('i:eq(0)').stop().animate({top: '3px', left: 0},350);
    })
    .mouseout(function(){
        $(this).animate({backgroundColor:"#fff"},200).find('i:eq(0)').stop().animate({backgroundColor: 'transparent', top:'3px', left: '5px'},500);
    });
    
    /**
     * Mailchimp Newsletter
     */
    $('#signup').submit(function() {
        // update user interface
        $('#response').html($('span#adding').text());
        
        // Prepare query string and send AJAX request
        $.ajax({
            dataType: 'html',
            type: 'GET',
            url: '/folderblog/inc/mailchimp.php',
            data: 'ajax=true&email=' + escape($('#email').val()),
            success: function(msg) {
                $('#response').html(msg);
            }
        });
        
        return false;
    });
});