<?php
require_once 'functions.php';
?>
<aside class="c33r">
    <div class="box">
        <h3><?php _e('LINKS'); ?></h3>
        <nav>
            <ul id="links">
                <?php if (strpos('_'.$_SERVER["REQUEST_URI"],"themes.php")>0) { ?><li><a href="/zendglot/"><i class="icon-folder-open-alt"></i> <?php _e('FBHOME'); ?></a></li><?php } ?>
                <li><a href="http://translate.erikpoehler.com/"><i class="icon-eye-open"></i> <?php _e('SBDEMO'); ?></a></li>
                <li><a href="/zendglot/download.php" class="dl"><i class="icon-download-alt"></i> <?php _e('DLFB'); ?></a></li>
                <li><a href="http://websvn.erikpoehler.com/listing.php?repname=Zendglot"><i class="icon-beaker"></i> <?php _e('WSVN'); ?></a></li>
                <li><a href="http://svn.erikpoehler.com/zendglot"><i class="icon-beaker"></i> <?php _e('SVN'); ?></a></li>
                <li><a href="http://jira.erikpoehler.com"><i class="icon-hand-up"></i> <?php _e('JIRA'); ?></a></li>
            </ul>
        </nav>
    </div>
    <div class="box">
        <form id="signup" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="get">
            <span id="adding" style="display:none;"><?php _e('ADDING'); ?></span>
            <fieldset>
                <legend><strong><i class="icon-envelope-alt"></i> <?php _e('SIGNUP'); ?></strong></legend>
                <span id="response"><? require_once(dirname(__FILE__).'/mailchimp.php'); if(isset($_GET['submit'])){ echo storeAddress(); } ?></span>
                <input autocomplete="off" type="text" class="text" name="email" id="email" placeholder="<?php _e("EML"); ?>" />
                <input type="submit" name="submit" id="join" value="<?php _e('JOIN'); ?>" class="btn" alt="Join" />
                <br />
                <small style="display:block; font-style:italic; color:#bbb; margin-top:5px;"><?php _e('NOSPAM'); ?></small>
            </fieldset>
        </form>
    </div>
    <div class="box" id="tweets" style="position:relative;">
        <h3 style="display:inline;"><?php _e('TWIT');?></h3>
        <ul id="twitter">
            <?php echo showTwitter('folderblog');?>
        </ul>
        <p style="text-align:right;"><?php _e('FOLLOW'); ?></p>
    </div>

    <div class="box">
        <h3>Featured Folderblog</h3>
        <ul id="featured">
            <li><a href="http://photos.tetto.org/"><img width="289" height="160" src="images/featured.png" /><br />A Photojournal by Donald Tetto</a>
            </li>
            <li><a id="getfeatured" href="#"><i class="icon-envelope"></i> Get featured!</a>
            </li>
        </ul>
        <hr class="clear" />
    </div>

    <div class="box">
        <h3>Buy me a drink</h3>
        <p><?php _e('DONATE'); ?></p>
        <ul id="donate" style="margin-top: .5em; border-bottom: 1px dotted #ccc;">
            <li style="padding: 5px 0;">Paypal
                <form style="display: inline" action="https://www.paypal.com/cgi-bin/webscr" method="post">
                    <input type="hidden" name="cmd" value="_s-xclick" />
                <input type="hidden" name="hosted_button_id" value="C3FKE5R3R7FM4" />
                <input type="image" style="display: inline; vertical-align: -12px; margin-left: 3px;" src="images/donate-beer.png" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!" />
                <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1" />
                </form>
            </li>
            <li style="padding-top: 5px;"><a class="FlattrButton" style="vertical-align: -4px; display: none;" rev="flattr;button:compact;" href="http://erikpoehler.com/folderblog/"></a>
                <noscript>
                    <a href="http://flattr.com/thing/1129185/Folderblog-4" target="_blank"> <img src="http://api.flattr.com/button/flattr-badge-large.png" alt="Flattr this" title="Flattr this" border="0" /> </a>
                </noscript>
            </li>
        </ul>
        <hr class="clear" />
    </div>
</aside>
<hr class="clear" />