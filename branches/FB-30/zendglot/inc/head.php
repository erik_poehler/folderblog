<head>
    <title>ZendGlot 1.0</title>
    <meta content="Zendglot 1.0 is a tool to help you translate applications and websites. It's written in PHP and Zend Framework 2. It's open source and it's free." name="description" />
    <meta content="Internationalization, Localization, i18n, l10n, multilingual, bilingual, PHP, Zend Framework, ZF2" name="keywords" />
    <meta charset="UTF-8" />
    <meta content="Erik Pöhler" name="generator" />
    <meta content="Erik Pöhler" name="author" />
    <meta content="<?php _e('LANGISO'); ?>" name="language" />
    <meta property="og:type" content="software" />
    <meta property="og:title" content="ZendGlot 1.0" />
    <meta property="og:description" content="<?php _e('META_DESCRIPTION'); ?>" />
    <meta property="og:url" content="http://erikpoehler.com/zendglot/" />
    <meta property="og:image" content="http://erikpoehler.com/zendglot/images/logo.png" />
    <meta property="og:locale" content="<?php _e('LOCALE'); ?>" />
    <meta property="og:site_name" content="erikpoehler.com" />
    <!-- <meta name="twitter:site" content="@folderblog" />
    <meta property="fb:app_id" content="403152586446756" /> -->
    <link rel="icon" href="/zendglot/images/favicon.ico" rel="icon" type="image/x-icon" />
    <link rel="stylesheet" href="/zendglot/css/styles.css" media="all" type="text/css" />
    <link rel="stylesheet" href="/zendglot/css/webfonts.css" media="all" type="text/css" />
    <script type="text/javascript">(function(){var b=document.createElement("script"),a=document.getElementsByTagName("script")[0];b.type="text/javascript";b.async=true;b.src="http://api.flattr.com/js/0.6/load.js?mode=auto";a.parentNode.insertBefore(b,a)})();</script>
</head>