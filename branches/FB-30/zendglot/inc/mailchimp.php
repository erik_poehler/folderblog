<?php
session_start();
if (isset($_GET['l'])) {
    $locale = $_GET['l'];
    $_SESSION['fblocale'] = $locale;
    header("Location: ".pathinfo($_SERVER["REQUEST_URI"],PATHINFO_DIRNAME));
    session_write_close();
} else {
    include_once dirname(__FILE__)."/functions.php";
    $locale = getLanguage();
}

if (is_file(realpath(dirname(__FILE__) . "/../lang/").$locale.".php")) {
    $trans = include_once realpath(dirname(__FILE__) . "/../lang/").$locale.".php";
}
include_once dirname(__FILE__)."/functions.php";

// var_dump($trans);exit;

/**
 * Adds a subsciber to the Mailchimp list
 */
function storeAddress(){
    // Validation
    if(!$_GET['email']){ return __('NOEML'); }

    if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*$/i", $_GET['email'])) {
        return __("EMLINVALID");
    }

    require_once('MCAPI.php');
    // grab an API Key from http://admin.mailchimp.com/account/api/
    $api = new MCAPI('a73f5e732157c0eca151aaed7a4064b0-us6');

    // grab your List's Unique Id by going to http://admin.mailchimp.com/lists/
    // Click the "settings" link for the list - the Unique Id is at the bottom of that page.
    $list_id = "d23393bcb7";

    if($api->listSubscribe($list_id, $_GET['email'], '') === true) {
        // It worked!
        return __("SUCCESS");
    }else{
        // An error ocurred, return error message
        file_put_contents(dirname(__FILE__).'/mailchimp.log', $api->errorMessage."\n", FILE_APPEND);
        if (strpos($api->errorMessage, 'is already subscribed to list') > 0) {
            $error = __('ALREADY');
        } else {
            $error = $api->errorMessage;
        }
        return __("ERROR") . $error;
    }
}
if (isset($_GET)) {
    if(isset($_GET['ajax'])) {
        echo storeAddress();
    }
}
?>