<?php
include_once dirname(__FILE__)."/inc/functions.php";
session_start();
if (isset($_GET['l'])) {
    $locale = $_GET['l'];
    $_SESSION['fblocale'] = $locale;
    header("Location: ".pathinfo($_SERVER["REQUEST_URI"],PATHINFO_DIRNAME).parse_url(pathinfo($_SERVER["REQUEST_URI"],PATHINFO_BASENAME), PHP_URL_PATH));
    session_write_close();
} else {
    $locale = getLanguage();
}
if (is_file(dirname(__FILE__) . "/lang/".$locale.".php")) {
    $trans = include_once realpath(dirname(__FILE__) . "/lang/")."/".$locale.".php";
}
?><!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <?php include_once "inc/head.php"; ?>
    <body class="archive">
        <div id="page">
            <div id="content">
                <header style="overflow: hidden;">
                    <?php include "inc/header.php"; ?>
                </header>
                <div class="subcolumns">
                    <article class="c66l" id="themes">
                        <div class="box">
                            <div style="padding-top:10px;text-align:center;">
                                <img src="images/logo-large-only-folder.png" style="vertical-align:middle;"/>
                                <h2 style="font-size:2.5em;">Folderblog 4.0</h2>
                            </div>
                            <hr class="seperate" />
                            <h2><?php _e("TITLE_THEMES");?></h2>
                            <a rel="gallery-themes" title="Default Theme v1.0" href="images/default-theme-big.png" class="thickbox left">
                                <img src="images/default-theme-small.png"  />
                            </a>
                            <dl class="theme">
                                <dt><?php _e("NAME");?>:</dt>
                                <dd>Default Theme v.1.0<br /></dd>
                                <dt><?php _e("DEMO");?>:</dt>
                                <dd><a href="http://photos.erikpoehler.com/"><?php _e("SEE_IN_ACTION"); ?></a><br /></dd>
                                <dt><?php _e("AUTHOR");?>:</dt>
                                <dd>Erik Pöhler<br /></dd>
                                <dt><?php _e("DETAILS");?>:</dt>
                                <dd>The lightly colored, blue-ish default theme of Folderblog. It's a two column layout written in HTML5 making quite some use of Javascript for every index view. (so there's no time spent on waiting for pages to load).</dd>
                            </dl>
                            <hr class="seperate" />
                            <a rel="gallery-themes" title="Portfolio Theme v1.0" href="images/portfolio-theme-big.png" class="thickbox left">
                                <img src="images/portfolio-theme-small.png" />
                            </a>
                            <dl class="theme">
                                <dt><?php _e("NAME");?>:</dt>
                                <dd>Portfolio Theme v.1.0<br /></dd>
                                <dt><?php _e("DEMO");?>:</dt>
                                <dd><a href="http://portfolio.erikpoehler.com/"><?php _e("SEE_IN_ACTION"); ?></a><br /></dd>
                                <dt><?php _e("AUTHOR");?>:</dt>
                                <dd>Erik Pöhler<br /></dd>
                                <dt><?php _e("DETAILS");?>:</dt>
                                <dd>A dark, fullscreen gallery theme written in HTML 5. As well it's fully accessible and functional without Javascript in order to enable search engines to find all content. It is prepared to be branded with your own logos. Also, it makes use of Javascript to preload and load images with slide effects.</dd>
                            </dl>
                            <hr class="seperate" />
                            <p style="text-align:center">more to come...</p>

                        </div>
                    </article>
                    <?php include_once "inc/sidebar.php"; ?>
                </div>
                <?php include_once "inc/footer.php"; ?>
            </div>
        </div>
        <?php include_once "inc/footer_scripts.php"; ?>
    </body>
</html>