<?php
function rsstotime($rss_time) {
    $day = substr($rss_time, 5, 2);
    $month = substr($rss_time, 8, 3);
    $month = date('m', strtotime("$month 1 2011"));
    $year = substr($rss_time, 12, 4);
    $hour = substr($rss_time, 17, 2);
    $min = substr($rss_time, 20, 2);
    $second = substr($rss_time, 23, 2);
    $timezone = substr($rss_time, 26);

    $timestamp = mktime($hour, $min, $second, $month, $day, $year);

    date_default_timezone_set('UTC');

    if(is_numeric($timezone)) {
        $hours_mod = $mins_mod = 0;
        $modifier = substr($timezone, 0, 1);
        $hours_mod = (int) substr($timezone, 1, 2);
        $mins_mod = (int) substr($timezone, 3, 2);
        $hour_label = $hours_mod>1 ? 'hours' : 'hour';
        $strtotimearg = $modifier.$hours_mod.' '.$hour_label;
        if($mins_mod) {
            $mins_label = $mins_mod>1 ? 'minutes' : 'minute';
            $strtotimearg .= ' '.$mins_mod.' '.$mins_label;
        }
        $timestamp = strtotime($strtotimearg, $timestamp);
    }

    return $timestamp;
}
/**
 *
 * turns the response from twitter into an array
 * @param string $feed
 * @return array
 */
function parse_feed($feed) {
    $dom = new DomDocument();
    @$dom->loadXML($feed);
    $entries = $dom->getElementsByTagName('item');
    $r = array();
    if ($entries->length > 0) {
        foreach ($entries as $entry) {
            $r[] = array(
                //'published' => rsstotime($entry->getElementsByTagName('pubDate')->item(0)->nodeValue),
                'link'      => $entry->getElementsByTagName('link')->item(0)->getAttribute('href'),
                'content'   => substr($entry->getElementsByTagName('title')->item(0)->nodeValue,11),
            );
        }
    }
    return $r;
}

/**
 *
 * retrieves either static html from the flatfile or updates the file once an hour from twitter
 * @param unknown_type $username
 */
function showTwitter($username) {
    if (is_file($_SERVER["DOCUMENT_ROOT"].'/folderblog/inc/tweets.xml')) {
        $mtime = filemtime($_SERVER["DOCUMENT_ROOT"].'/folderblog/inc/tweets.xml');
        $diff = time() - $mtime;
        // only check for new tweets once an hour
        if ($diff < 60 * 60) {
            echo file_get_contents($_SERVER["DOCUMENT_ROOT"].'/folderblog/inc/tweets.xml');
        }
    }
    $twitterFeed = file_get_contents("https://api.twitter.com/1/statuses/user_timeline.rss?screen_name=Folderblog");
    $entries = parse_feed($twitterFeed);
    $out = '';
    $limit = 3;
    $i = 0;
    foreach($entries as $entry) {
        //$time = $entry['published'];
        if ($i < $limit) {
            $out .= sprintf('<li><a href="%s">%s</a></li>', $entry['link'], stripslashes($entry['content']));
        }
        $i++;
    }

    file_put_contents($_SERVER["DOCUMENT_ROOT"].'/folderblog/inc/tweets.xml', $out);
    return $out;
}

function getLanguage() {
    session_start();
    if (isset($_SESSION['fblocale'])) {
        return $_SESSION['fblocale'];
    }
    if (function_exists('http_negotiate_language')) {
        $langs = array(
            'en-US', //default
            'de',
            'de-DE',
            'de-AT',
            'de-CH',
            'es',
            'es-MX',
            'es-ES',
            'es-AR',
        );
        return str_replace('-','_',http_negotiate_language($langs, $result));
    } else {
        return "en_US";
    }
}

function __($text){
    return translate($text);
}

function _e($text) {
    echo translate($text);
}

function translate($text) {
    global $trans;
    if (!is_array($trans)) $trans = getTrans();

    if (array_key_exists($text, $trans)) {
        return $trans[$text];
    } else {
        return $text;
    }
}

function getTrans() {
    global $trans;
    if (is_array($trans)) return $trans;
    else {
        session_start();
        if (isset($_SESSION['fblocale'])) {
            $locale = $_SESSION['fblocale'];
        } else {
            $locale = getLanguage();
        }
        if (is_file(realpath(dirname(__FILE__) . "/../lang/")."/".$locale.".php")) {
            $trans = include_once realpath(dirname(__FILE__) . "/../lang/")."/".$locale.".php";
        }
        if (!is_array($trans)) throw new Exception("WTF");
        return $trans;
    }
}
?>