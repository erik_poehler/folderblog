<head>
    <title>Folderblog 4.0</title>
    <meta content="Folderblog is a fast, intuitive and easy to use image gallery written in PHP. It does not require a database. And it's open source." name="description" />
    <meta content="PHP, image, gallery, photos, script, folderblog, photolog, flog, publish, easy, intuitive, batch, upload" name="keywords" />
    <meta charset="UTF-8" />
    <meta content="Erik Pöhler" name="generator" />
    <meta content="Erik Pöhler" name="author" />
    <meta content="<?php _e('LANGISO'); ?>" name="language" />
    <meta property="og:type" content="software" />
    <meta property="og:title" content="Folderblog 4.0" />
    <meta property="og:description" content="<?php _e('META_DESCRIPTION'); ?>" />
    <meta property="og:url" content="http://erikpoehler.com/folderblog/" />
    <meta property="og:image" content="http://erikpoehler.com/folderblog/images/logo-large-only-folder.png" />
    <meta property="og:locale" content="<?php _e('LOCALE'); ?>" />
    <meta property="og:site_name" content="erikpoehler.com" />
    <meta name="twitter:site" content="@folderblog" />
    <meta property="fb:app_id" content="403152586446756" />
    <link rel="icon" href="/folderblog/images/favicon.ico" rel="icon" type="image/x-icon" />
    <link rel="stylesheet" href="/folderblog/css/styles.css" media="all" type="text/css" />
    <link rel="stylesheet" href="/folderblog/css/webfonts.css" media="all" type="text/css" />
    <link rel="stylesheet" href="/folderblog/css/thickbox.css" media="all" type="text/css" />
    <script type="text/javascript">(function(){var b=document.createElement("script"),a=document.getElementsByTagName("script")[0];b.type="text/javascript";b.async=true;b.src="http://api.flattr.com/js/0.6/load.js?mode=auto";a.parentNode.insertBefore(b,a)})();</script>
</head>