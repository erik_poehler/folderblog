<?php
include_once dirname(__FILE__)."/inc/functions.php";
session_start();
if (isset($_GET['l'])) {
    $locale = $_GET['l'];
    $_SESSION['fblocale'] = $locale;
    header("Location: ".pathinfo($_SERVER["REQUEST_URI"],PATHINFO_DIRNAME));
    session_write_close();
} else {
    $locale = getLanguage();
}
if (is_file(dirname(__FILE__) . "/lang/".$locale.".php")) {
    $trans = include_once realpath(dirname(__FILE__) . "/lang/")."/".$locale.".php";
}
?><!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <?php include_once "inc/head.php"; ?>
    <body class="archive">
        <div id="page">
            <div id="content">
                <header style="overflow: hidden;">
                    <?php include_once "inc/header.php"; ?>
                </header>
                <div class="subcolumns">
                    <article class="c66l">
                        <div class="box">
                            <div style="padding-top:10px;text-align:center;">
                                <img src="images/logo-large-only-folder.png" style="vertical-align:middle;"/>
                                <h2 style="font-size:2.5em;">Folderblog 4.0</h2>
                                <a href="download.php" class="btn" style="height:40px;text-align:left;padding:5px 10px 5px 65px ; margin-right:10px; position:relative">
                                    <i style="position:absolute; left:12px; top:0; height:50px; line-height:50px; font-size:2.5em" class="icon-download-alt"></i>
                                    <?php
                                    if (is_file(dirname(__FILE__)."/count.txt")) {
                                        $count = file_get_contents('count.txt');
                                    } else {
                                        $count = 204;
                                    }
                                    _e('DL'); echo $count." "; _e('DLCOUNT'); ?>
                                </a>
                                <?php _e('or'); ?>
                                <a href="http://photos.erikpoehler.com/" class="btn" style="height:40px;text-align:left;padding:5px 10px 5px 65px ;position:relative; margin-left:10px;">
                                    <i style="position:absolute; left:12px; top:0; height:50px; line-height:50px; font-size:2.5em" class="icon-eye-open"></i>
                                    <?php _e('SEEDEMO'); ?>
                                </a>
                                <hr class="seperate" />
                            </div>

                            <h3><?php _e('ABOUT'); ?></h3>
                            <p>
                                <?php _e('INTRO'); ?>
                            </p>
                            <hr class="seperate" />
                            <h3><?php _e('REQ'); ?></h3>
                            <ol id="requirements" style="margin-bottom:1em;">
                                <li><i class="icon-ok"></i> <?php _e('HT'); ?></li>
                                <li><i class="icon-ok"></i> <?php _e('MR'); ?></li>
                                <li><i class="icon-ok"></i> <?php _e('PHP'); ?></li>
                                <li><i class="icon-ok"></i> <?php _e('XSLT'); ?></li>
                                <li><i class="icon-ok"></i> GD <?php _e('OR'); ?> ImageMagick</li>
                            </ol>
                            <p><?php _e('SERVERCHECK'); ?></p>
                            <hr class="seperate" />
                            <h3><?php _e('FEATURES');?></h3>
                            <ol id="features">
                                <li><i class="icon-asterisk"></i> <?php _e('BATCH');?></li>
                                <li><i class="icon-asterisk"></i> <?php _e('THEMES');?></li>
                                <li><i class="icon-asterisk"></i> <?php _e('SEO');?></li>
                                <li><i class="icon-asterisk"></i> <?php _e('IMGEDIT');?></li>
                                <li><i class="icon-asterisk"></i> <?php _e('UNLALBUMS');?></li>
                                <li><i class="icon-asterisk"></i> <?php _e('USRS');?></li>
                                <li><i class="icon-asterisk"></i> <?php _e('NODB');?></li>
                                <li><i class="icon-asterisk"></i> <?php _e('NOJS');?></li>
                                <li><i class="icon-asterisk"></i> <?php _e('CACHING');?></li>
                                <li><i class="icon-asterisk"></i> <?php _e('I18N');?></li>
                            </ol>
                            <hr class="seperate" />
                            <h3><?php _e('START'); ?></h3>
                            <ol id="gettingstarted">
                                <li><strong class="blue" style="font-style:normal;font-size:1.1em;">1.</strong> <?php _e('INS1'); ?></li>
                                <li><strong class="blue" style="font-style:normal;font-size:1.1em;">2.</strong> <?php _e('INS2'); ?></li>
                                <li><strong class="blue" style="font-style:normal;font-size:1.1em;">3.</strong> <?php _e('INS3'); ?>
                                </li>
                                <li><strong class="blue" style="font-style:normal;font-size:1.1em;">4.</strong> <?php _e('INS4'); ?></li>
                                <li><strong class="blue" style="font-style:normal;font-size:1.1em;">5.</strong> <?php _e('INS5'); ?></li>
                                <li><strong class="blue" style="font-style:normal;font-size:1.1em;">6.</strong> <?php _e('INS6'); ?></li>
                                <li><strong class="blue" style="font-style:normal;font-size:1.1em;">7.</strong> <?php _e('INS7'); ?></li>
                            </ol>
                            <hr class="seperate" />
                            <h3><?php _e('TITLE_THEMES'); ?></h3>
                            <p><?php _e('THEMES_DESC'); ?></p>
                            <hr class="seperate" />
                            <h3><?php _e('LICENSE'); ?></h3>
                            <p>
                                <?php _e('LICTXT'); ?>
                            </p>
                            <hr class="seperate" />
                            <h3><?php _e('SUPPORTTITLE'); ?></h3>
                            <p><?php _e('SUPPORTTXT'); ?></p>
                            <hr class="seperate" />
                            <h3><?php _e('THANKS'); ?></h3>
                            <p><?php _e('THANKSTXT'); ?></p>
                        </div>
                    </article>
                    <?php include_once "inc/sidebar.php"; ?>
                </div>
                <?php include_once "inc/footer.php"; ?>
            </div>
        </div>
        <?php include_once "inc/footer_scripts.php"; ?>
    </body>
</html>