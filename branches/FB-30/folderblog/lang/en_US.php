<?php
return array(
    'BACK'      => 'Back to',
    'DL'        => 'Download (zip, 1MB)<br />',
    'DLCOUNT'   => 'downloads',
    'SEEDEMO'   => 'See the Demo<br />Photolog',
    'ABOUT'		=> 'About',
    'INTRO'		=> 'Folderblog is a lightweight photolog script written in PHP. It uses XML as data storage and XSLT to generate HTML. It can be used as an image gallery or as a photolog - or for anything in between. It\'s intuitive, simple and fast. It does <strong>not</strong> require a database or knowledge in HTML. The theme engine is highly flexible and there are some pre-made designs included.<br />See the <a href="http://photos.erikpoehler.com/"><i class="icon-eye-open"></i> Demo</a> or check out the <a href="/folderblog/themes.php"><i class="icon-circle-arrow-right"></i> Themes</a>.',
    'REQ'		=> 'Requirements',
    'HT'		=> 'Your provider allows .htaccess files',
    'MR'		=> 'You\'ll need mod_rewrite enabled (standard)',
    'PHP'		=> 'PHP 5.3 or greater',
    'XSLT'		=> 'XSLT enabled',
    'OR'		=> 'or',
    'SERVERCHECK' => 'After uploading Folderblog you can open <samp>/folderblog-path/server.php</samp> to see if your server complies with the requirements.<!--<br />You don\'t know if your server has these features? <a href="requirements.php"><i class="icon-info-sign"></i> Find more details here</a>.-->',
    'FEATURES'	=> 'Features',
    'BATCH'		=> 'Batch upload your files.',
	'THEMES'	=> 'Flexible themes. Switch themes with a click.',
    'SEO'		=> 'SEO friendly markup in HTML5.',
    'IMGEDIT'	=> 'Basic image editing (rotate)',
    'UNLALBUMS' => 'Unlimited albums or Categories.',
    'USRS'		=> 'Multiple user accounts.',
    'NODB'		=> 'No database required!',
    'NOJS'		=> 'Fully functional without Javascript.',
    'CACHING'	=> 'Images and thumbnails are automatically cached. Stylesheets and javascripts are also minified.',
    'I18N'		=> 'Supports different languages using translation files. <a href="http://translate.erikpoehler.com/projects/folderblog" title="Translations"><i class="icon-globe"></i> Browse translations</a>.',
    'START'		=> 'Getting Started',
    'INS1'		=> 'Download the tar.gz archive and upack it to your computer',
    'INS2'		=> 'Upload its contents to your webserver (Can be the document root or any subfolder)',
    'INS3'		=> 'Make sure these directories and their subfolders are writebale for Apache:<br /><samp>/cache</samp>, <samp>/folders</samp> and <samp>/lib/data</samp>',
    'INS4'		=> 'Verify server requirements http://yoursite.com/&lt;folderblog-path&gt;/server.php',
    'INS5'		=> 'Now open Folderblog in your browser. http://yoursite.com/&lt;folderblog-path&gt;/',
    'INS6'		=> 'Enter a name for your Photolog and to set a password for <samp>superuser</samp>.',
    'INS7'		=> 'Now you can log into the admin panel at http://yoursite.com/&lt;folderblog-path&gt;/admin/ and start photo blogging.',
    'TITLE_THEMES' => 'Themes',
    'THEMES_DESC' => 'Folderblog allows the use of different themes. They can be found (or added to) <samp>/addons/themes</samp> of your Folderblog installation.',
    'LICENSE'	=> 'License',
    'LICTXT'	=> 'Folderblog is open source, and thus is published under the <a href="http://www.gnu.org/licenses/gpl.html">GNU Public Domain License</a>.<br /> Feel free to use and modify or even re-publish Folderblog as long as you use the same license. If you do so, it would be nice if you drop me a line and include a reference to this site.',
    'SUPPORTTITLE' => 'Support',
    'SUPPORTTXT'=> 'Folderblog comes with no support. Folderblog is open source software, so you are free to improve and modify the code to your needs. If you need a forum or more documentation you can <a href="#" id="support"><i class="icon-envelope"></i> request</a> it.',
    'THANKS'	=> 'Credits',
    'THANKSTXT'	=> 'Folderblog has been developed and maintained by <a href="http://tetto.org">Donald Tetto</a> for many years.<br> This version, however, is a complete rewrite of the software by Erik Pöhler. I sticked to the idea of Folderblog of being easy to use, and not being bloated. Folderblog makes use of other pre-made tools, like the <a href="http://jquery.com/">jQuery</a> Javascript library, <a href="http://phpthumb.sourceforge.net/">phpThumb</a> for all image resizing tasks, and <a href="https://github.com/mrclay/minify">Minify</a> for script compression. Thanks to <a href="http://www.atlassian.com/">Atlassian</a> for supporting this project with a <a href="http://jira.erikpoehler.com/">JIRA</a> license.<br><br>Last but not least, a big "Thank You!" to everybody who uses Folderblog.',
    'BY'		=> 'by',
    'LINKS'		=> 'Links',
    'SBDEMO'	=> 'See a Demo',
    'SBTHEMES'	=> 'Browse Designs',
    'DLFB'		=> 'Download Folderblog',
    'SBLANG'	=> 'Social Translations',
    'WSVN'		=> 'Browse the Source Code',
    'SVN'		=> 'Subversion Access',
    'JIRA'      => 'JIRA (Found a bug? Report it.)',
    'TWIT'		=> 'What’s<i class="blue icon-twitter"></i>new?',
    'DONATE'	=> 'If you like Folderblog, show your appreciation in liquid form.',
    'FOLLOW'	=> 'Follow <a href="http://twitter.com/folderblog">@folderblog</a>',
    'NOEML'		=> 'Oops. No email address provided',
    'EMLINVALID'=> 'Oops. Email address is invalid',
    'SUCCESS'	=> 'Success! Check your email to confirm sign up.',
    'ERROR'		=> 'Error: ',
    'SIGNUP'	=> 'Get updates via newsletter',
    'NOSPAM'	=> 'We\'ll never spam or give this address away',
    'EML'		=> 'Email address',
    'JOIN'		=> 'Join',
    'ALREADY'	=> 'You are already subscribed to the list <strong>Folderblog News & Updates</strong>',
    'ADDING'	=> 'Adding email address...',
    'FBHOME'	=> 'Folderblog',
    'LANGISO'	=> 'en',
    'LOCALE'	=> 'en_US',
	'META_DESCRIPTION' => 'Folderblog is a simple, lightweight photolog platform written in PHP. It does not require a database. It\'s open source and it\'s free.',
	'META_TYPE' => 'software',
    'NAME'		=> 'Name',
	'DEMO'		=> 'Demo',
	'AUTHOR'	=> 'Author',
    'DETAILS'	=> 'Details',
    'SEE_IN_ACTION'	=> '<i class="icon-eye-open"></i> See this theme in action',
);