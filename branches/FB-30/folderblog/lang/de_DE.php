<?php
return array(
    'BACK'      => 'Zurück zu',
    'DL'        => 'Herunterladen (zip, 1MB)<br />',
    'DLCOUNT'   => 'mal heruntergeladen',
	'SEEDEMO'   => 'Demo<br />ansehen',
	'ABOUT'     => 'Was ist Folderblog?',
    'INTRO'		=> 'Folderblog ist ein PHP-basiertes, einfaches CMS für Photologs. Alle Daten werden in einem XML gespeichert mittels XSLT werden HTML-Seiten generiert. Man kann es sowohl als Bildergalerie verwenden oder als Photolog - und was es dazwischen noch so gibt. Die Bedienung ist einfach und intuitiv und die Darstellung ist performant. Man benötigt für Folderblog <strong>kein</strong> Datenbank oder Kentnisse in HTML. Die Themes sind höchst flexibel und einige vorgefertigte Designs sind bereits enthalten.<br />Schau dir die <a href="http://photos.erikpoehler.com/"><i class="icon-eye-open"></i> Demo</a> an oder durchstöber <a href="/folderblog/themes.php"><i class="icon-circle-arrow-right"></i> die Designs</a>.',
	'REQ'		=> 'Serveranforderungen',
    'HT'		=> 'Dein Provider erlaubt .htaccess Dateien',
    'MR'		=> 'mod_rewrite muss verfügbar sein (für "saubere" URLs)',
    'PHP'		=> 'PHP 5.3 oder höher',
    'XSLT'		=> 'XSLT muss aktiviert sein',
    'OR'		=> 'oder',
    'SERVERCHECK' => 'Nachdem du Folderblog auf deinen Webserver hochgeladen hast kannst du <samp>/folderblog-pfad/server.php</samp> im Browser öffnen, um zu sehen ob dein Server mit Folderblog kompatibel ist. <!--Falls etwas nicht klappt, gibt es auch noch <a href="requirements.php"><i class="icon-info-sign"></i> detailliertere Infos</a> zu den Anforderungen.-->',
    'FEATURES'	=> 'Merkmale',
    'BATCH'		=> 'Batch upload für deine Dateien.',
    'THEMES'	=> 'Flexible Designs. Änder das Design per Mausklick.',
    'SEO'		=> 'Für SEO bereites HTML5.',
	'IMGEDIT'	=> 'Einfache Bildbearbeitung (Bilder rotieren)',
    'UNLALBUMS' => 'Unbegrenzte Alben oder Kategorien.',
    'USRS'		=> 'Mehrere Benutzerzugänge',
    'NODB'		=> 'Keine Datenbank erforderlich',
    'NOJS'		=> 'Auch ohne Javaskript voll funktionsfähig',
    'CACHING'	=> 'Alle Bilder werden gecached. Javaskripte und Stylesheets werden zusätzlich komprimiert',
    'I18N'		=> 'Unterstützt jede beliebige Sprache mittels Übersetzungsdateien.<br />Hier kannst du <a href="http://translate.erikpoehler.com/projects/folderblog" title="Übersetzungen"><i class="icon-globe"></i>&nbsp;Übersetzungen suchen oder erstellen</a>.',
	'START'		=> 'Installation',
    'INS1'		=> 'Lade das .zip Archiv herunter, entpacke es auf deinem Computer',
    'INS2'		=> 'Lade den Inhalt auf deinen Webserver (Document Root oder ein beliebiges Unterverzeichnis)',
    'INS3'		=> 'Stelle sicher, dass Apache Schreibrechte für die Verzeichnisse <br /><samp>/cache</samp>, <samp>/folders</samp> und <samp>/lib/data</samp> (einschließlich deren Unterordner) hat',
    'INS4'		=> 'Du kannst im Browser die Adresse http://yoursite.com/&lt;folderblog-path&gt;/server.php aufrufen und sehen ob dein Server den Anforderungen entspricht.',
    'INS5'		=> 'Öffne nun Folderblog in deinem Browser. http://yoursite.com/&lt;folderblog-path&gt;/',
    'INS6'		=> 'Gib einen Title für deinen Photolog an und speicher ein Passwort für den <samp>superuser</samp>.',
    'INS7'		=> 'Du kannst dich jetzt im Administrationsbereich anmelden (http://yoursite.com/&lt;folderblog-path>/admin/) und deinen Photolog mit Bildern füllen.',
    'TITLE_THEMES' => 'Designs',
    'THEMES_DESC' => 'Folderblog erlaubt es verschiedene Designs zu installieren und zu benutzen. Die Designs findest du im Verzeichnis <samp>/addons/themes/</samp> deiner Folderblog-Installation.',
    'LICENSE'	=> 'Lizenz',
    'LICTXT'	=> 'Folderblog ist Open Source Software und steht unter der <a href="http://www.gnu.org/licenses/gpl.html">GNU Public Domain Lizenz</a>.<br /> Du kannst Folderblog benutzen, modifizieren oder sogar weiterverbreiten oder neu publizieren solange du die Lizenz nicht veränderst. Solltest du Folderblog unter anderem Namen neu publizieren, wäre es nett davon zu erfahren.',
    'SUPPORTTITLE' => 'Support',
    'SUPPORTTXT'=> 'Ich biete keinen Support für Folderblog an. Folderblog ist Open Source Software, also hast du alle Freiheiten den Kode zu verbessern oder an deine Bedürfnisse anzupassen. Sollte das gewünscht sein kann man bei mir aber zusätzliche Dokumentation oder beispielsweise ein Forum <a href="#" id="support"><i class="icon-envelope"></i> erfragen</a>.',
    'THANKS'	=> 'Dankeschöns',
    'THANKSTXT'	=> 'Alle Vorgängerversionen von Folderblog wurden von <a href="http://tetto.org">Donald Tetto</a> entwickelt und für viele Jahre gepflegt. Diese Version 4.0 habe ich allerdings "von Grund auf Neu" entwickelt. Immer der Grundidee folgend, dass Folderblog nicht überladen sein darf und einfach zu benutzen sein muss.<br /> Ich habe mich einiger vorgefertigter Tools bedient, die da wären: <a href="http://jquery.com/">jQuery</a> Javaskript Bibliothek, <a href="http://phpthumb.sourceforge.net/">phpThumb</a> für alles was mit Bildern und deren Größe zu tun hat, und <a href="https://github.com/mrclay/minify">Minify</a> um Skripte zu komprimieren. Einen Dank auch an <a href="http://www.atlassian.com/">Atlassian</a>, die die Entwicklung dieses Projekts mit einer <a href="http://jira.erikpoehler.com/">JIRA</a> Lizenz fördern.<br /> <br />Nicht zuletzt auch ein großes "Dankeschön" an jeden der Folderblog benutzt.',
    'BY' 		=> 'von',
    'LINKS'		=> 'Links',
    'SBDEMO'	=> 'Demo ansehen',
    'SBTHEMES'	=> 'Alle Designs',
    'DLFB'		=> 'Folderblog herunterladen',
    'SBLANG'	=> 'Übersetzungen',
    'WSVN'		=> 'Quelltext ansehen',
    'SVN'		=> 'Subversion Zugriff',
    'JIRA'		=> 'JIRA (Bug gefunden? Hier melden.)',
    'TWIT'		=> 'Was gibts<i class="blue icon-twitter"></i>Neues?',
    'DONATE'	=> 'Wenn dir Folderblog gefällt, verleihe deiner Begeisterung Ausdruck - in flüssiger Form.',
    'FOLLOW'	=> 'Folge <a href="http://twitter.com/folderblog">@folderblog</a> bei Twitter',
    'NOEML'		=> 'Oops. Keine E-Mail Adresse angegeben.',
    'EMLINVALID'=> 'Oops. Keine richtige E-Mail-Adresse angegeben.',
    'SUCCESS'	=> 'Glückwunsch! Bitte bestätige dein Abonnement in der Bestätigungs-E-Mail.',
    'ERROR'		=> 'Fehler: ',
    'SIGNUP'	=> 'Newsletter abonnieren',
    'NOSPAM'	=> 'Kein Spam oder Weitergabe deiner E-Mail an Dritte!',
    'EML'		=> 'E-Mail Adresse',
    'JOIN'		=> 'Absenden',
    'ALREADY'	=> 'Du hast dich bereits in der Liste <strong>Folderblog News & Updates</strong> eingetragen.',
    'ADDING'	=> 'Füge E-Mail Adresse hinzu...',
    'FBHOME'	=> 'Folderblog',
    'LANGISO'	=> 'de',
    'LOCALE'	=> 'de_DE',
	'META_DESCRIPTION' => 'Folderblog ist eine einfache Plattform um Bilder zu publizieren. Es ist in PHP geschrieben und benötigt keine Datenbank. Folderblog ist open source und kostenlos.',
	'META_TYPE' => 'software',
    'NAME'		=> 'Name',
	'DEMO'		=> 'Demo',
	'AUTHOR'	=> 'Autor',
    'DETAILS'	=> 'Details',
	'SEE_IN_ACTION'	=> '<i class="icon-eye-open"></i> Dieses Design in Aktion sehen',
);