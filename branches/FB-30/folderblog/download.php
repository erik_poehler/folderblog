<?php
$count = intval(trim(file_get_contents($_SERVER["DOCUMENT_ROOT"]."/folderblog/count.txt")));
$newcount = (int) $count+1;
file_put_contents($_SERVER["DOCUMENT_ROOT"]."/folderblog/count.txt", $newcount);
header("Content-Disposition: attachment; filename=" . urlencode("Folderblog-trunk.r295.tar.gz"));
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Description: File Transfer");
header("Content-Length: " . filesize($_SERVER["DOCUMENT_ROOT"]."/folderblog/src/Folderblog-trunk.r295.tar.gz"));
header("Location: http://erikpoehler.com/folderblog/src/Folderblog-trunk.r295.tar.gz");
exit;
?>