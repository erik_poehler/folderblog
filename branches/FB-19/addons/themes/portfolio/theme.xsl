<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    version="1.0" 
    xmlns="http://www.w3.org/1999/xhtml" 
    xmlns:html="http://www.w3.org/1999/xhtml" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:exsl="http://exslt.org/common" 
    extension-element-prefixes="exsl" 
    exclude-result-prefixes="exsl xsl html">
    
    <xsl:output encoding="UTF-8" indent="no" method="xml" omit-xml-declaration="yes"/>
    <xsl:strip-space elements="*"/>
    <xsl:param name="view"/>
    <xsl:param name="base"/>
    <xsl:param name="folder" />
    <xsl:variable name="myFolder">
        <xsl:choose>
            <xsl:when test="$folder = '/folders'">/folders/featured</xsl:when>
            <xsl:otherwise><xsl:value-of select="$folder" /></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:param name="element"/>
    <xsl:param name="page" />
    <xsl:variable name="current_theme" select="/folderblog/settings/theme"/>
    <xsl:variable name="theme" select="/folderblog/settings/themes/theme[@dirname=$current_theme]"/>
    <xsl:variable name="base_with_slash">
        <xsl:choose>
            <xsl:when test="substring($base,-1) != '/'"><xsl:value-of select="$base"/>/</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$base"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <!-- store some settings in variables, so they can be easily accessed - also outside the dom scope -->
    <xsl:variable name="thumb_maxsize"><xsl:value-of select="/folderblog/settings/thumb_maxsize"/></xsl:variable>
    <xsl:variable name="image_maxwidth"><xsl:value-of select="/folderblog/settings/image_maxwidth" /></xsl:variable>
    <xsl:variable name="crop_images"><xsl:value-of select="/folderblog/settings/crop_images" /></xsl:variable>
    <xsl:variable name="square_thumbs"><xsl:value-of select="/folderblog/settings/square_thumbs" /></xsl:variable>
    <xsl:variable name="show_thumbs"><xsl:value-of select="/folderblog/settings/show_thumbs" /></xsl:variable>
    
    <!-- 
    also store all relevant folders 
    in this case: only first level below /folder
    in alphabetical order
    -->
    <xsl:variable name="myFolders">
        <xsl:for-each select="/folderblog/folder/folders/folder">
            <xsl:sort data-type="text" order="ascending" select="title"/>
            <xsl:copy-of select="." />
        </xsl:for-each>
    </xsl:variable>
    
    <xsl:variable name="myImages">
        <xsl:for-each select="/folderblog//folder[path=$myFolder]//element[type='image']">
            <xsl:sort data-type="text" order="ascending" select="title"/>
            <xsl:copy-of select="." />
        </xsl:for-each>
    </xsl:variable>
    
    <xsl:template match="/">
        <!-- <xsl:value-of select="$folder" /><br />
        <xsl:value-of select="$myFolder" /><br />
        <xsl:value-of select="$element" /><br /> -->
        <xsl:choose>
            <xsl:when test="$view = 'index' and $element != '' and not(exsl:node-set($myImages)/element[slug=$element])">
                <xsl:apply-templates select="folderblog" mode="error404"/>
            </xsl:when>
            <xsl:when test="$view = 'index'">
                <xsl:apply-templates select="folderblog" mode="index"/>
            </xsl:when>
            <xsl:when test="$view = 'page'">
                <xsl:apply-templates select="folderblog" mode="index"/>
            </xsl:when>
            <xsl:when test="$view = 'archive'">
                <xsl:apply-templates select="folderblog" mode="archive"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="folderblog" mode="error404"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="folderblog" mode="index">
        <html lang="{$language}">
            <xsl:variable name="imgNumber">
                <xsl:choose>
                    <xsl:when test="$element != ''">
                        <xsl:for-each select="exsl:node-set($myImages)/element">
                            <xsl:if test="slug = $element">
                                <xsl:value-of select="position()" />
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:otherwise>1</xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <head>
                <xsl:call-template name="css" />
            </head>
            <body class="{$view} no-js" data-folder="{$folder}" data-view="{$view}">
                <!-- <textarea><xsl:copy-of select="." /></textarea> -->
                <div id="page">
                    <img id="logo" src="/addons/themes/portfolio/img/logo2-white.png" />
                    <xsl:if test="$view != '404' and $view != 'archive'">
                        <div id="controls">
                            <xsl:call-template name="pagination">
                                <xsl:with-param name="folder" select="$folder" />
                                <xsl:with-param name="imageNumber" select="$imgNumber" />
                                <xsl:with-param name="recordsPerPage" select="1" />
                                <xsl:with-param name="numberOfRecords" select="count(exsl:node-set($myImages)/element)" />
                                <xsl:with-param name="element" select="''" />
                                <xsl:with-param name="page" select="'1'" />
                            </xsl:call-template>
                        </div>
                    </xsl:if>
                    <div id="content">
                        <div class="container">
                            <h1><xsl:apply-templates select="/folderblog/language/term[@id='ERROR']"/></h1>
                            <p>The page you've been looking for couldn't be found.</p>
                        </div>
                    </div>
                    <div id="navwrapper">
                        <xsl:call-template name="navigation" />
                    </div>
                </div>
                <div id="carousel">
                    <xsl:variable name="allImages">
                        <xsl:value-of select="count(exsl:node-set($myImages)/element)" />
                    </xsl:variable>
                    <xsl:for-each select="exsl:node-set($myImages)/element">
                        <div class="image_wrap" data-path="{path}" data-url="/image/{width}x{height}{path}" data-position="{position()}">
                            <xsl:attribute name="style">
                                z-index:<xsl:value-of select="100 + $allImages + position()" />;
                                <xsl:choose>
                                    <xsl:when test="$imgNumber = position()">display:block;</xsl:when>
                                    <xsl:otherwise>display:none;</xsl:otherwise>
                                </xsl:choose>
                            </xsl:attribute>
                            <img src="/image/{width}x{height}{path}" class="carousel_image">
                                <xsl:attribute name="id"><xsl:if test="$imgNumber = position()">carousel_image</xsl:if></xsl:attribute>
                            </img>
                            <span class="about text-center"><i class="icon-camera-retro"><xsl:text> </xsl:text></i><strong><xsl:value-of select="exsl:node-set($myImages)/element[1]/title" /></strong>: <xsl:value-of disable-output-escaping="yes" select="exsl:node-set($myImages)/element[1]/description" /></span>
                        </div>
                    </xsl:for-each>
                </div>
                <div id="spinner"><i class="icon-spinner icon-spin icon-4x"><xsl:text> </xsl:text></i></div>
                <xsl:call-template name="js"/>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="folderblog" mode="archive">
        <html lang="{$language}">
            <head>
                <xsl:call-template name="css" />
            </head>
            <body class="{$view} no-js" data-folder="{$folder}" data-view="{$view}">
                <div id="page">
                    <img id="logo" src="/addons/themes/portfolio/img/logo2.png" />
                    <div id="content">
                        <div class="container">
                            <img src="/addons/themes/portfolio/img/logo2.png" />
                            <h1>Archive</h1>
                        </div>
                    </div>
                    <div id="navwrapper">
                        <xsl:call-template name="navigation" />
                    </div>
                </div>
                <xsl:call-template name="js"/>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="folderblog" mode="error404">
        <html lang="{$language}">
            <head>
                <xsl:call-template name="css" />
            </head>
            <body class="{$view} no-js" data-folder="{$folder}" data-view="{$view}">
                <div id="page">
                    <img id="logo" src="/addons/themes/portfolio/img/logo2-white.png" />
                    <div id="controls">
                        <a id="previous" class="inactive cyan" href="#previous"><i class="icon-angle-left"><xsl:text> </xsl:text></i></a>
                        <span id="count"><span id="current">01</span> / <span id="all">109</span></span>
                        <a id="next" class="magento" href="#next"><i class="icon-angle-right"><xsl:text> </xsl:text></i></a>
                    </div>
                    <div id="content">
                        <div class="container">
                            <h1><xsl:apply-templates select="/folderblog/language/term[@id='ERROR']"/></h1>
                            <p>The page you've been looking for couldn't be found.</p>
                        </div>
                    </div>
                    <span id="about" class="text-center"><i class="icon-camera-retro"><xsl:text> </xsl:text></i><strong>Example Shoot</strong>: photographer <a href="http://example.com">John Doe</a>, model <a href="http://example.com">Sophia Ducoing</a>, stylist <a href="http://example.com">Mary Jane</a>.</span>
                    <div id="navwrapper">
                        <xsl:call-template name="navigation" />
                    </div>
                </div>
                <div id="carousel">
                    <img id="carousel_image" src="/addons/themes/portfolio/img/df.png" />
                </div>
                <xsl:call-template name="js"/>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template name="navigation">
        <nav id="mainnav">
            <a id="brand" class="text-left" href="/"><xsl:value-of select="/folderblog/settings/title" /><small><sup>™</sup></small></a>
            <ul class="social inline text-left">
                <li class="magento"><a href="http://twitter.com/folderblog"><i class="icon-twitter"><xsl:text> </xsl:text></i></a></li>
                <li class="cyan"><a href="https://facebook.com/folderblog/"><i class="icon-facebook"><xsl:text> </xsl:text></i></a></li>
                <xsl:if test="string-length(/folderblog/settings/comment_email)>1"><li class="green"><a href="mailto:{/folderblog/settings/comment_email}"><i class="icon-envelope-alt"><xsl:text> </xsl:text></i></a></li></xsl:if>
            </ul>
            <ul class="inline text-right">
                <xsl:variable name="color_count" select="count(/folderblog/settings/themes/theme[@dirname=/folderblog/settings/theme]/colors/color)" />
                <xsl:if test="$myFolder != '/folders/featured'">
                <li>
                    <xsl:attribute name="class">
                        <xsl:value-of select="/folderblog/settings/themes/theme[@dirname=/folderblog/settings/theme]/colors/color[position()= ($color_count - (0 mod $color_count))]" />
                    </xsl:attribute>
                    <a href="{$base}">Home</a>
                </li>
                </xsl:if>
                <xsl:for-each select="/folderblog/folder/folders/folder[not(path = '/folders/featured')]">
                    <xsl:variable name="folder_count" select="count(/folderblog/folder/folders/folder)" />
                    <xsl:variable name="test">
                        <xsl:value-of select="position() div $color_count" />
                    </xsl:variable>
                    <xsl:variable name="pos" select="position()" />
                    <xsl:if test="count(elements/element)>0">
                        <li>
                            <xsl:attribute name="class">
                                <xsl:value-of select="/folderblog/settings/themes/theme[@dirname=/folderblog/settings/theme]/colors/color[position()= ($color_count - ($pos mod $color_count))]" />
                                <xsl:if test="path=$folder"> active</xsl:if>
                            </xsl:attribute>
                            <a href="{path}/">
                            <xsl:attribute name="class">
                                
                            </xsl:attribute>
                            <xsl:value-of select="title" /></a>
                        </li>
                    </xsl:if>
                </xsl:for-each>
            </ul>
        </nav>
    </xsl:template>
    
    <xsl:template name="css">
        <xsl:choose>
            <xsl:when test="/folderblog/settings/minify_css='1'">
                <link media="all" rel="stylesheet" type="text/css">
                    <xsl:attribute name="href"><xsl:value-of select="$base_with_slash"/>lib/php/min/?f=<xsl:for-each select="$theme/styles/style"><xsl:value-of select="$base_with_slash"/>addons/themes/<xsl:value-of select="$current_theme"/>/<xsl:value-of select="."/><xsl:if test="position() &lt; count($theme/styles/style)">,</xsl:if></xsl:for-each></xsl:attribute>
                </link>
            </xsl:when>
            <xsl:otherwise>
                <xsl:for-each select="$theme/styles/style">
                    <link href="{$base_with_slash}addons/themes/{$current_theme}/{.}" media="all" rel="stylesheet" type="text/css"/>
                </xsl:for-each>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="js">
        <script defer="defer" type="text/javascript">var base='<xsl:value-of select="$base_with_slash"/>';</script>
        <xsl:choose>
            <xsl:when test="/folderblog/settings/minify_js='1'">
                <script defer="defer" type="text/javascript">
                    <xsl:attribute name="src">
                        <xsl:value-of select="$base_with_slash"/>lib/php/min/?f=<xsl:for-each select="$theme/scripts/script"><xsl:value-of select="$base_with_slash"/>addons/themes/default/<xsl:value-of select="."/><xsl:if test="position() &lt; count($theme/scripts/script)">,</xsl:if></xsl:for-each>
                    </xsl:attribute>
                    <xsl:comment/>
                </script>
            </xsl:when>
            <xsl:otherwise>
                <xsl:for-each select="$theme/scripts/script">
                    <script defer="defer" src="{$base_with_slash}addons/themes/{$current_theme}/{.}" type="text/javascript">
                        <xsl:comment>
                        </xsl:comment>
                    </script>
                </xsl:for-each>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:if test="string-length(/folderblog/settings/google_analytics)>0">
            <script type="text/javascript" async="" src="http://www.google-analytics.com/ga.js"><xsl:text> </xsl:text></script>
            <script defer="defer" type="text/javascript">var _gaq=_gaq||[];_gaq.push(["_setAccount","<xsl:value-of select="/folderblog/settings/google_analytics" />"]);_gaq.push(["_trackPageview"]);(function(){var b=document.createElement("script");b.type="text/javascript";b.async=true;b.src=("https:"==document.location.protocol?"https://ssl":"http://www")+".google-analytics.com/ga.js";var a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(b,a)})();</script>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="pagination">
        <xsl:param name="folder"/>
        <xsl:param name="imageNumber"/>
        <xsl:param name="recordsPerPage"/>
        <xsl:param name="numberOfRecords"/>
        <xsl:param name="element"/>
        <xsl:param name="page"/>
        <div class="pagination" id="pagination">
            <!-- <textarea><xsl:copy-of select="$myImages" /></textarea> -->
            <!-- <xsl:for-each select="exsl:node-set($myImages)/element"> -->
                <xsl:choose>
                    <xsl:when test="$element = '' and $folder = ''"></xsl:when>
                    <xsl:when test="$element != ''"></xsl:when>
                    <xsl:otherwise>
                        <xsl:choose>
                            <xsl:when test="$imageNumber = 1">
                                <span id="previous" class="cyan inactive">
                                    <i class="icon-angle-left"><xsl:text> </xsl:text></i>
                                </span>
                            </xsl:when>
                            <xsl:otherwise>
                                <a id="previous" href="{$myFolder}/e/{exsl:node-set($myImages)/element[$imageNumber - 1]/slug}/" class="cyan">
                                    <i class="icon-angle-left"><xsl:text> </xsl:text></i>
                                </a>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose>
                <span id="count"><span id="current"><xsl:value-of select="$imageNumber" /></span> / <span id="all"><xsl:value-of select="$numberOfRecords" /></span></span>
                <xsl:choose>
                    <xsl:when test="$element = '' and $folder = ''"></xsl:when>
                    <xsl:when test="$element != ''"></xsl:when>
                    <xsl:otherwise>
                        <xsl:choose>
                            <xsl:when test="$imageNumber = count(exsl:node-set($myImages)/element)">
                                <span id="next" class="magento  inactive">
                                    <i class="icon-angle-right"><xsl:text> </xsl:text></i>
                                </span>
                            </xsl:when>
                            <xsl:otherwise>
                                <a id="next" href="{$myFolder}/e/{exsl:node-set($myImages)/element[$imageNumber + 1]/slug}/" class="magento">
                                    <i class="icon-angle-right"><xsl:text> </xsl:text></i>
                                </a>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose>
            
        </div>
    </xsl:template>
</xsl:stylesheet>