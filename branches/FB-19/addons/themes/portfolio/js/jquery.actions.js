jQuery(document).ready(function(){
    //set default view mode
    $defaultViewMode="full"; //full (fullscreen background), fit (fit to window), original (no scale)
    //cache vars
    $bg = $("#carousel");
    /*$("#about").animate({opacity:0},500,'',function(){
        $(this).css('opacity',1);
    });*/
    $bg.height($(window).height() - 45);
    $bgimg = $("#carousel #carousel_image");
    
    $("a#next").click(function(){
        
    });

$(window).load(function() {
    //resize browser window functions
    $(window).resize(function() {
        CarouselHeight();
        FullScreenBackground("img.carousel_image"); //scale bg image
    });
    LargeImageLoad($bgimg);
});
    
    //loading bg image
    $bgimg.load(function() {
        LargeImageLoad($(this));
    });
    function CarouselHeight() {
        $bg.height($(window).height() - 45);
    }
    function LargeImageLoad($this){
        $this.css({opacity:0});
        $this.removeAttr("width").removeAttr("height").css({ width: "", height: "" }); //lose all previous dimensions in order to rescale new image data
        $bg.data("originalImageWidth",$this.width()).data("originalImageHeight",$this.height());
        if($bg.data("newTitle")){
            $this.attr("title",$bg.data("newTitle")); //set new image title attribute
        }
        FullScreenBackground($this); //scale new image
        if(typeof itemIndex!="undefined"){
            if(itemIndex==lastItemIndex){ //check if it is the last image
                $bg.data("lastImageReached","Y");
                $bg.data("nextImage",$outer_container_a.first().attr("href")); //get and store next image
            } else {
                $bg.data("lastImageReached","N");
            }
        } else {
            $bg.data("lastImageReached","N");
        }
        $this.animate({opacity:1}); //fadein background image
        $("#spinner").fadeOut();
        if($bg.data("nextImage") || $bg.data("lastImageReached")=="Y"){ //don't close thumbs pane on 1st load
            SlidePanels("close"); //close the left pane
        }
        //NextImageTip();
    }

    //clicking on large image loads the next one
    $bgimg.click(function(event){
        var $this=$(this);
        if($bg.data("nextImage")){ //if next image data is stored
            $this.css("display","none");
            $preloader.fadeIn("fast"); //show preloader
            $($outer_container.data("selectedThumb")).children(".selected").css("display","none"); //deselect thumb
            if($bg.data("lastImageReached")!="Y"){
                $($outer_container.data("selectedThumb")).a().children(".selected").css("display","block"); //select new thumb
            } else {
                $outer_container_a.first().children(".selected").css("display","block"); //select new thumb - first
            }
            //store new selected thumb
            var selThumb=$outer_container.data("selectedThumb");
            if($bg.data("lastImageReached")!="Y"){
                $outer_container.data("selectedThumb",$(selThumb).next()); 
            } else {
                $outer_container.data("selectedThumb",$outer_container_a.first()); 
            }
            $bg.data("newTitle",$($outer_container.data("selectedThumb")).children("img").attr("title")); //get and store new image title attribute
            if($bg.data("lastImageReached")!="Y"){
                itemIndex++;
            } else {
                itemIndex=0;
            }
            $this.attr("src", "").attr("src", $bg.data("nextImage")); //switch image
        }
    });
    
    //function to get element index (fuck you IE!)
    function getIndex(theItem){
        for ( var i = 0, length = $outer_container_a.length; i < length; i++ ) {
            if ( $outer_container_a[i] === theItem ) {
                return i;
            }
        }
    }


//Image scale function
function FullScreenBackground(theItem){
    var winWidth=$(window).width();
    var winHeight=$(window).height() - 45;
    var imageWidth=$(theItem).width();
    var imageHeight=$(theItem).height();
    
        $(theItem).removeClass("with_border").removeClass("with_shadow"); //remove extra styles of orininal view mode
        var picHeight = imageHeight / imageWidth;
        var picWidth = imageWidth / imageHeight;
        
            if ((winHeight / winWidth) < picHeight) {
                $(theItem).css("width",winWidth).css("height",picHeight*winWidth);
          //      console.log('height:'+picHeight*winWidth);
            } else {
                $(theItem).css("height",winHeight).css("width",picWidth*winHeight);
            //    console.log('width:'+picWidth*winHeight);
            };
        
        //center it
        $(theItem).css("margin-left",((winWidth - $(theItem).width())/2)).css("margin-top",((winHeight - $(theItem).height())/8));
}

/*preload script images
var images=["ajax-loader_dark.gif","round_custom_scrollbar_bg_over.png"];
$.each(images, function(i) {
  images[i] = new Image();
  images[i].src = this;
}); */

});