<?php
/**
 *
 * @package Folderbog 4.0
 * @author Erik Pöhler
 * @version 4.0
 * @see http://erikpoehler.com/folderblog/
 * @desc AJAX Helper to rotate images in clockwise 90° steps
 *
 */

require_once "../../../config.php";

function __autoload($cn) {
    if (is_file($_SERVER["DOCUMENT_ROOT"] . FB_BASE . "/lib/php/$cn.php")) require_once $_SERVER["DOCUMENT_ROOT"].FB_BASE."/lib/php/$cn.php";
}

session_start();
$response = new AjaxResponse();
session_write_close();

if (isset($_GET["file"]) && strlen(trim($_GET["file"]))>0) {
    $file = trim(strip_tags($_GET["file"]));
    if (is_file($_SERVER["DOCUMENT_ROOT"] . FB_BASE . $file)) {
        // File and rotation
        try {
            $filename = $_SERVER["DOCUMENT_ROOT"] . FB_BASE . $file;
            $angle = 360 - 90;
            setMemoryForImage($filename);
            $source = imagecreatefromjpeg($filename);
            $rotate = imagerotate($source, $angle, 0);
            imagejpeg($rotate, $filename, 100); // save the image
            imagedestroy($rotate); // clear memory
            $dom = new DOMDocument();
            if (FB_PROFILE === "on") { file_put_contents($_SERVER["DOCUMENT_ROOT"] . FB_BASE . '/php-profiling.txt', microtime()." - read - folderblog.xml - ".basename(__FILE__)."\n", FILE_APPEND); }
            if($dom->load($_SERVER["DOCUMENT_ROOT"] . FB_BASE.'/lib/data/folderblog.xml')) {
                $xpath = new DOMXPath($dom);
                $results = $xpath->query("//element[path='$file']");
                if ($results->length === 0) {
                    $response->setSuccess(false)->setMessage("Element could not be found in folderblog.xml.");
                } else {
                    $node = $results->item(0);
                    $width = $node->getElementsByTagName("width")->item(0)->nodeValue;
                    $height = $node->getElementsByTagName("height")->item(0)->nodeValue;
                    $node->getElementsByTagName("width")->item(0)->nodeValue = $height;
                    $node->getElementsByTagName("height")->item(0)->nodeValue = $width;
                    Frontend::saveData($dom->saveXML());
                    $response->setSuccess(true)->setMessage("DOM was saved");
                }
            } else {
                $response->setSuccess(false)->setMessage("Problem loading folderblog.xml.");
            }
        } catch (Exception $e) {
            $response->setSuccess(false)->setMessage($e->getMessage());
        }
    } else {
        $response->setSuccess(false)->setMessage("Error. Element not found.");
    }
    header('Content-type: text/xml; charset=utf8');
    echo $response->createResponse();
}

function setMemoryForImage( $filename ){
    $imageInfo = getimagesize($filename);
    $MB = 1048576;  // number of bytes in 1M
    $K64 = 65536;    // number of bytes in 64K
    $TWEAKFACTOR = 8;  // Or whatever works for you
    $memoryNeeded = round( ( $imageInfo[0] * $imageInfo[1]
            * $imageInfo['bits']
            * $imageInfo['channels'] / 8
            + $K64
    ) * $TWEAKFACTOR
    );
    $memoryLimit = 8 * $MB;
    if (function_exists('memory_get_usage') && memory_get_usage() + $memoryNeeded > $memoryLimit) {
        $newLimit = $memoryLimitMB + ceil( ( memory_get_usage() + $memoryNeeded - $memoryLimit ) / $MB);
        ini_set( 'memory_limit', $newLimit . 'M' );
        return true;
    } else {
        return false;
    }
}