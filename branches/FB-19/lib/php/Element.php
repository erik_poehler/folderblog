<?php
/**
 *
 * @package Folderbog 4.0
 * @author Erik Pöhler
 * @version 4.0
 * @see http://erikpoehler.com/folderblog/
 *
 */
class Element extends Frontend {
    public $element = null;
    public $folder = null;
    public $file = null;
    public $xml;

    /**
     *
     * @param string $dir
     * @param string $filename
     * @throws FolderblogException
     * @return void
     */
    public function __construct($dir, $filename) {
        parent::__construct();
        try {
            $this->folder = rtrim($dir,'/');
            if (!is_writeable($_SERVER["DOCUMENT_ROOT"].FB_BASE.$this->folder)) {
                throw new FolderblogException("Nope. Folder is not writeable.");
            }
            $this->file = $this->folder.'/'.strtolower(StringFunctions::getFilename(pathinfo($filename,PATHINFO_FILENAME))).".".strtolower(pathinfo($filename,PATHINFO_EXTENSION));
            // load data xml if not present already
            if (is_null($this->xml)) {
                $this->xml = DOMDocument::loadXML(file_get_contents($_SERVER["DOCUMENT_ROOT"].FB_BASE."/lib/data/folderblog.xml"));
            }
            if (is_dir($_SERVER["DOCUMENT_ROOT"].FB_BASE.$this->folder) && is_file($_SERVER["DOCUMENT_ROOT"].FB_BASE.$this->file)) {
                // safety backup
                $this->backupData();
                $this->determineFiletype();
            } else {
                throw new FolderblogException ("Folder or file not found ". $this->file);
            }
        } catch (FolderblogException $e) {
            $e->displayError();
        }
    }

    /**
     * @return void
     */
    public function determineFiletype() {
        if (class_exists('finfo')) {
            $finfo = new finfo(FILEINFO_MIME);    // object oriented approach!
        } else {
            $finfo = mime_content_type($_SERVER["DOCUMENT_ROOT"].FB_BASE.$this->file);
        }
        $mime_type = $finfo->buffer(file_get_contents($_SERVER["DOCUMENT_ROOT"].FB_BASE.$this->file));  // e.g. gives "image/jpeg"
        $mime_type = explode(';',$mime_type);
        $mime_type = $mime_type[0];
        switch($mime_type) {
            // audio
            case "audio/mpeg":
            case "audio/mp3":
                $this->addAudio();
                break;

            // videos
            case "video/mpeg":
                $this->addVideo();
                break;

            // images
            case "image/png":
            case "image/jpeg":
                $this->addImage();
                break;
            // archives
            case "application/x-gtar":
            case "application/x-compressed":
            case "application/x-gzip":
            case "multipart/x-gzip":
            case "application/x-compressed":
            case "application/x-zip-compressed":
            case "application/zip":
            case "multipart/x-zip":
                // archive
                $this->extractArchive();
                break;
            default:
                die("filetype didnt match $mime_type");
                break;
        }
    }

    /**
     *
     * @desc Adds a new image element to the DOM
     * @return void
     */
    public function addImage() {
        $dom = new DOMDocument();
        $dom->load($_SERVER["DOCUMENT_ROOT"].FB_BASE."/lib/data/folderblog.xml");
        $xpath = new DOMXPath($dom);
        // making sure elements container exists
        $results = $xpath->query("//folder[path='".$this->folder."']/elements[1]");
        if ($results->length > 0) {
            $folder = $results->item(0);
            $file = $_SERVER["DOCUMENT_ROOT"].FB_BASE.$this->file;
            $size = getimagesize($file, $info);
            $filesize = filesize($file);
            if (function_exists('exif_read_data')) {
                $exif = @exif_read_data($file);
            }
            if (isset($info["APP13"]) && function_exists('iptcparse')) {
                $iptc = iptcparse($info["APP13"]);
            }

            /**
                Exif Array:
                [FileName] =&gt; img_0412.jpg
                [FileDateTime] =&gt; 1331565884
                [FileSize] =&gt; 105319
                [FileType] =&gt; 2
                [MimeType] =&gt; image/jpeg
                [SectionsFound] =&gt; ANY_TAG, IFD0, EXIF
                [COMPUTED] =&gt; Array
                    (
                        [html] =&gt; width=&quot;1280&quot; height=&quot;853&quot;
                        [Height] =&gt; 853
                        [Width] =&gt; 1280
                        [IsColor] =&gt; 1
                        [ByteOrderMotorola] =&gt; 1
                        [CCDWidth] =&gt; 8mm
                        [ApertureFNumber] =&gt; f/3.5
                    )

                [Make] =&gt; Canon
                [Model] =&gt; Canon EOS REBEL T1i
                [Orientation] =&gt; 1
                [XResolution] =&gt; 72/1
                [YResolution] =&gt; 72/1
                [ResolutionUnit] =&gt; 2
                [Software] =&gt; QuickTime 7.7.1
                [DateTime] =&gt; 2011:12:18 20:07:18
                [HostComputer] =&gt; Mac OS X 10.7.2
                [Exif_IFD_Pointer] =&gt; 228
                [ExposureTime] =&gt; 1/60
                [FNumber] =&gt; 7/2
                [ExposureProgram] =&gt; 2
                [ISOSpeedRatings] =&gt; 400
                [ExifVersion] =&gt; 0220
                [DateTimeOriginal] =&gt; 2011:12:18 03:09:47
                [DateTimeDigitized] =&gt; 2011:12:18 03:09:47
                [ComponentsConfiguration] =&gt; 
                [ShutterSpeedValue] =&gt; 6/1
                [ApertureValue] =&gt; 29/8
                [ExposureBiasValue] =&gt; 0/1
                [MaxApertureValue] =&gt; 906/247
                [MeteringMode] =&gt; 5
                [Flash] =&gt; 9
                [FocalLength] =&gt; 35/1
                [SubSecTime] =&gt; 99
                [SubSecTimeOriginal] =&gt; 99
                [SubSecTimeDigitized] =&gt; 99
                [FlashPixVersion] =&gt; 0100
                [ColorSpace] =&gt; 1
                [ExifImageWidth] =&gt; 1280
                [ExifImageLength] =&gt; 853
                [FocalPlaneXResolution] =&gt; 135302/35
                [FocalPlaneYResolution] =&gt; 283629/73
                [FocalPlaneResolutionUnit] =&gt; 2
                [CustomRendered] =&gt; 0
                [ExposureMode] =&gt; 0
                [WhiteBalance] =&gt; 0
                [SceneCaptureType] =&gt; 0
            )
             */

            // create new element
            $element = $dom->createElement("element");

            $element->appendChild($dom->createElement("type", "image"))
            ->parentNode->appendChild($dom->createElement("slug", $this->getUniqueSlug($this->file)))
            ->parentNode->appendChild($dom->createElement("path", $this->file))
            ->parentNode->appendChild($dom->createElement("title"))
            ->parentNode->appendChild($dom->createElement("description"))
            ->parentNode->appendChild($dom->createElement("uploaded", date("YmdHis")))
            ->parentNode->appendChild($dom->createElement("credits", ""))
            ->parentNode->appendChild($dom->createElement("width", $size[0]))
            ->parentNode->appendChild($dom->createElement("height", $size[1]))
            ->parentNode->appendChild($dom->createElement("mime", $size["mime"]))
            ->parentNode->appendChild($dom->createElement("filesize", $filesize))
            ->parentNode->appendChild($dom->createElement("comments"))
            ->parentNode->appendChild($dom->createElement("tags"));
            if (is_array($exif)){
                if (isset($exif["Model"]) && strlen($exif["Model"])>0) {
                    $element->appendChild($dom->createElement("model", $exif["Model"]));
                }
                if (isset($exif["DateTimeOriginal"]) && strlen($exif["DateTimeOriginal"])>0) {
                    $element->appendChild($dom->createElement("taken", date("YmdHis", strtotime($exif["DateTimeOriginal"]))));
                }
                if (isset($exif["ExposureTime"]) && strlen($exif["ExposureTime"])>0) {
                    $element->appendChild($dom->createElement("exposure", $exif["ExposureTime"]));
                }
                if (isset($exif["ExposureTime"]) && strlen($exif["ExposureTime"])>0) {
                    $element->appendChild($dom->createElement("focallength", $exif["FocalLength"]));
                }
            }
            $folder->appendChild($element);
            $this->saveData($dom->saveXML());
        }
    }

    /**
     * Adds a new video element to DOM
     * @return void
     */
    public function addVideo() {
        $xpath = new DOMXPath($this->xml);
        $results = $xpath->query("//folder[path='".$this->folder."']/elements[1]");
        if ($results->length > 0) {
            $folder = $results->item(0);

            // create new element
            $element = $this->xml->createElement("element");
            $element->appendChild($this->xml->createElement("type", "video"));
            $element->appendChild($this->xml->createElement("path", $this->file));
            $element->appendChild($this->xml->createElement("title", ""));
            $element->appendChild($this->xml->createElement("description", ""));
            $element->appendChild($this->xml->createElement("created", date("YmdHis")));
            $element->appendChild($this->xml->createElement("credits", ""));
            $element->appendChild($this->xml->createElement("comments"));
            $element->appendChild($this->xml->createElement("tags"));
            // append new element to DOM
            $folder->appendChild($element);


            $this->saveData($this->xml->saveXML());
        }
    }

    /**
     * Adds a new audio element to DOM
     * @return void
     */
    public function addAudio() {
        $xpath = new DOMXPath($this->xml);
        $results = $xpath->query("//folder[path='".$this->folder."']/elements[1]");
        if ($results->length > 0) {
            $folder = $results->item(0);

            // create new element
            $element = $this->xml->createElement("element");
            $element->appendChild($this->xml->createElement("type", "audio"));
            $element->appendChild($this->xml->createElement("path", $this->file));
            $element->appendChild($this->xml->createElement("title", ""));
            $element->appendChild($this->xml->createElement("description", ""));
            $element->appendChild($this->xml->createElement("created", date("YmdHis")));
            $element->appendChild($this->xml->createElement("credits", ""));
            $element->appendChild($this->xml->createElement("comments"));
            $element->appendChild($this->xml->createElement("tags"));
            // append new element to DOM
            $folder->appendChild($element);

            $this->saveData($this->xml->saveXML());
        }
    }

    /**
     * @desc makes sure the slug for newly added files is unique (if the file is a dup, appends an autoincrement)
     * @param unknown_type $str
     * @return string
     */
    public function getUniqueSlug($str) {
        // step 1: get the name without extension
        $filename = pathinfo($str, PATHINFO_FILENAME);
        $name = $filename;
        $file = basename($str);

        // step 2: has the same slug been used before?
        $dom = new DOMDocument();
        if (FB_PROFILE === "on") { file_put_contents($_SERVER["DOCUMENT_ROOT"] . FB_BASE . '/php-profiling.txt', microtime()." - read - folderblog.xml - ".basename(__FILE__)."\n", FILE_APPEND); }
        $dom->load($_SERVER["DOCUMENT_ROOT"].FB_BASE."/lib/data/folderblog.xml");
        $xpath = new DOMXPath($dom);
        $res = $xpath->query("//element[contains(path,'$file')]");
        if ($res->length > 0) {
            // if we find more than one here, it means there are images with the same
            // filename within different folders.
            $matches = array();
        	foreach($res as $node) {
                $matches[$node->getElementsByTagName("uploaded")->item(0)->nodeValue] = array(
                    "filename"    => basename($node->getElementsByTagName("path")->item(0)->nodeValue),
                    "slug"        => $node->getElementsByTagName("slug")->item(0)->nodeValue,
                    );
            }
            $count = count($matches);
            $name = $name . "-" . ($count + 1);
        }
        return $name;
    }

    /**
     * Extracts an archive to the filesystem
     * @return void
     */
    public function extractArchive() {
    }

}