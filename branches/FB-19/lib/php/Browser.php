<?php
/**
 *
 * @package Folderbog 4.0
 * @author Erik Pöhler
 * @version 4.0
 * @see http://erikpoehler.com/folderblog/
 *
 */
class Browser extends Frontend {
    /**
     * @desc saves descriptions and titles whenever someone saves this in the browser
     * @return void
     */
    public function updateElements() {
        if ($_POST["action"] === "elementdetails" && count($_POST["details"]) > 0 ) {
            $array = $_POST["details"];
                $xml = new DOMDocument();
                $xml->load($_SERVER["DOCUMENT_ROOT"].FB_BASE."/lib/data/folderblog.xml");
                foreach($array as $element) {
                    $xpath = new DOMXPath($xml);
                    $path = str_replace(FB_BASE,"",$element["path"]);
                    $result = $xpath->query("//element[path='$path'][1]");
                    if ($result->length > 0) {
                        $title = $xml->createCDATASection(trim($element["title"]));
                        $desc = $xml->createCDATASection(trim($element["description"]));
                        $result->item(0)->getElementsByTagName("slug")->item(0)->nodeValue = $this->getUniqueSlug(trim($element["title"]), $element["path"], $xml);
                        $result->item(0)->getElementsByTagName("title")->item(0)->nodeValue = "";
                        $result->item(0)->getElementsByTagName("title")->item(0)->appendChild($title);
                        $result->item(0)->getElementsByTagName("description")->item(0)->nodeValue = "";
                        $result->item(0)->getElementsByTagName("description")->item(0)->appendChild($desc);
                    } else {
                        throw new FolderblogException("I couldn't even find the image...");
                    }
                }
                $this->backupData();
                $this->saveData($xml->saveXML());
                header("Location: ".FB_BASE."/admin/#browser");
                exit;
        }
    }

    /**
     * @desc makes sure the slug for newly added files is unique (if the file is a dup, appends an autoincrement)
     * @param string $str
     * @return string
     */
    public function getUniqueSlug($title, $path, $dom) {
        $name = StringFunctions::getFilename($title);
        $xpath = new DOMXPath($dom);
        // searching for matches on the slug not on the title, because there might be images without title but same filename as current title
        $nodelist = $xpath->query("//element[title = '$title']");
        $nodes = 0;
        $matches = array();
        if ($nodelist->length > 0) {
            foreach ($nodelist as $node) {
                $matches[$node->getElementsByTagName("uploaded")->item(0)->nodeValue] = array(
                    "path" => $node->getElementsByTagName("path")->item(0)->nodeValue,
                    'slug'    => $node->getElementsByTagName("slug")->item(0)->nodeValue
                );
            }
            ksort($matches);
            $i = 1;
            foreach($matches as $match) {
                if ($match["path"] === $path) {
                    if ($i > 1) {
                        $name = $name."-".$i;
                    } else {
                        $name = $name;
                    }
                }
                $i++;
            }
            return $name;
        } else {
            return $name;
        }
    }
}