<?php
/**
 * Folderblog 4.0
 * @author Erik Pöhler
 * @version 4.0
 * @see http://folderblog.org/
 */

require_once "config.php";

/**
 * Magic function __autoload()
 * @param string $class name of a class to be called.
 * @return void
 */
function __autoload($class) {
	if (is_file($_SERVER["DOCUMENT_ROOT"].FB_BASE."/lib/php/$class.php")) require_once $_SERVER["DOCUMENT_ROOT"].FB_BASE."/lib/php/$class.php";
}

if (FB_PROFILE === "on") { file_put_contents($_SERVER["DOCUMENT_ROOT"] . FB_BASE . '/php-profiling.txt', "--- ".$_SERVER["REQUEST_URI"]. "---\n", FILE_APPEND); }
if (FB_PROFILE === "on") {
	$debug = new DebugUtilities();
	$debug->startDuration();
}

/**
 * Initialize the App
 * loads a Folderblog instance, and shows the frontend
 */
try {
    Folderblog::getInstance()->run();
} catch (FolderblogException $e) {
    $e->displayError();
}

if (FB_PROFILE === "on") {
	echo "<pre id=\"profile\">Memory Usage: ".FilesystemFunctions::bytesToSize(memory_get_usage(),2)."\n";
	echo "Memory Peak Usage: ".FilesystemFunctions::bytesToSize(memory_get_peak_usage(),2)."\n";
	echo "Included Files: ".count(get_included_files())."\n";
	foreach (get_included_files() as $file) {
		echo "\t".str_replace($_SERVER["DOCUMENT_ROOT"],"",$file)."\n";
	}
	$debug->endDuration();
	echo $debug->showDurationStatistics();
	echo "</pre>";
}
?>