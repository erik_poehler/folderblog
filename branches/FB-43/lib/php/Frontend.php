<?php
/**
 *
 * @package Folderbog 4.0
 * @author Erik Pöhler
 * @version 4.0
 * @see http://erikpoehler.com/folderblog/
 *
 */
class Frontend {
    /**
     *
     * @desc type of expected response
     * @var string $response_type
     */
    private $response_type = 'html'; // one of html, xml, json

    /**
     *
     * @desc xml data
     * @var DOMDocument $xml
     */
    public $xml = null;

    /**
     *
     * @desc xsl stylesheet (either theme.xsl or folderblog.xsl in admin cp)
     * @var DOMDocument $xsl
     */
    private $xsl = null;

    /**
     * @desc Array of parameters to be passed to the XSLTProcessor
     * @var array $params
     */
    private $params = null;
    private $html = null;
    private $preferred_language = 'en';
    private $default_language = 'en';
    private $language;

    /**
     *
     * @desc Loads the XML and XSL according to the context and allows to manipulate the DOM where required (eg: error messages in admin cp, checking for available themes, etc)
     * @return void
     */
    public function __construct() {
        $dom = new DOMDocument();
        $dom->loadXML(Folderblog::getInstance()->xml);
        $this->xml = $dom;
        $prts = explode('/',substr(str_replace(FB_BASE,"",$_SERVER["REQUEST_URI"]),1));
        $settings = new Settings();
        $this->preferred_language = $settings->getSetting("preferred_language");
        //var_dump($this->preferred_language);
        $this->default_language = $settings->getSetting("default_language");
        //var_dump($this->default_language);
        $this->getDynamicData($prts);
        $this->xsl = $this->loadXSL($prts);
        $this->params = $this->getParams($prts);
    }

    /**
     * @desc load dynamic data into the XML where needed (for example: available themes)
     * @return void
     */
    public function getDynamicData($prts) {
        // Theme Details
        $themes = FilesystemFunctions::rglob($_SERVER["DOCUMENT_ROOT"].FB_BASE.'/addons/themes/*', GLOB_ONLYDIR);
        $themesNode = $this->xml->createElement("themes");
        foreach ($themes as $theme) {
            if (is_file($theme."/theme.xml")) {
                $themeDom = new DOMDocument();
                if (FB_PROFILE === "on") { file_put_contents($_SERVER["DOCUMENT_ROOT"] . FB_BASE . '/php-profiling.txt', microtime()." - read - theme.xml- ".basename(__FILE__)." (68)\n", FILE_APPEND); }
                $themeDom->load($theme."/theme.xml");
                $node = $themeDom->documentElement;
                $themeNode = $this->xml->importNode($node, true);
                $themesNode->appendChild($themeNode);
            }
        }
        $settings = $this->xml->getElementsByTagName("settings")->item(0);
        $settings->appendChild($themesNode);

        // Translations
        switch($prts[0]) {
            case "setup":
            case "login":
            case "admin":
                // backend
                $language = $this->getLanguage($this->preferred_language, $this->default_language, 'admin');
                //var_dump($language);
                $transDom = new DOMDocument();
                $transDom->load($_SERVER["DOCUMENT_ROOT"]. FB_BASE . "/lib/languages/" . $language . ".xml");
                $translations = $this->xml->importNode($transDom->documentElement, true);
                $root = $this->xml->documentElement;
                $root->appendChild($translations);
                break;
            default:
                // frontend
                $set = new Settings();
                $theme = $set->getSetting("theme");
                $language = $this->getLanguage($this->preferred_language, $this->default_language, $theme);
                $transDom = new DOMDocument();
                $transDom->load($_SERVER["DOCUMENT_ROOT"]. FB_BASE . "/addons/themes/" . $theme . "/languages/" . $language . ".xml");
                $translations = $this->xml->importNode($transDom->documentElement, true);
                $root = $this->xml->documentElement;
                $root->appendChild($translations);
                break;
        }
    }

    /**
     *
     * @desc determines which XSL to load (backend or a specific theme)
     * @param array $prts
     * @return DOMDocument
     */
    private function loadXSL($prts) {
        $dom = new DOMDocument();
        switch ($prts[0]) {
            case "setup":
            case "admin":
            case "login":
            	if (FB_PROFILE === "on") { file_put_contents($_SERVER["DOCUMENT_ROOT"] . FB_BASE . '/php-profiling.txt', microtime()." - read - folderblog.xsl - ".basename(__FILE__)." (91)\n", FILE_APPEND); }
                $dom->load($_SERVER["DOCUMENT_ROOT"].FB_BASE.'/lib/xsl/folderblog.xsl', LIBXML_NOCDATA);
                break;
            default:
                if (FB_PROFILE === "on") { file_put_contents($_SERVER["DOCUMENT_ROOT"] . FB_BASE . '/php-profiling.txt', microtime()." - read - theme.xsl - ".basename(__FILE__)." (91)\n", FILE_APPEND); }
                $dom->load($_SERVER["DOCUMENT_ROOT"].FB_BASE."/addons/themes/default/theme.xsl", LIBXML_NOCDATA);
                break;
        }
        return $dom;
    }

    /**
     * @desc get array of xslt params
     * @return array
     */
    private function getParams($prts) {
            $folder = $this->getFolder($prts);
            $element = $this->getElement($prts);
            $page = $this->getPage($prts);

            if (in_array($prts[0],array('setup','login','admin'))) {
                $context = 'admin';
            } else {
                $settings = new Settings();
                $context = $settings->getSetting('theme');
            }
            $language = $this->getLanguage($this->preferred_language, $this->default_language, $context);
            return array(
                'view'              => (in_array($prts[0],array("login","archive","404","admin","setup","page"))) ? $prts[0] : "index",
                'loggedinUser'      => (isset($_SESSION["Folderblog".FB_UID]["loggedinUser"])) ? $_SESSION["Folderblog".FB_UID]["loggedinUser"] : "",
                'base'              => (FB_BASE === "") ? '/' : FB_BASE,
                'folder'            => $folder,
                'element'           => $element,
                'page'              => $page,
                'language'	        => $language,
        );
    }

    /**
     *
     * Determine the language to load
     * @param string $preferred
     * @param string $default
     * @param string $context either 'admin' or name of a theme
     * @return string
     */
    private function getLanguage($preferred, $default, $context) {
        // Backend/Login/etc.
        if ($context === 'admin') {
            $langs = $this->getBackendLanguages();
            if ($preferred === 'browser') {
                if (function_exists('http_negotiate_language')) {
                    return str_replace('-','_',http_negotiate_language($langs, $result));
                } else {
                    $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
                    if (in_array($lang, $langs)) {
                        return $lang;
                    } else {
                        return $default;
                    }
                }
            } else {
                if (in_array($preferred,$langs)) {
                    return $preferred;
                } else {
                    return $default;
                }
            }
        }
        // Frontend
        else {
            $langs = $this->getThemeLanguages($context);
            if (in_array($preferred,$langs)) {
                return $preferred;
            } else {
                return $default;
            }
        }
    }

    private function getThemeLanguages($theme) {
        $array = glob($_SERVER["DOCUMENT_ROOT"].FB_BASE."/addons/themes/$theme/languages/*.xml");
        $out = array();
        foreach($array as $file) {
            $out[] = str_replace('_','-',pathinfo($file, PATHINFO_FILENAME));
        }
        return $out;
    }

    /**
     *
     * what backend languages are available?
     * @return array
     */
    private function getBackendLanguages() {
        $array = glob($_SERVER["DOCUMENT_ROOT"].FB_BASE."/lib/languages/*.xml");
        $out = array();
        foreach($array as $file) {
            $out[] = str_replace('_','-',pathinfo($file, PATHINFO_FILENAME));
        }
        return $out;
    }

    /**
     *
     * what page was requested?
     * @param array $parts
     * @return int
     */
    private function getPage($parts) {
        if (!in_array("page", $parts)) {
            return 1;
        } else {
            $i = 0;
            foreach($parts as $part) {
                if ($part === "page") {
                    $start = $i + 1;
                }
                if (isset($start) && $i >= $start) {
                    return intval(urldecode($part));
                }
                $i++;
            }
        }
    }

    /**
     *
     * what folder was requested?
     * @param array $parts
     * @return string
     */
    private function getFolder($parts) {
        if (!in_array("folders", $parts)) {
            return "/folders";
        } else {
            $r = '/';
            foreach($parts as $part) {
                if ($part !== "e" && $part !== "page") $r .= $part . '/';
                else return urldecode(rtrim($r,'/'));
            }
            return urldecode(rtrim($r, '/'));
        }
    }

    /**
     *
     * what element is requested?
     * @param array $parts
     * @return string
     */
    private function getElement($parts) {
        if (!in_array("e", $parts)) {
            return "";
        } else {
            $i = 0;
            foreach($parts as $part) {
                if ($part === "e") {
                    $start = $i + 1;
                }
                if (isset($start) && $i >= $start) {
                    return urldecode($part);
                }
                $i++;
            }
        }
    }

    /**
     *
     * @desc Instantiates the XSLTProcessor, performs the transformation, sets HTTP headers and outputs the transformation result.
     * @return string
     * @todo support for different content types (html, json, xml)
     */
    public function show() {
        $proc = new XSLTProcessor();
        $settings = new Settings();
        if ($settings->getSetting("profile") == '1') {
            $proc->setProfiling($_SERVER["DOCUMENT_ROOT"].FB_BASE.'/xslt-profiling.txt');
        }
        $proc->importStylesheet($this->xsl);
        if ($this->params) {
            foreach ($this->params as $param => $value) {
                $proc->setParameter('', $param, $value);
            }
        }
        header("Content-Type: text/html; charset=UTF-8");
        echo '<!DOCTYPE HTML>'."\n";
        echo $proc->transformToXML( $this->xml );
    }

    /**
     *
     * prepends the folderblog base to a given URL and then redirects there.
     * @param string $url internal folderblog URL to redirect to.
     * @return void
     * @todo parametrize this method to support status headers
     */
    public function redirect($url) {
        header("Location: ".FB_BASE.$url);
        exit;
    }

    /**
     *
     * Sanitizes the XML, tries to tidy it up and saves it.
     * @param string $str XML to save
     * @return void
     */
    public static function saveData($str) {
        $str = self::removeDynamicData($str);
        if (FB_PROFILE === "on") { file_put_contents($_SERVER["DOCUMENT_ROOT"] . FB_BASE . '/php-profiling.txt', microtime()." - checking permissions - /lib/data/folderblog.xml\n", FILE_APPEND); }
        if (!is_writeable($_SERVER["DOCUMENT_ROOT"].FB_BASE."/lib/data/folderblog.xml")) {
            throw new FolderblogException("Check permissions for the file <samp>lib/data/foldrblog.xml</samp>");
        }
        try {
            if (trim($str) === "") {
                throw new FolderblogException("Do not empty the data file");
            }
            $sanitized = StringFunctions::sanitizeXML($str);
            $tidy = StringFunctions::maketidy($sanitized);
            if (!is_null($tidy)) $out = $tidy;
            else $out = $sanitized;
            if (FB_PROFILE === "on") { file_put_contents($_SERVER["DOCUMENT_ROOT"] . FB_BASE . '/php-profiling.txt', microtime()." - write - /lib/data/folderblog.xml", FILE_APPEND); }
            file_put_contents($_SERVER["DOCUMENT_ROOT"].FB_BASE."/lib/data/folderblog.xml", $out);
            if (class_exists("Memcache")) {
                $memcache = new Memcache();
                $isMemcacheAvailable = @$memcache->connect('127.0.0.1', 11211);
                if ($isMemcacheAvailable) {
                    $memcache->set('Folderblog'.FB_UID, new Folderblog());
                }
            }
        } catch (Exception $e) {
            throw new FodlerblogException($e->getMessage());
        }
    }

    /**
     * method that ensures, that data that is dynamically added to the DOM is not stored (theme details, tranlsations, etc)
     * @param string $str
     * @return string
     */
    public static function removeDynamicData($str) {
        $dom = new DOMDocument();
        $dom->loadXML($str);
        $nodes = $dom->getElementsByTagName("themes");
        if ($nodes->length > 0) {
            $dom->getElementsByTagName("settings")->item(0)->removeChild($nodes->item(0));
        }

        $translations = $dom->getElementsByTagName("language");
        if ($translations->length > 0) {
            foreach($translations as $language) {
                $dom->documentElement->removeChild($language);
            }
        }
        return $dom->saveXML();
    }

    /**
     *
     * Saves a backup copy of the Folderblog data file
     * @throws FolderblogException
     * @return void
     * @todo write a restore function for the admin cp
     */
    public function backupData() {
    	if (FB_PROFILE === "on") { file_put_contents($_SERVER["DOCUMENT_ROOT"] . FB_BASE . '/php-profiling.txt', microtime()." - copy - /lib/data/folderblog.xml", FILE_APPEND); }
        if(!copy($_SERVER["DOCUMENT_ROOT"].FB_BASE."/lib/data/folderblog.xml", $_SERVER["DOCUMENT_ROOT"].FB_BASE."/cache/data/".date("Y-m-d-His")."-folderblog.xml")) {
            throw new FolderblogException("Could not create backup of data file. Make sure permissions for /cache/data are set correctly.");
        }
    }
}