<?php
/**
 *
 * @package Folderbog 4.0
 * @author Erik Pöhler
 * @version 4.0
 * @see http://erikpoehler.com/folderblog/
 *
 */
class Route extends Frontend {
    private $_request_uri;
    private $_commands = array('index', 'admin', 'folders', 'e', 'archive', 'login', 'logout', '404', 'setup', 'page');

    public function __construct() {
        $this->_request_uri = substr(str_replace(FB_BASE,"",$_SERVER["REQUEST_URI"]),1);
        $this->validate();
    }

    public function validate() {
        $prts = explode("/", $this->_request_uri);
        // in case someone tried to access /index
        if ($prts[0] === 'index') $this->redirect("/");
        // in case there is no command called we want to see the index
        if ($prts[0] === "") $prts[0] = 'index';

        // make sure trailing slash is present
        if (strlen($this->_request_uri) > 0 && substr($this->_request_uri,-1) !== '/') {
            $this->redirect('/'.$this->_request_uri."/");
        }

        switch ($prts[0]) {
            case "index.php":
                // avoid direct calls to index.php
                $this->redirect("/");
                break;
            case "login":
                if (isset($_SESSION["Folderblog".FB_UID])) {
                	if (isset($_SESSION["Folderblog".FB_UID]["loggedinUser"]) && strlen($_SESSION["Folderblog".FB_UID]["loggedinUser"]) > 0) {
                        $this->redirect("/admin/");
                    }
                }
                break;
            case "admin":
                session_start();
                if (isset($_SESSION["Folderblog".FB_UID])) {
                    if (!isset($_SESSION["Folderblog".FB_UID]["loggedinUser"]) || strlen($_SESSION["Folderblog".FB_UID]["loggedinUser"])==0) {
                        // user missing? go away
                        $this->redirect("/login/");
                    }
                } else {
                    // session missing? go away!
                	$this->redirect("/login/");
                }
                session_write_close();
                break;
            default:
                // validate command
                if (!in_array($prts[0], $this->_commands)) {
                    $this->redirect("/404/");
                }
                break;
        }
    }
}