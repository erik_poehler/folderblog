<?php
/**
 *
 * @package Folderbog 4.0
 * @author Erik Pöhler
 * @version 4.0
 * @see http://erikpoehler.com/folderblog/
 *
 */

error_reporting(E_ALL);
class Settings {

    /**
     * Saves the submitted settings to data XML
     * @return void
     */
    public function saveSettings() {
        if (!isset($_SESSION["Folderblog".FB_UID]) || $_SESSION["Folderblog".FB_UID] === null) $this->redirect("/login/");
        $frontend = new Frontend();
        $frontend->backupData();

        // set checkboxes to defaults in case they are unchecked
        if (!array_key_exists("require_captcha", $_POST)) $_POST["require_captcha"] = 0;
        if (!array_key_exists("square_thumbs", $_POST)) $_POST["square_thumbs"] = 0;
        if (!array_key_exists("crop_images", $_POST)) $_POST["crop_images"] = 0;
        if (!array_key_exists("moderate_comments", $_POST)) $_POST["moderate_comments"] = 0;
        if (!array_key_exists("enable_comments", $_POST)) $_POST["enable_comments"] = 0;
        if (!array_key_exists("minify_css", $_POST)) $_POST["minify_css"] = 0;
        if (!array_key_exists("minify_js", $_POST)) $_POST["minify_js"] = 0;
        if (!array_key_exists("profile", $_POST)) $_POST["profile"] = 0;
        if (!array_key_exists("theme", $_POST)) $_POST["theme"] = "default";
        if (!array_key_exists("preferred_language", $_POST)) $_POST["preferred_language"] = "browser";
        if (!array_key_exists("default_language", $_POST)) $_POST["default_language"] = "en_US";
        $dom = new DOMDocument();
        if (FB_PROFILE === "on") { file_put_contents($_SERVER["DOCUMENT_ROOT"] . FB_BASE . '/php-profiling.txt', microtime()." - read - folderblog.xml - ".basename(__FILE__)."\n", FILE_APPEND); }
        $dom->load($_SERVER["DOCUMENT_ROOT"].FB_BASE."/lib/data/folderblog.xml"); // read from file, no need to store dynamic data her
        if (gettype($dom) === "object") {
            if ($dom instanceof DOMDocument) {
                if (isset($_POST)) {
                    foreach($_POST as $key => $value) {
                        //var_dump($dom->saveXML());exit;
                        $dom->getElementsByTagName($key)->item(0)->nodeValue = (string) $value;
                    }
                }
            }
        }
        $frontend->backupData();
        $frontend->saveData($dom->saveXML());
        $frontend->redirect("/admin/#settings");
        exit;
    }

    public function getSetting($name) {
    	$dom = new DOMDocument();
    	$dom->load($_SERVER["DOCUMENT_ROOT"].FB_BASE."/lib/data/folderblog.xml");
        $nodes = $dom->getElementsByTagName("settings")->item(0)->getElementsByTagName($name);
        if ($nodes->length > 0) {
        	return $nodes->item(0)->nodeValue;
        } else {
        	throw new Exception("Could not find a seting by the name <samp>$name</samp>.");
        }
    }
}