/**
 * @package Folderblog 4.0
 * @author Erik Pöhler <iroybot@gmail.com>
 * @desc Default Theme Javascript
 * @version 1.0
 * @see http://erikpoehler.com/folderblog/
 */

function reportEvent(event) {
    jQuery("#lastevent").text(event.type);
}

function reportData(data) {
    jQuery("#output").text(data);
}


jQuery(window).bind('popstate', function(event) {
    event = event.originalEvent || window.event;
    var currentLocation = event.location || document.location;
    var data = event.state;
    var url = parse_url(currentLocation.pathname);
    var path = url.path;
    var parts = explode("/",trim(path,'/'));
    var elementId = null;
    if (path !== "/") {
        for (var i = 0; i<parts.length; i++) {
            if (parts[i] = "e") { 
                element = parts[i+1];
                break;
              }
        }
    } else {
        elementId = 1;
    }
    if (isNaN(elementId) || elementId === null) {
        if ($("#all > div > span[data-slug='" + element + "']").parent().length > 0) {
            elementId = $("#all > div > span[data-slug='" + element + "']").parent().attr("id").replace("element","");
        } else {
            elementId = $("#image-container span[data-slug='" + element + "']").parent().parent().attr("id").replace("element","");
        }
    }
    var id = $("#image-container").parent().attr('id');
    theme.gotoElement(elementId, id, false); // load the content without calling pushState over again.
});

jQuery(window).bind('hashchange', function(event) {
    // we won't do this for now - let's stay focused on states
    /*
     * if (event.newURL) { urlhistory.innerHTML = event.oldURL; } else {
     * urlhistory.innerHTML = "no support for <code>event.newURL/oldURL</code>"; }
     */
});

jQuery(window).bind('pageshow', function(event) {
    // reportEvent(event);
});

jQuery(window).bind('pagehide', function(event) {
    // reportEvent(event);
});

var theme = {
    init: function() {
        $("body").addClass("js");

        // loading animation
        spinner(50, 11, document.getElementById('spinner'), 100)
        
        // hover over prev/next
        var prevnext_config = {
            over: function() { $(this).find("em").animate({opacity:1},200,'easeInQuad'); },
            timeout: 0,
            out: function() { $(this).find("em").animate({opacity:0},200,'easeInQuad'); },
        };
        $("#image-container a.previous, #image-container a.next").hoverIntent(prevnext_config);
        
        // use cursor to navigate through images
        $(document).keydown(function(e){
            if (!e.altKey && !e.ctrlKey && !e.metaKey) {
                // next image
                if (e.keyCode == 39) { 
                    $("#image-container a.next").click();
                    return false;
                }
                
                // previous
                if (e.keyCode == 37) { 
                    $("#image-container a.previous").click();
                    return false;
                }
            }
        });
        
        // next image
        $("#image-container a.next").click(function(){
            var id = $(this).parent().parent().attr("id");
            var curpos = id.replace("element","");
            if ($(this).parent().parent().attr("class") != '') {
                curpos = $(this).parent().parent().attr("class").replace("element","");
            }
            theme.nextElement(curpos, id);
            return false;
        });
        
        // previous image
        $("#image-container a.previous").click(function(){
            var id = $(this).parent().parent().attr("id");
            var curpos = id.replace("element","");
            if ($(this).parent().parent().attr("class") != '') {
                curpos = $(this).parent().parent().attr("class").replace("element","");
            }
            theme.previousElement(curpos, id);
            return false;
        });
        
        // sidebar click on image
        $("#latest li").click(function(){
            var id = $("#image-container").parent().attr('id');
            var pos = $(this).data("position");
            theme.gotoElement(pos, id, true);
            return false;
        });
        
        // pagination
        $("#pagination a").click(function(){
            var page = $(this).data("page");
            $("#latest li").removeClass("visible").addClass("hidden");
            $("#latest li[data-page='"+page+"']").addClass("visible").removeClass("hidden");
            $("#pagination a").removeClass("active");
            $(this).addClass("active");
            return false;
        });
    },
    
    nextElement: function(curpos, id) {
        var nextpos = Number(curpos) + 1;
        theme.gotoElement(nextpos, id, true);
    },
    
    previousElement: function(curpos, id) {
        var prevpos = Number(curpos) - 1;
        theme.gotoElement(prevpos, id, true);
    },
    
    // the actual loading of content
    gotoElement: function(pos, id, doPush) {
        if ($("#element" + pos + " span:eq(0)").length > 0) { // only between first/last
            var $element = $("#element" + pos + " span:eq(0)");
            var src = $element.data("src");
            var title = jQuery.trim($element.data("title"));
            if (title === '') title = "Untitled";
            var description = jQuery.trim($element.data("description"));
            var height = $element.data("height");
            // @todo handle different types (image/vide/audio/etc.)
            $("#spinner").fadeIn();
            $("#image").animate({opacity:1},300,'easeOutExpo',function(){
                // div.main-container h2, div.main-container p.description
                $("#image").one('load', function() {
                    $("#spinner").hide();
                    $("#latest li.active").removeClass("active");
                    $("#latest li[data-position='"+pos+"']").addClass("active");
                    if (!$("#latest li[data-position='"+pos+"']").hasClass("visible")) {
                        var curpage = $("#latest li.active:eq(0)").data("page");
                        $("#latest li").removeClass("visible").addClass("hidden");
                        $("#latest li[data-page='" + curpage + "']").removeClass("hidden").addClass("visible");
                        $("#pagination a").removeClass("active");
                        $("#pagination a[data-page='" + curpage + "']").addClass("active");
                    }
                    $(this).attr("height",height).animate({opacity:1}, 500, 'easeOutExpo')
                    .parent().animate({height: height}, 500, 'easeOutExpo')
                    .parent().find("h2").text(title)
                    .parent().find("p.description").text(description);
                    $("#"+id).attr("class","element" + pos);
                    if (pos == 1) {
                        $("#image-container a.previous").hide();
                    }
                    if (pos > 1) {
                        $("#image-container a.previous").show();
                    }
                    if (pos == $("#all > div").length) {
                        $("#image-container a.next").hide();
                    }
                    if (pos < $("#all > div").length) {
                        $("#image-container a.next").show();
                    }
                    $("#image").attr("data-slug",$element.data("slug"));
                    var stateObj = { position: pos };
                    if (doPush) {
                        document.title = title;
                        history.pushState(title, title, "/e/"+$element.data("slug")+"/");
                    }
                }).attr("src",src).each(function() {
                    if(this.complete) $(this).load();
                });
            });
        }
    },
};

var spinner = (function () {
    var timerId;

    return function (width, times, element, speed) {
        var i = 0;
        timerId = setInterval(function () {
            if (i > times)
                i = 0;
                element.style.backgroundPosition = "-" + i * width + 'px 0px';
                i ++;
        }, speed);
    };
})();

(function ($) {
    $(document).ready(function(){
        theme.init();
    });
})(jQuery);