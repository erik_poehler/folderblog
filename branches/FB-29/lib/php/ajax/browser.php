<?php
/**
 *
 * @package Folderbog 4.0
 * @author Erik Pöhler
 * @version 4.0
 * @see http://erikpoehler.com/folderblog/
 * @desc AJAX Helper to generate clean folder names
 *
 */

error_reporting(E_ALL ^ E_NOTICE);

if (!defined('FB_BASE')) {
	define('FB_BASE', str_replace($_SERVER["DOCUMENT_ROOT"],"",realpath(dirname(__FILE__)."/../../../"))); // going up to folderblog base
}

function __autoload($cn) {
	if (is_file($_SERVER["DOCUMENT_ROOT"] . FB_BASE . "/lib/php/$cn.php")) require_once $_SERVER["DOCUMENT_ROOT"].FB_BASE."/lib/php/$cn.php";
}

define('FB_PROFILE','off');
if (FB_PROFILE === "on") { file_put_contents($_SERVER["DOCUMENT_ROOT"] . FB_BASE . '/php-profiling.txt', "--- ".$_SERVER["REQUEST_URI"]. "---\n", FILE_APPEND); }

// making sure you have permissions to view this
session_start();
$response = new AjaxResponse();

// setting defaults
$html = null;
$folder = "/folders";
$results_per_page = 10;
$page = 1;

// looking up session stored selection
if (isset($_SESSION["Folderblog"]["browser"])) {
	$def = json_decode($_SESSION["Folderblog"]["browser"], true);
	$folder = $def["folder"];
	$results_per_page = $def["results_per_page"];
	$page = $def["page"];
}

// overwriting values if any are sent
$folder = (isset($_GET['folder'])) ? $_GET['folder'] : $folder;
$page = (isset($_GET['page'])) ? $_GET['page'] : $page;
$results_per_page = (isset($_GET["results_per_page"])) ? $_GET["results_per_page"] : $results_per_page;

// storing selection to session
if (isset($_GET['folder']) || isset($_GET['page']) || isset($_GET["results_per_page"])) {
    $_SESSION["Folderblog"]["browser"] = json_encode(array(
        "page"=>$page,
        "folder" => $folder,
        "results_per_page" => $results_per_page));
    session_write_close();
}
// not really expecting this to fail but anywho...
try {
	$dom = new DOMDocument();
	$dom->loadXML(Folderblog::getInstance()->xml);
	$xsl = new DOMDocument();
	if (FB_PROFILE === "on") { file_put_contents($_SERVER["DOCUMENT_ROOT"] . FB_BASE . '/php-profiling.txt', microtime()." - read - browser.xsl - ".basename(__FILE__)." (65)\n", FILE_APPEND); }
	$xsl->load($_SERVER["DOCUMENT_ROOT"] . FB_BASE . "/lib/xsl/browser.xsl");
	$proc = new XSLTProcessor();
	$proc->importStylesheet($xsl);
	$params = array();
	$params["page"] = (string) $page;
	$params["folder"] = (string) $folder;
	$params["results_per_page"] = (string) $results_per_page;
	$params["base"] = (string) FB_BASE;
	$params["command"] = "admin";
	foreach ($params as $k => $v) {
		$proc->setParameter('', $k, $v);
	}
	$html = $proc->transformToXml($dom);
} catch (Exception $e) {
	$response->setMessage($e->getMessage());
}
if (!is_null($html)) {
	header("Content-Type: text/html; charset=UTF-8");
	echo $html;
} else {
	header("Content-Type: text/xml; charset=UTF-8");
    echo $response->createResponse();
}
exit;