<?php
/**
 *
 * @package Folderbog 4.0
 * @author Erik Pöhler
 * @version 4.0
 * @see http://erikpoehler.com/folderblog/
 * @desc AJAX Helper to upload media
 *
 */

error_reporting(E_ALL ^ E_NOTICE);
session_start();

if (!defined('FB_BASE')) {
    define('FB_BASE', str_replace($_SERVER["DOCUMENT_ROOT"],"",realpath(dirname(__FILE__)."/../../../"))); // going up to folderblog base
}

function __autoload($cn) {
    if (is_file($_SERVER["DOCUMENT_ROOT"] . FB_BASE . "/lib/php/$cn.php")) require_once $_SERVER["DOCUMENT_ROOT"].FB_BASE."/lib/php/$cn.php";
}

define('FB_PROFILE', "off");

if (isset($_GET["qqfile"]) && strlen($_GET["qqfile"])>0) {
    if (isset($_FILES)) {
        $uploaddir = str_replace('%2C','',trim($_GET["folder"],','));
        $uploaddir = rtrim($uploaddir,'/').'/';
        $allowedExtensions = array(
            'mpg','mpeg','qt','mp4','mov','avi',
            'mp3','ogg','wav',
            'jpg','jpeg','png',
            'tar','tar.gz','gz','zip','tgz'
        );
        // max file size in bytes 1GB
        $sizeLimit = 1000 * 1024 * 1024;
        $filename = StringFunctions::getFilename(pathinfo($_GET["qqfile"], PATHINFO_FILENAME)) . "." . strtolower(pathinfo($_GET["qqfile"], PATHINFO_EXTENSION));

        // don't overwrite existing files
        $uploader = new AjaxUploader($allowedExtensions, $sizeLimit);
        if (is_file($_SERVER["DOCUMENT_ROOT"].FB_BASE.$uploaddir.$filename)) {
            $result = $uploader->handleUpload($_SERVER["DOCUMENT_ROOT"].FB_BASE.$uploaddir, false);
            if ($result["success"]===true){
                new Element($uploaddir, $result["filename"]);
            }
        } else {
            $result = $uploader->handleUpload($_SERVER["DOCUMENT_ROOT"].FB_BASE.$uploaddir, true);
            if ($result["success"]===true) {
                new Element($uploaddir, $result["filename"]);
            }
            echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        }
    }
}