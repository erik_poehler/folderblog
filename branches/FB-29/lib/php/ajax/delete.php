<?php
/**
 *
 * @package Folderbog 4.0
 * @author Erik Pöhler
 * @version 4.0
 * @see http://erikpoehler.com/folderblog/
 * @desc AJAX Helper to physically delete images
 *
 */

error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);

if (!defined('FB_BASE')) {
	define('FB_BASE', str_replace($_SERVER["DOCUMENT_ROOT"],"",realpath(dirname(__FILE__)."/../../../"))); // going up to folderblog base
}
function __autoload($cn) {
	if (is_file($_SERVER["DOCUMENT_ROOT"] . FB_BASE . "/lib/php/$cn.php")) require_once $_SERVER["DOCUMENT_ROOT"].FB_BASE."/lib/php/$cn.php";
}

define('FB_PROFILE','off');

session_start();
$response = new AjaxResponse();

if (isset($_GET['file'])) {
    $file = trim(strip_tags($_GET['file']));
    if (is_file($_SERVER["DOCUMENT_ROOT"].FB_BASE.$file)) {
    	$response->setSuccess(true);
        // backup data
        if(!copy($_SERVER["DOCUMENT_ROOT"].FB_BASE."/lib/data/folderblog.xml", $_SERVER["DOCUMENT_ROOT"].FB_BASE."/cache/data/".date("Y-m-d-His")."-folderblog.xml")) {
            $response->setSuccess(false)->setMessage("Could not create backup of data file. Make sure permissions for /cache/data are set correctly.");
        }

        // backup image
        if(!copy($_SERVER["DOCUMENT_ROOT"].FB_BASE.$file, $_SERVER["DOCUMENT_ROOT"].FB_BASE."/cache/backups/".date("Y-m-d-His")."-".basename($file))) {
            $response->setSuccess(false)->setMessage("Could not create backup of data file. Make sure permissions for /cache/backups are set correctly.");
        }

        // remove image from DOM
        $dom = new DOMDocument();
        if (FB_PROFILE === "on") { file_put_contents($_SERVER["DOCUMENT_ROOT"] . FB_BASE . '/php-profiling.txt', microtime()." - read - folderblog.xml - ".basename(__FILE__)."\n", FILE_APPEND); }
        $dom->load($_SERVER["DOCUMENT_ROOT"].FB_BASE."/lib/data/folderblog.xml");
        $xpath = new DOMXPath($dom);
        $results = $xpath->query("//element[path='$file']");
        if ($results->length > 0) {
            $results->item(0)->parentNode->removeChild($results->item(0));
            Frontend::saveData($dom->saveXML());
        } else {
        	$response->setSuccess(false)->setMessage("The file was not found in the XML");
        }

        // remove image
        if (!unlink($_SERVER["DOCUMENT_ROOT"].FB_BASE.$file)) {
            $response->setSuccess(false)->setMessage("Could not delete the file ".basename($file));
        } else {
        	if (FB_PROFILE === "on") { file_put_contents($_SERVER["DOCUMENT_ROOT"] . FB_BASE . '/php-profiling.txt', microtime()." - deleting file - ".basename($file)."\n", FILE_APPEND); }
        }
    } else {
    	$response->setSuccess(false)->setMessage("Image not found in file system.");
    }
}

header("Content-Type: text/xml; charset=UTF-8");
echo $response->createResponse();