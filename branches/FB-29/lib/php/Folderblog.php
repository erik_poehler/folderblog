<?php
/**
 *
 * @package Folderbog 4.0
 * @author Erik Pöhler
 * @version 4.0
 * @see http://erikpoehler.com/folderblog/
 *
 */

if(!defined('FB_PROFILE')) define('FB_PROFILE','off');

final class Folderblog {
    public $xml;

    public function __construct() {
        if (FB_PROFILE === "on") { file_put_contents($_SERVER["DOCUMENT_ROOT"] . FB_BASE . '/php-profiling.txt', microtime()." - read - folderblog.xml - " .basename(__FILE__)." (16)\n", FILE_APPEND); }
        $dom = new DOMDocument();
        $dom->load($_SERVER["DOCUMENT_ROOT"] . FB_BASE . '/lib/data/folderblog.xml');
        $this->xml = $dom->saveXML();
    }

    public function run() {
        new ActionHandler();
        $frontend = new Frontend();
        $frontend->show();
    }

    public static function getInstance() {
        /* store the instance in the RAM basically to reduce file access to folderblog.xml */
        if (class_exists("Memcache")) {
            $memcache = new Memcache;
            //echo "<pre>";
            $memcache->connect('127.0.0.1', 11211);
            //var_dump($memcache->getstats());
            //var_dump(get_class_methods('Memcache'));exit;
            $folderblog = $memcache->get('Folderblog');
            if($folderblog instanceof Folderblog) {
                // if it's there, use it
                $memcache->close();
                return $folderblog;
            } else {
                // otherwise set it
                $folderblog = new Folderblog();
                $memcache->set('Folderblog', $folderblog);
                $memcache->close();
                return $folderblog;
            }
        } else {
            return new Folderblog();
        }
    }
}