<?php
/**
 *
 * @package Folderbog 4.0
 * @author Erik Pöhler
 * @version 4.0
 * @see http://erikpoehler.com/folderblog/
 *
 */
class ActionHandler {
    public $data = '/lib/data/folderblog.xml';

    /**
     * ActionHandler Constructor
     * creates an instance of the router to validate the request, then checks for existance of any POST actions
     */
    public function __construct() {
        $dom = new DOMDocument();
        $dom->loadXML(Folderblog::getInstance()->xml);

        // setup logic
        if (isset($_POST["superuser"]) && isset($_POST["repeat"]) && $_POST["action"] == "setup") {
            $users = $dom->getElementsByTagName("user");
            $usercount = $users->length;
            $pass = $users->item(0)->getAttribute("password");
            if ($usercount == 1 && $pass == "") {
                $users = new Users();
                $users->setSuperuserPassword();
            } else {
                header("Location: ".FB_BASE."/");
                exit;
            }
        }
        new Route();
        if (strpos("_".$_SERVER["REQUEST_URI"], "/setup/") === false) {
            // setup has not been requested
            $this->checkInstallation($dom); // figures out if we have to redirect there
        }
        if (strpos("_".$_SERVER["REQUEST_URI"], "/setup/") > 0) {
            // setup was reuested
            $users = $dom->getElementsByTagName("user");
            $usercount = $users->length;
            $pass = $users->item(0)->getAttribute("password");
            if ($usercount !== 1 || $pass !== "") {
                header("Location: ".FB_BASE."/");
                exit;
        	}
        }
        $this->login();
        $this->logout();
        // only process forms if a user is logged in
        if (isset($_SESSION["Folderblog"]["loggedinUser"])) {
            if (strlen($_SESSION["Folderblog"]["loggedinUser"])>0) {
                $this->handlePost();
            }
        }
    }

    public function checkInstallation() {
        $dom = new DOMDocument();
        $dom->loadXML(Folderblog::getInstance()->xml);
        $user = $dom->getElementsByTagName("user")->item(0);
        $name = $user->getAttribute("name");
        $password = $user->getAttribute("password");
        if ($name == "superuser" && $password == "") {
            header("Location: ".FB_BASE."/setup/");
            exit;
        }
    }

    /**
     *
     * Logs-in a user, creates session and redirects accordingly
     * @return void
     */
    public function login() {
        if (isset($_POST["username"]) && isset($_POST["password"])) {
            $dom = new DOMDocument();
            $dom->loadXML(Folderblog::getInstance()->xml);
            $xpath = new DOMXPath($dom);
            $users = $xpath->query("/folderblog/users/user[@name='".$_POST["username"]."']");
            if ($users->length > 0) {
                if (md5($_POST["password"]) === $users->item(0)->getAttribute("password")) {
                    session_start();
                    $_SESSION["Folderblog"]["loggedinUser"] = $_POST["username"];
                    session_write_close();
                    header("Location: ".FB_BASE."/admin/");
                }
            }
        }
    }

    /**
     *
     * Logs-out a user from the backend, and redirects him
     * @return void
     */
    public function logout() {
        $prts = explode('/',substr(str_replace(FB_BASE,"",$_SERVER["REQUEST_URI"]),1));
        if ($prts[0] === "logout" || (isset($_POST["action"]) && $_POST["action"] === "logout")) {
        	session_start();
        	$_SESSION["Folderblog"]["loggedinUser"] = null;
        	session_write_close();
            header("Location: ".FB_BASE."/login/");
        }
    }

    /**
     *
     * handles POST actions for admin forms.
     */
    public function handlePost() {
        if (isset($_POST["action"]) && strlen($_POST["action"])>0) {
            //var_dump($_POST);exit;
        	switch($_POST["action"]) {
                case "settings":
                    // save general settings
                    $settings = new Settings();
                    $settings->saveSettings();
                    break;
                case "users":
                    // update users
                    $users = new Users();
                    $users->saveUsers();
                    break;
                case "adduser":
                    // add new user
                    $users = new Users();
                    $users->addUser();
                    break;
                case "deleteuser":
                    $users = new Users();
                    $users->deleteUser();
                    break;
                case "addfolder":
                    $f = new Folder();
                    $f->addFolder();
                    break;
                case "deletefolder":
                    $f = new Folder();
                    $f->deleteFolder();
                    break;
                case "editfolder":
                    $f = new Folder();
                    $f->editFolder();
                    break;
                case "elementdetails":
                    $browser = new Browser();
                    $browser->updateElements();
                    break;
                case "delete":
                    break;
            }
        }
    }
}