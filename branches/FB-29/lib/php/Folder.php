<?php
/**
 *
 * @package Folderbog 4.0
 * @author Erik Pöhler
 * @version 4.0
 * @see http://erikpoehler.com/folderblog/
 *
 */
class Folder extends Frontend {
    /**
     *
     * Physically creates a new folder and adds the corresponding structure to the data file
     * @return void
     */
    public function addFolder() {
        $title = trim(strip_tags($_POST["title"]));
        $parent = trim(strip_tags($_POST["parent"]));
        $path = $parent.'/'.StringFunctions::stringToFilename(trim(strip_tags($_POST["title"])));
        $description = trim(strip_tags($_POST["description"])); // don't allow any script or html in here for now.

        if (!is_dir($_SERVER["DOCUMENT_ROOT"].FB_BASE.$path)) {
            // folder doesn't exist, so create it
            if (is_writeable(dirname($_SERVER["DOCUMENT_ROOT"].FB_BASE.$path))) {
                // folder is writeable as well.
                if (mkdir($_SERVER["DOCUMENT_ROOT"].FB_BASE.$path, 0755)) {
                    // folder successfully created, now append folder element to dom
                    if ($this->xml instanceof DOMDocument) {
                    	$dom = $this->xml;
                    } else {
                        $dom = new DOMDocument();
                        if (FB_PROFILE === "on") { file_put_contents($_SERVER["DOCUMENT_ROOT"] . FB_BASE . '/php-profiling.txt', microtime()." - read - folderblog.xml - ".basename(__FILE__)."\n", FILE_APPEND); }
                        $dom->load($_SERVER["DOCUMENT_ROOT"].FB_BASE."/lib/data/folderblog.xml");
                    }
                    $xpath = new DOMXPath($dom);
                    $results = $xpath->query("//folder[path='$parent']");
                    if (intval($results->length) > 0) {
                        // parent node was found!
                        // safety backup
                        $this->backupData();
                        $appendto = $results->item(0)->getElementsByTagName("folders")->item(0);

                        $newfolder = $dom->createElement("folder");
                        // title
                        $t = $dom->createCDATASection($title);
                        $tnode = $dom->createElement("title");
                        $tnode->appendChild($t);
                        $newfolder->appendChild($tnode);
                        // path
                        $p = $dom->createElement("path",$path);
                        $newfolder->appendChild($p);
                        // description
                        $desc = $dom->createCDATASection($description);
                        $descnode = $dom->createElement("description");
                        $descnode->appendChild($desc);
                        $newfolder->appendChild($descnode);
                        // empty childnodes
                        $newfolder->appendChild($dom->createElement("comments"));
                        $newfolder->appendChild($dom->createElement("elements"));
                        $newfolder->appendChild($dom->createElement("folders"));
                        $appendto->appendChild($newfolder);
                        // save updated DOM
                        $this->saveData($dom->saveXML());
                        $this->redirect("/admin/#folders");
                    } else {
                        // no folder with parent path found.
                        die("the parent folder <samp>$parent</samp> could not be found.");
                    }
                } else {
                    // error creating folder
                    die("error creating folder");
                }
            } else {
                // the parent folder isn't writeable.
                die("The folder <samp>".str_replace($_SERVER["DOCUMENT_ROOT"],'',dirname($_SERVER["DOCUMENT_ROOT"].FB_BASE.$path))."</samp> is not writeable. Check permissions.");
            }
        } else {
            // folder already exists
            die("Folder <samp></samp> already exists.");
        }
    }

    public function editFolder() {
        // post
        $path = trim(strip_tags($_POST["path"]));
        $title = trim(strip_tags($_POST["title"]));
        $description = trim(strip_tags($_POST["description"]));
        // DOM node
        if ($this->xml instanceof DOMDocument) {
        	$dom = $this->xml;
        } else {
        	$dom = new DOMDocument();
        	if (FB_PROFILE === "on") { file_put_contents($_SERVER["DOCUMENT_ROOT"] . FB_BASE . '/php-profiling.txt', microtime()." - read - /lib/data/folderblog.xml\n", FILE_APPEND); }
        	$dom->load($_SERVER["DOCUMENT_ROOT"].FB_BASE."/lib/data/folderblog.xml");
        }
        $xpath = new DOMXPath($dom);
        $results = $xpath->query("//folder[path='$path']");
        if (intval($results->length)>0) {
            $node = $results->item(0);
            $node->getElementsByTagName("title")->item(0)->nodeValue = $title;
            $node->getElementsByTagName("description")->item(0)->nodeValue = $description;
            $this->backupData();
            $this->saveData($dom->saveXML());
            header("Location: ".FB_BASE."/admin/#folders");
            exit;
        }
    }

    public function deleteFolder() {
        // post
        $path = trim(strip_tags($_POST["path"]));
        if (is_dir($_SERVER["DOCUMENT_ROOT"].FB_BASE.$path)) {
            $this->backupData();

            // safety backup folder
            $foldername = basename($path);
            if (is_dir($_SERVER["DOCUMENT_ROOT"].FB_BASE.'/cache/backups')) {
                if(function_exists('exec')) {
                    $command = 'tar zcvf "'.$_SERVER["DOCUMENT_ROOT"].FB_BASE.'/cache/backups/'.$foldername.'-'.date("Y-m-d-His").'.tar.gz" "'.$_SERVER["DOCUMENT_ROOT"].FB_BASE.$path.'" 2>&1';
                    exec($command, $return, $code);
                    if ($code !== 0) {
                        throw new FolderblogException(printf("Fuck! Something went wrong: %s (%s)", join('<br />', $return), $code));
                    }
                } else {
                    throw new FolderblogException("native php zip creation not yet implemented.");
                }
            }
            // physical files
            $fs = new FilesystemFunctions();
            $fs->rrmdir($_SERVER["DOCUMENT_ROOT"].FB_BASE.$path);

            // DOM node
            if ($this->xml instanceof DOMDocument) {
            	$dom = $this->xml;
            } else {
            	$dom = new DOMDocument();
            	if (FB_PROFILE === "on") { file_put_contents($_SERVER["DOCUMENT_ROOT"] . FB_BASE . '/php-profiling.txt', microtime()." - read - folderblog.xml - ".basename(__FILE__)."\n", FILE_APPEND); }
            	$dom->load($_SERVER["DOCUMENT_ROOT"].FB_BASE."/lib/data/folderblog.xml");
            }
            $xpath = new DOMXPath($dom);
            $results = $xpath->query("//folder[path='$path']");
            if (intval($results->length)>0) {
                foreach($results as $node) {
                    $node->parentNode->removeChild($node);
                }
            }
            // sanitize, tidy and save updated DOM
            $this->saveData($dom->saveXML());
            header("Location: ".FB_BASE."/admin/#folders");
            exit;
        } else {
            throw new FolderblogException("Folder <samp>".FB_BASE.$path."</samp> not found.");
        }
    }
}