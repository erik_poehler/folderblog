/**
 * Folderblog 4.0
 * @author Erik Pöhler
 * @version 4.0
 * @see http://folderblog.org/
 */

/**
 * @todo use an equivalent, structured code for 
 *       the rest of the backend as seen below in var mediabrowser;
 */
var folderblog = {
    
};

var tooltip=function(){
    var id = 'tt';
    var top = 3;
    var left = 3;
    var maxw = 300;
    var speed = 10;
    var timer = 20;
    var endalpha = 95;
    var alpha = 0;
    var tt,t,c,b,h;
    var ie = document.all ? true : false;
    return{
        show:function(v,w){
            if(tt == null){
                tt = document.createElement('div');
                tt.setAttribute('id',id);
                t = document.createElement('div');
                t.setAttribute('id',id + 'top');
                c = document.createElement('div');
                c.setAttribute('id',id + 'cont');
                b = document.createElement('div');
                b.setAttribute('id',id + 'bot');
                tt.appendChild(t);
                tt.appendChild(c);
                tt.appendChild(b);
                document.body.appendChild(tt);
                tt.style.opacity = 0;
                tt.style.filter = 'alpha(opacity=0)';
                document.onmousemove = this.pos;
            }
            tt.style.display = 'block';
            c.innerHTML = v;
            tt.style.width = w ? w + 'px' : 'auto';
            if(!w && ie){
                t.style.display = 'none';
                b.style.display = 'none';
                tt.style.width = tt.offsetWidth;
                t.style.display = 'block';
                b.style.display = 'block';
            }
            if(tt.offsetWidth > maxw){tt.style.width = maxw + 'px'}
            h = parseInt(tt.offsetHeight) + top;
            clearInterval(tt.timer);
            tt.timer = setInterval(function(){tooltip.fade(1)},timer);
        },
        pos:function(e){
            var u = ie ? event.clientY + document.documentElement.scrollTop : e.pageY;
            var l = ie ? event.clientX + document.documentElement.scrollLeft : e.pageX;
            tt.style.top = (u - h) + 'px';
            tt.style.left = (l + left) + 'px';
        },
        fade:function(d){
            var a = alpha;
            if((a != endalpha && d == 1) || (a != 0 && d == -1)){
                var i = speed;
                if(endalpha - a < speed && d == 1){
                    i = endalpha - a;
                }else if(alpha < speed && d == -1){
                    i = a;
                }
                alpha = a + (i * d);
                tt.style.opacity = alpha * .01;
                tt.style.filter = 'alpha(opacity=' + alpha + ')';
            }else{
                clearInterval(tt.timer);
                if(d == -1){tt.style.display = 'none'}
            }
        },
        hide:function(){
            clearInterval(tt.timer);
            tt.timer = setInterval(function(){tooltip.fade(-1)},timer);
        }
    };
}();

var mediabrowser = {
    init: function () {
        $.ajax({
            type: "GET",
            url: base+"lib/php/ajax/browser.php",
            contentType: 'text/html; charset=UTF-8',
            dataType: 'html',
            cache: false,
            success: function(response){
                document.getElementById('mediabrowser').innerHTML = response;
                mediabrowser.onload();
            }
        });
    },
    
    onload: function() {
        $("select.filter").select_skin();
        this.imagediting();
        this.imagetooltip();
        this.filesize();
        this.imagerotate();
        this.imagedelete();
        this.pagination();
        this.filters();
    },
    
    imagetooltip: function() {
        $("#mediabrowser td.preview img").each(function(){
            $(this).hover(
                    function(){
                        // mouseover
                        console.log('YES!!!');
                        var tiny = this.src;
                        var big = tiny.replace('&w=50&q=50&nocache','&w=150&q=80');
                        tooltip.show('<img src="'+big+'" />');
                    },
                    function() {
                        // mouseout
                        tooltip.hide();
                    }
                );
        });
    },
    
    imagediting: function() {
        $("#mediabrowser input.text").alphanumeric({allow:' ()%$#@!./}{\"_;:^*&,\''});
        $("#mediabrowser #textarea").alphanumeric({allow:'  ()%$#@!./}{\"_-;:^*&,\''});
    },
    
    filters: function() {
        $("#mediabrowser select.filter").each(function(){
            $(this).change(function(){
                var name = $(this).attr("name");
                var value = $(this).getValue();
                $.ajax({
                    type: "GET",
                    url: base+"lib/php/ajax/browser.php?" + name + "=" + value,
                    contentType: 'text/html; charset=UTF-8',
                    dataType: 'html',
                    cache: false,
                    success: function(response){
                        document.getElementById('mediabrowser').innerHTML = response;
                        mediabrowser.onload();
                    }
                });
            });
        });
    },
    
    pagination: function() {
        $("#mediabrowser #pagination a").click(function(){
            var page = $(this).data("page");
            $.ajax({
                type: "GET",
                url: base+"lib/php/ajax/browser.php?page="+page,
                contentType: 'text/html; charset=UTF-8',
                dataType: 'html',
                cache: false,
                success: function(response){
                    document.getElementById('mediabrowser').innerHTML = response;
                    mediabrowser.onload();
                }
            });
            return false;
        });
    },
    
    imagedelete: function() {
        // delete image
        $("#mediabrowser a.delete").click(function(){
            if(confirm("Are you sure you want to delete this image?")) {
                var href = $(this).attr("href");
                var parent = $(this).parent();
                $.ajax({
                    type: "GET",
                    url: href,
                    contentType: 'text/xml; charset=UTF-8',
                    dataType: 'xml',
                    cache: false,
                    success:
                    function(response){
                        if ($(response).find("response").attr("isSuccess") === "true") {
                            mediabrowser.init();
                        } else {
                            alert($(response).find("response message").text());
                        }
                        return false;
                    }
                });
            }
            return false;
        });
    },
    
    filesize: function() {
        // bytes to size
        $("td.filesize").each(function(){
            var orig = $(this).text();
            if (orig === "undefined" || orig === "" || orig === " ") {
                $(this).text("0");
            } else {
                $(this).text(mediabrowser.bytesToSize(orig));
            }
        });
    },
    
    /**
     * formats byte value into human readable string
     */
    bytesToSize: function(bytes){
        var i = -1;
        do {
            bytes = bytes / 1024;
            i++;  
        } while (bytes > 99);
        return Math.max(bytes, 0.1).toFixed(1) + ['KB', 'MB', 'GB', 'TB', 'PB', 'EB'][i];
    },
    
    imagerotate: function() {
        $("#mediabrowser a.rotate").click(function(){
            var parent = $(this).parent();
            var href = $(this).attr("href");
            var img = parent.find("img:eq(0)");
            img.animate({"opacity":0},200);
            parent.css({"backgroundImage":"url('/lib/img/loading.gif')", "backgroundRepeat":"no-repeat", "backgroundPosition":"25px 50%"});
            $.ajax({
                type: "GET",
                url: href,
                success: function(response){
                    if ($(response).find("response").attr("isSuccess") === "true") {
                        var img = parent.find("img:eq(0)");
                        var src = img.attr("src").replace('&nocache','') + '&nocache';
                        img.attr("src", src);
                        $(img).load(function(){
                            img.animate({"opacity":1},200);
                            parent.css({"backgroundImage":""});
                        });
                    } else {
                        alert($(response).find("message").text());
                    }
                    return false;
                }
            });
            return false;
        });
    },
};


(function ($) {
    // wrap all jQuery stuff in here for compatibility with .noConflict()
    
    // dom ready
    $(document).ready(function(){
        $.ajaxSetup({ cache: true });
        $("body:eq(0)").addClass("jquery");
        login();
        admin();
    });
    
    // login screen
    function login() {
        if ($("body.login")) {
            $("body.login #username").focus();
        }
    }
    
    // update folder path
    function updateFolderPath() {
        var orig = $("#addfolder input[name=title]").getValue();
        if ($.trim(orig) !== '') {
            $.ajax({
                type: "GET",
                url: base+"lib/php/ajax/foldertitle.php",
                data: "title="+orig,
                success:
                function(response){
                    if ($(response).find("response").attr("isSuccess") === "true") {
                        var title = $(response).find("data:eq(0)").text();
                        var parent = $("#addfolder #parent").getValue();
                        var physical_path = parent + '/' + title;
                        $("#physical_path").setValue(physical_path);
                    } else {
                        alert($(response).find("message").text());
                    }
                }
            });
        } else {
            var parent = $("#addfolder #parent").getValue();
            var physical_path = parent + '/';
            $("#physical_path").setValue(physical_path);
        }
    }
    
    // admin cp
    function admin() {
        if ($("body.admin")) {
            // skinned selects
            $.getScript(base+"lib/js/jquery.select_skin.js").done(function(){
                $("select").select_skin();
            });
            
            // tabs and tab history
            $.getScript(base+"lib/js/jquery.tools.tabs.js").done(function(){
                $.getScript(base+"lib/js/jquery.tools.history.js").done(function(){
                    $("ul.tabs").tabs("div.panes > div", { history: true });
                });
            });
            
            // load browser if the visible tab is the browser.
            if (document.location.hash === "#browser") {
                mediabrowser.init();
            }
            
            $("a[href='#browser']").click(function(){
                mediabrowser.init();
            });
            
            // ajax fileupload
            $.getScript(base + "lib/js/fileuploader.js").done(function(){
                $.getScript(base+"lib/js/jquery.field.js").done(function(){
                    $("#uploadmedia").parents("form:eq(0)").find("input.submit").hide();
                    if ($("input.file").length > 0) {
                        var uploader = new qq.FileUploader({
                            element: document.getElementById("uploadmedia"),
                            action: base + 'lib/php/ajax/upload.php'
                        });
                    }
                });
            });
            
            // zebra style
            $("ol.folders li:not('ul li')").each(function(i){
                // use modulo for zebra style
                if ((Math.floor(i / 2) - (i / 2)) == 0) {
                    $(this).addClass("even");
                } else {
                    $(this).addClass("odd");
                }
            });
            // folder name and description 
            $("body.admin div.folders div.folderdetails").hide();
            $("body.admin div.folders ol.folders a.edit").each(function(){
                $(this).click(function(){
                    $("div.folderdetails").hide();
                    $(this).parents("li:eq(0)").find("div.folderdetails:eq(0)").show();
                    return false;
                });
            });
            
            $("body.admin div.folders ol.folders div.folderdetails a.close").click(function(){
                $(this).parents("div.folderdetails").hide();
                return false;
            });
            
            // title to pathname
            $("#addfolder input[name=title]").keyup(function(){
                $.getScript(base+"lib/js/jquery.field.js").done(function(){
                    updateFolderPath();
                });
            });
            
            $("#addfolder select[name='parent']").change(function(){
                $.getScript(base+"lib/js/jquery.field.js").done(function(){
                    updateFolderPath();
                });
            });
            
            // remove single white space of descriptions on focus
            $("#mediabrowser td.description textarea").focus(function(){
                $(this).val($.trim($(this).val()));
            });
            
            // recursive folder delete
            $("#deletefolder").submit(function(){
                if(confirm("Are you sure you want to delete this fodler?")) {
                    return true;
                } else {
                    return false;
                }
            });
        }
    }
})(jQuery);