<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" exclude-result-prefixes="xsl xsi html xsd" xmlns="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:html="http://www.w3.org/1999/xhtml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <xsl:output method="xml" indent="no" encoding="UTF-8" omit-xml-declaration="yes" />
    <xsl:include href="./common.xsl"/>
    <xsl:strip-space elements="*" />
    <xsl:param name="view" />
    <xsl:param name="action" />
    <xsl:param name="element" />
    <xsl:param name="loggedinUser" />
    <xsl:param name="base" />
    <xsl:param name="debug" select="'0'" />
    <xsl:variable name="base_with_slash">
        <xsl:choose>
            <xsl:when test="substring($base,-1) != '/'"><xsl:value-of select="$base" />/</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$base" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

    <xsl:template match="/">
        <html lang="en">
        <head>
            <title>
                <xsl:choose>
                    <xsl:when test="$view = 'admin'">Folderblog <xsl:value-of select="/folderblog/@version" /></xsl:when>
                    <xsl:when test="$view='image' or $view='audio' or $view='video'"><xsl:value-of select="/folderblog/settings/title" /> | <xsl:value-of select="//element[file='$element']/title" /></xsl:when>
                    <xsl:otherwise><xsl:value-of select="/folderblog/settings/title" /><xsl:text> </xsl:text></xsl:otherwise>
                    <xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
                </xsl:choose>
            </title>
            <link href="{$base_with_slash}lib/img/favicon.ico" rel="icon" type="image/x-icon" />
            <link rel="stylesheet" type="text/css" href="{$base_with_slash}lib/css/styles.css" />
            <link rel="stylesheet" type="text/css" href="{$base_with_slash}lib/css/fileuploader.css" />
        </head>
        <body class="{$view}">
            <xsl:if test="$debug='1'">
            <pre id="debug">
            User: <xsl:value-of select="$loggedinUser" />
            Base: <xsl:value-of select="$base" />
            View: <xsl:value-of select="$view" />
            Action: <xsl:value-of select="$action" />
            Element: <xsl:value-of select="$element" />
            </pre>
            </xsl:if>
            <xsl:choose>
                <!-- <xsl:when test="$view='' or $view='index'"><xsl:call-template name="index" /></xsl:when> -->
                <xsl:when test="$view='login'"><xsl:call-template name="login" /></xsl:when>
                <xsl:when test="$view='admin'"><xsl:call-template name="admin" /></xsl:when>
                <xsl:when test="$view='setup'"><xsl:call-template name="setup" /></xsl:when>
                <xsl:otherwise><xsl:call-template name="error404" /></xsl:otherwise>
            </xsl:choose>
            <span class="credits"><a href="http://erikpoehler.com/folderblog/">Folderblog <xsl:value-of select="/folderblog/@version" /></a></span>
            <script type="text/javascript"><xsl:comment>
                var base = "<xsl:value-of select="$base_with_slash" />";
            </xsl:comment></script>
            <script type="text/javascript" src="{$base_with_slash}lib/js/jquery.js"><xsl:comment></xsl:comment></script>
            <script type="text/javascript" src="{$base_with_slash}lib/js/jquery.field.js"><xsl:comment></xsl:comment></script>
            <script type="text/javascript" src="{$base_with_slash}lib/js/jquery.alphanumeric.js"><xsl:comment></xsl:comment></script>
            <script type="text/javascript" src="{$base_with_slash}lib/js/folderblog.js"><xsl:comment></xsl:comment></script>
        </body>
        </html>
    </xsl:template>
    
    <xsl:template name="login">
        <form id="login" action="{$base_with_slash}{$view}/" method="post" class="box">
            <h1>folderblog <span class="light">login</span></h1>
            <div>
                <label class="screenreader" for="username">Username</label>
                <input type="text" autocomplete="off" id="username" name="username" maxlength="200" length="20" class="text username" value="Username" />
                <label class="screenreader" for="password">Password</label>
                <input type="text" id="password" name="password" maxlength="200" length="20" class="password" value="Password" />
                <input type="submit" value="Login" class="submit" />
            </div>
            <br />
            <a href="{$base_with_slash}">Back to index</a>
        </form>
    </xsl:template>
    
    <xsl:template name="setup">
        <form id="setup" action="{$base_with_slash}" method="post" class="box">
            <h1>folderblog <span class="light">setup</span></h1>
            <p><strong>Welcome to Folderblog 4.0.</strong><br />You are seeing this page, because you are opening your Folderblog installation for the first time. You will be able to change the password later or add new users.
            <br />Please choose a title for your Folderblog and set a password for Folderblog's main user <strong><samp>superuser</samp></strong> now.</p>
            
            <div>
                <label for="title"><strong>Choose a title</strong></label><br />
                <input type="text" autocomplete="off" id="title" name="title" maxlength="200" length="20" class="text title" value="" /><br />
                
                <label for="superuser"><strong>Password for superuser</strong></label><br />
                <input type="text" autocomplete="off" id="superuser" name="superuser" maxlength="200" length="20" class="text superuser" value="" /><br />
                
                <label for="admin"><strong>Repeat password for superuser</strong></label><br />
                <input type="text" id="repeat" name="repeat" maxlength="200" length="20" class="text superuser" value="" /><br /><br />
                <input type="hidden" name="action" value="setup" />
                
                <p>You're settings will be saved and you'll be redirected to the login screen where you can sign in using username superuser and the password you just chose.</p>
                <input type="submit" value="Save" class="submit" />
            </div>
        </form>
    </xsl:template>
    
    <xsl:template name="error404">
        <span>I am the error page.</span>
    </xsl:template>
    
    <xsl:template name="editfolder">
        <div class="folderdetails">
            <div class="inner">
                <a href="#" class="close">Cancel</a>
                <form action="{$base_with_slash}{$view}/#folders" method="post" id="folder{position()}">
                    <ul>
                        <li>
                            <label for="title">Title</label>
                            <input type="text" class="text" name="title" id="title" value="{title}" />
                        </li>
                        <li>
                            <label for="description">Description</label>
                            <textarea name="description" id="description"><xsl:value-of select="description" /><xsl:text> </xsl:text></textarea>
                        </li>
                        <li>
                            <input type="hidden" name="action" value="editfolder" />
                            <input type="hidden" name="path" value="{path}" />
                            <input type="submit" class="submit" value="Save" />
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </xsl:template>
    
    <xsl:template match="folder">
        <li>
            <span class="inner">
            <img src="{$base_with_slash}lib/img/folder.png" alt="Folder" style="vertical-align:-2px;margin-right:4px;" /><strong><samp><xsl:value-of select="path" /></samp><xsl:text> </xsl:text></strong><xsl:value-of select="title" /> <xsl:text> </xsl:text>(<xsl:value-of select="count(elements/element)" /> Elements)<xsl:text> </xsl:text><a class="edit" href="#edit">Edit</a><br />
            </span>
            <xsl:call-template name="editfolder" />
            <xsl:if test="*[folder]">
                <ol>
                    <xsl:apply-templates select="folders/folder" />
                </ol>
            </xsl:if>
        </li>
    </xsl:template>
    
    <xsl:template name="admin">
        <div class="box">
            <h1>folderblog <span class="light">admin</span></h1>
            <span class="warn">Please enable Javascript in your Browser to make full use of Foderblogs Functionality. Non-Javascript use is on your own risk.</span>
            <div class="user"><xsl:if test="$loggedinUser!=''">Welcome back <xsl:value-of select="/folderblog/users/user[@name=$loggedinUser]/@nicename" /></xsl:if>! <a href="{$base_with_slash}logout/">Logout</a> | <a href="{$base_with_slash}" target="_blank">Go to Website</a></div>
            <div class="tabs">
                <ul class="tabs">
                    <li><a href="#folders">Folders</a></li>
                    <li><a href="#browser">Browser</a></li>
                    <li><a href="#media">Uploader</a></li>
                    <!-- <li><a href="#comments">Comments</a></li>
                    <li><a href="#tags">Tags</a></li> -->
                    <li><a href="#settings">Settings</a></li>
                    <li><a href="#users">Users</a></li>
                </ul>
            </div>
            <div class="panes">
                <div class="folders">
                    <h2>About</h2>
                    <p>A folder in folderblog acts like a category or album for your media files. This feature is intended for people who'd like organize their media in albums. For a simple, chronological photolog or podcast there is no need to create more folders (but you can). Start uploading media <a href="#media">here</a>.</p>
                    <h2>Edit Folders</h2>
                    <ol class="folders">
                        <xsl:for-each select="/folderblog/folder">
                            <xsl:sort select="path"/>
                            <xsl:apply-templates select="." />
                        </xsl:for-each>
                    </ol>
                    <hr class="clear" style="margin-top:2em;"/>
                    <div style="width:50%;float:left;">
                        <h2>Create new folder</h2>
                        <form action="{$base_with_slash}{$view}/#folders" method="post" id="addfolder">
                            <ol>
                                <li>
                                    <label for="parent">Create in this parent directory</label>
                                    <select name="parent" id="parent">
                                        <option value="/folders">/folders (default)</option>
                                        <xsl:for-each select="/folderblog/folder/folders//folder">
                                            <xsl:sort select="path"/>
                                                <option value="{path}"><xsl:value-of select="path" /></option>
                                        </xsl:for-each>
                                    </select>
                                    <label for="title">Title</label>
                                    <input type="text" class="text" name="title" id="title" value="" />
                                    <label>Physical folder:</label>
                                    <input type="text" class="text" id="physical_path" value="/folders" readonly="readonly" />
                                    <br />
                                    <label for="description">Description</label>
                                    <textarea name="description" id="description"><xsl:text> </xsl:text></textarea>
                                </li>
                            </ol>
                            <input type="hidden" name="action" value="addfolder" />
                            <input type="submit" class="big submit" value="Add Folder" />
                        </form>
                    </div>
                    <div style="width:50%;margin-left:50%;">
                        <h2>Recursive Folder Deletion</h2>
                        <form id="deletefolder" method="post" action="{$base_with_slash}{$view}/#folders">
                            <p>Deleting a folder means deleting all images and subfolders within it (including its tags, title, descriptions, media contents and comments). <em>Folderblog will automatically store a .zip backup of the directory in <samp>/cache/backups</samp> and a backup of its main data file in <samp>/cache/data</samp></em>. in case you want to undo your change at a later time.</p>
                            <ol>
                                <li>
                                    <label for="path">Choose a folder to delete</label>
                                    <select name="path" id="path">
                                        <option value="">Choose a path</option>
                                        <xsl:for-each select="/folderblog/folder/folders//folder">
                                            <xsl:sort select="path"/>
                                            <option value="{path}"><xsl:value-of select="concat(path,' - ',count(./elements//element),' Elements')" /></option>
                                        </xsl:for-each>
                                    </select>
                                </li>
                            </ol>
                            <input type="hidden" name="action" value="deletefolder" />
                            <input type="submit" class="submit" value="delete" />
                        </form>
                    </div>
                    <hr class="clear" />
                </div>
                <div class="browser">
                    <h2>Media Browser</h2>
                    <div class="content" id="mediabrowser"><xsl:text> </xsl:text></div>
                </div>
                <div class="media">
                    <h2>Upload new Media</h2>
                    <p>A common use of Folderblog is to run your personal photo log with it. Also, you can blog videos and audio files with Folderblog. To simplify uploading, you can submit zip or tar.gz archives as well. These will be extracted to the album of your choice.</p>
                    <form id="addmedia" action="{$base_with_slash}{$view}/#media" method="post" enctype="multipart/form-data">
                        <ol>
                            <li>
                                <label for="folder">Choose the folder you want to add media to:</label>
                                <select name="path" id="path">
                                    <xsl:for-each select="/folderblog//folder">
                                        <xsl:sort select="path"/>
                                        <option value="{path}"><xsl:value-of select="path" /></option>
                                    </xsl:for-each>
                                </select>
                            </li>
                            <li>
                                <label for="uploadmedia">Choose one or multiple files to upload</label>
                                <div id="uploadmedia"><xsl:text> </xsl:text></div>
                                <input class="file" type="file" style="display:none;" />
                            </li>
                            <li>
                                <input type="hidden" name="action" value="addmedia" />
                                <input type="submit" class="submit" value="Upload File" />
                            </li>
                        </ol>
                    </form>
                </div>
                <!-- 
                <div class="comments">
                    <hr class="clear" />
                    <h2>Manage Comments</h2>
                    <p>Not implemented yet. Remember this is still beta...</p>
                </div>
                <div class="tags">
                    <hr class="clear" />
                    <h2>Manage Tags</h2>
                    <p>Not implemented yet. Remember this is still beta...</p>
                </div>
                 -->
                <div class="settings">
                    <hr class="clear" />
                    <form id="" method="post" action="{$base_with_slash}{$view}/#settings">
                        <div style="float:left; width:50%;margin-top:-9px;">
                            <h2>General Settings</h2>
                            <ol style="float:left;">
                                <li>
                                    <label for="title">Title</label>
                                    <input type="text" autocomplete="off" id="title" name="title" maxlength="300" length="20" class="text username" value="{/folderblog/settings/title}" />
                                </li>
                                
                                <li>
                                    <label for="comment_email">Comment Notification Email</label>
                                    <input type="text" autocomplete="off" id="comment_email" name="comment_email" maxlength="300" length="20" class="text username" value="{/folderblog/settings/comment_email}" />
                                </li>
                                
                                <li>
                                    <label for="home_link">Define a custom Home Link</label>
                                    <input type="text" autocomplete="off" id="home_link" name="home_link" maxlength="300" length="20" class="text home_link" value="{/folderblog/settings/home_link}" />
                                    <p><small style="display:block;font-size:85%;width:250px;">You can use this field, for example, if you likke to point to a different section of your website</small></p>
                                </li>
                                
                                <li>
                                    <input type="checkbox" name="enable_comments" id="enable_comments" value="1" class="checkbox">
                                        <xsl:if test="/folderblog/settings/enable_comments='1'">
                                            <xsl:attribute name="checked">checked</xsl:attribute>
                                        </xsl:if>
                                    </input>
                                    <label for="enable_comments" class="inline">Enable comments?</label>
                                </li>
                                
                                <li>
                                    <input type="checkbox" id="require_captcha" name="require_captcha" class="checkbox" value="1">
                                        <xsl:if test="/folderblog/settings/require_captcha='1'">
                                            <xsl:attribute name="checked">checked</xsl:attribute>
                                        </xsl:if>
                                    </input>
                                    <label for="require_captcha" class="inline">Require Captcha</label>
                                </li>
                                
                                <li>
                                    <input type="checkbox" id="moderate_comments" name="moderate_comments" class="checkbox" value="1">
                                        <xsl:if test="/folderblog/settings/moderate_comments='1'">
                                            <xsl:attribute name="checked">checked</xsl:attribute>
                                        </xsl:if>
                                    </input>
                                    <label for="moderate_comments" class="inline">Moderate Comments</label>
                                </li>
                                
                                <li>
                                    <label for="theme" class="inline">Active Theme</label>
                                    <select id="theme" name="theme">
                                        <xsl:for-each select="/folderblog/settings/themes/theme">
                                            <option value="{@dirname}">
                                            <xsl:if test="/folderblog/settings/theme=id">
                                                <xsl:attribute name="selected">selected</xsl:attribute>
                                            </xsl:if><xsl:value-of select="name" /> v<xsl:value-of select="version" /></option>
                                        </xsl:for-each>
                                    </select>
                                </li>
                                
                                <li>
                                    <input type="checkbox" id="minify_css" name="minify_css" class="checkbox" value="1">
                                        <xsl:if test="/folderblog/settings/minify_css='1'">
                                            <xsl:attribute name="checked">checked</xsl:attribute>
                                        </xsl:if>
                                    </input>
                                    <label for="minify_css" class="inline">Minify Stylesheets</label>
                                </li>
                                
                                <li>
                                    <input type="checkbox" id="minify_js" name="minify_js" class="checkbox" value="1">
                                        <xsl:if test="/folderblog/settings/minify_js='1'">
                                            <xsl:attribute name="checked">checked</xsl:attribute>
                                        </xsl:if>
                                    </input>
                                    <label for="minify_js" class="inline">Minify Javascript</label>
                                </li>
                                
                                <li>
                                    <input type="checkbox" id="profile" name="profile" class="checkbox" value="1">
                                        <xsl:if test="/folderblog/settings/profile='1'">
                                            <xsl:attribute name="checked">checked</xsl:attribute>
                                        </xsl:if>
                                    </input>
                                    <label for="profile" class="inline">Show profiling/debugging info</label>
                                </li>
                            </ol>
                            <hr class="clear" />
                        </div>
                        <div style="margin-left:50%; margin-top:0;">
                            <h2>Appearance</h2>
                            <ol>
                                <li>
                                    <label for="sorting">Sort Elements</label>
                                    <select name="sorting" id="sorting" class="select">
                                        <option value="no">
                                            <xsl:if test="/folderblog/settings/sorting = 'no'">
                                                <xsl:attribute name="selected">selected</xsl:attribute>
                                            </xsl:if>
                                            No Sorting
                                        </option>
                                        <option value="filetime">
                                            <xsl:if test="/folderblog/settings/sorting = 'filetime'">
                                                <xsl:attribute name="selected">selected</xsl:attribute>
                                            </xsl:if>
                                            Filetime
                                        </option>
                                        <option value="filename">
                                            <xsl:if test="/folderblog/settings/sorting = 'filename'">
                                                <xsl:attribute name="selected">selected</xsl:attribute>
                                            </xsl:if>
                                            Filename
                                        </option>
                                        <option value="exiftime">
                                            <xsl:if test="/folderblog/settings/sorting = 'exiftime'">
                                                <xsl:attribute name="selected">selected</xsl:attribute>
                                            </xsl:if>
                                            Exif Taken time
                                        </option>
                                    </select>
                                </li>
                                 
                                <li>
                                    <label for="order">Sort Elements</label>
                                    <select name="order" id="order" class="select">
                                        <option value="descending">
                                            <xsl:if test="/folderblog/settings/order = 'descending'">
                                                <xsl:attribute name="selected">selected</xsl:attribute>
                                            </xsl:if>
                                            Descending
                                        </option>
                                        <option value="ascending">
                                            <xsl:if test="/folderblog/settings/order = 'ascending'">
                                                <xsl:attribute name="selected">selected</xsl:attribute>
                                            </xsl:if>
                                            Ascending
                                        </option>
                                    </select>
                                </li>
                                
                                <li>
                                    <label for="thumb_maxsize">Thumbnail Maxsize</label>
                                    <input type="text" autocomplete="off" id="thumb_maxsize" name="thumb_maxsize" maxlength="300" length="20" class="text" value="{/folderblog/settings/thumb_maxsize}" /> px
                                </li>
                                
                                <li>
                                    <label for="image_maxwidth">Image Max Width</label>
                                    <input type="text" autocomplete="off" id="image_maxwidth" name="image_maxwidth" maxlength="300" length="20" class="text" value="{/folderblog/settings/image_maxwidth}" /> px
                                </li>
                                
                                <li>
                                    <label for="image_maxheight">Image Max Height</label>
                                    <input type="text" autocomplete="off" id="image_maxheight" name="image_maxheight" maxlength="300" length="20" class="text" value="{/folderblog/settings/image_maxheight}" /> px
                                </li>
                                
                                <li>
                                    <input type="checkbox" id="square_thumbs" name="square_thumbs" class="checkbox" value="1">
                                        <xsl:if test="/folderblog/settings/square_thumbs='1'">
                                            <xsl:attribute name="checked">checked</xsl:attribute>
                                        </xsl:if>
                                    </input>
                                    <label for="square_thumbs" class="inline">Square Thumbnails</label>
                                </li>
                                
                                <li>
                                    <input type="checkbox" id="crop_images" name="crop_images" class="checkbox" value="1">
                                        <xsl:if test="/folderblog/settings/crop_images='1'">
                                            <xsl:attribute name="checked">checked</xsl:attribute>
                                        </xsl:if>
                                    </input>
                                    <label for="crop_images" class="inline">Crop Images?</label>
                                </li>
                                
                                <li>
                                    <label for="show_thumbs" class="inline">Show Thumbs</label>
                                    <select name="show_thumbs" id="show_thumbs" class="select">
                                        <xsl:call-template name="options">
                                            <xsl:with-param name="count" select="20"/>
                                            <xsl:with-param name="start" select="1"/>
                                        </xsl:call-template>
                                    </select>
                                </li>
                            </ol>
                        </div>
                        <input type="hidden" name="action" value="settings" />
                        <input class="submit big" type="submit" value="Save Changes" />
                    </form>
                </div>
                <div class="users">
                    <hr class="clear" />
                    <h2>Edit Users</h2>
                    <form id="users" method="post" action="{$base_with_slash}{$view}/#users">
                        <ol>
                            <xsl:for-each select="/folderblog/users/user">
                                <li>
                                    
                                    <dl>
                                        <dt>
                                            <strong><xsl:value-of select="@name" /></strong>
                                        </dt>
                                        <dd>
                                            <label for="nicename{position()}">Display Name</label>
                                            <input type="text" class="text" name="user[{position()}][nicename]" id="nicename{position()}" value="{@nicename}">
                                                <xsl:if test="@name='superuser' and $loggedinUser != 'superuser'">
                                                    <xsl:attribute name="readonly">readonly</xsl:attribute>
                                                </xsl:if>
                                            </input>
                                        </dd>
                                        <dd>
                                            <label for="user[][new_password]">New Password:</label>
                                            <input type="text" class="text" name="user[{position()}][new_password]" id="new_password{position()}" value="">
                                                <xsl:if test="@name='superuser' and $loggedinUser != 'superuser'">
                                                    <xsl:attribute name="readonly">readonly</xsl:attribute>
                                                </xsl:if>
                                            </input>
                                        </dd>
                                        <dd>
                                            <label for="new_password_repeat{position()}">Repeat New Password:</label>
                                            <input type="text" class="text" name="user[{position()}][new_password_repeat]" id="new_password_repeat{position()}" value="">
                                                <xsl:if test="@name='superuser' and $loggedinUser != 'superuser'">
                                                    <xsl:attribute name="readonly">readonly</xsl:attribute>
                                                </xsl:if>
                                            </input>
                                        </dd>
                                        <dd>
                                            <label for="old_password{position()}">Old Password:</label>
                                            <input type="text" class="text" name="user[{position()}][old_password]" id="old_password{position()}" value="">
                                                <xsl:if test="@name='superuser' and $loggedinUser != 'superuser'">
                                                    <xsl:attribute name="readonly">readonly</xsl:attribute>
                                                </xsl:if>
                                            </input>
                                        </dd>
                                    </dl>
                                </li>
                            </xsl:for-each>
                        </ol>
                        <hr class="clear" />
                        <input type="hidden" name="action" value="users" />
                        <input type="submit" class="big submit" value="Save Changes" />
                    </form>
                    
                    <hr class="clear" style="margin-top:3em;" />
                    
                    <div style="width:50%; float:left; clear:both; ">
                        <h2>Create new user</h2>
                        <form id="adduser" method="post" action="{$base_with_slash}{$view}/#users">
                            <ol>
                                <li>
                                    <label for="username">Username</label>
                                    <input type="text" class="text" name="username" id="username" value="" />
                                </li>
                                <li>
                                    <label for="nicename">User Display Name</label>
                                    <input type="text" class="text" name="nicename" id="nicename" value="" />
                                </li>
                                <li>
                                    <label for="nicename">Password (minimum 4 characters)</label>
                                    <input type="text" class="text" name="password" id="password" value="" />
                                </li>
                            </ol>
                            <input type="hidden" name="action" value="adduser" />
                            <input type="submit" class="big submit" value="Create new user" />
                        </form>
                    </div>
                    
                    <div style="margin-left:50%;">
                        <h2>Delete user</h2>
                        <form id="deleteuser" method="post" action="{$base_with_slash}{$view}/#users">
                            <ol>
                                <li>
                                    <label for="deleteuser">Choose a user to delete</label>
                                    <select name="user" id="user">
                                        <option value="">Choose user</option>
                                        <xsl:for-each select="/folderblog/users/user[@name != 'superuser']">
                                            <option value="{@name}"><xsl:value-of select="concat(@name,' - ',@nicename)" /></option>
                                        </xsl:for-each>
                                    </select>
                                </li>
                            </ol>
                            <input type="hidden" name="action" value="deleteuser" />
                            <input type="submit" class="submit" value="delete" />
                        </form>
                    </div>
                    <hr class="clear" />
                </div>
            </div>
        </div>
    </xsl:template>
    
    <xsl:template name="options">
        <xsl:param name="count" select="1"/>
        <xsl:param name="start" select="1"/>
        <xsl:if test="$count > 0">
            <option>
                <xsl:attribute name="value"><xsl:value-of select="$start" /></xsl:attribute>
                <xsl:if test="$start = /folderblog/settings/show_thumbs">
                    <xsl:attribute name="selected">selected</xsl:attribute>
                </xsl:if>
                <xsl:value-of select="$start" />
            </option>
            <xsl:call-template name="options">
                <xsl:with-param name="count" select="$count - 1" />
                <xsl:with-param name="start" select="$start + 1" />
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>