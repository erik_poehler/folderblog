<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet exclude-result-prefixes="xsl xsi html xsd" version="1.0" xmlns="http://www.w3.org/1999/xhtml" xmlns:html="http://www.w3.org/1999/xhtml" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output encoding="UTF-8" indent="no" method="xml" omit-xml-declaration="yes"/>
    <xsl:include href="./common.xsl"/>
    <xsl:strip-space elements="*"/>
    <xsl:param name="page" select="'1'" />
    <xsl:param name="folder" />
    <xsl:param name="results_per_page" select="'25'"/>
    <xsl:param name="command" />
    <xsl:param name="base" />
    
    <xsl:template match="/">
        <div>
        <div style="float:left;width:33%">
        <strong>Choose a Folder:</strong>
        <select id="folder" name="folder" style="display:inline;" class="filter">
            <xsl:for-each select="/folderblog//folder">
                <xsl:sort select="path"/>
                <option value="{path}">
                    <xsl:if test="path = $folder">
                        <xsl:attribute name="selected">selected</xsl:attribute>
                    </xsl:if>
                    <xsl:value-of select="path"/>
                </option>
            </xsl:for-each>
        </select>
        </div>
        
        <div style="margin-left:33%">
        <strong>Results per page</strong>
        <select id="results_per_page" name="results_per_page" style="display:inline;" class="select small filter">
            <option value="10">
                <xsl:if test="$results_per_page = '10'">
                    <xsl:attribute name="selected">selected</xsl:attribute>
                </xsl:if>10</option>
            <option value="25">
                <xsl:if test="$results_per_page = '25'">
                    <xsl:attribute name="selected">selected</xsl:attribute>
                </xsl:if>25</option>
            <option value="50">
                <xsl:if test="$results_per_page = '50'">
                    <xsl:attribute name="selected">selected</xsl:attribute>
                </xsl:if>50</option>
        </select>
        </div>
        <hr class="clear" />
        <!-- Pagination config -->
        <xsl:variable name="pageNumber" select="number($page)" />
        <xsl:variable name="recordsPerPage" select="number($results_per_page)" />
        <xsl:variable name="numberOfRecords" select="count(//folder[path=$folder]//element)" />
        <form action="{$base}/admin/#browser" method="post">
            <input class="big submit" style="float:right;margin-bottom:10px;" type="submit" value="save changes"/>
            <table id="mediabrowser" style="clear:right;" width="100%">
                <thead>
                    <th class="preview">Preview</th>
                    <th>Type</th>
                    <th>Filename</th>
                    <th>Upload Date</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Size</th>
                </thead>
                <tbody>
                    <xsl:for-each select="//folder[path=$folder]//element">
                        <xsl:sort data-type="number" order="descending" select="uploaded"/>
                        <xsl:if test="position() &gt; $recordsPerPage * number($pageNumber -1) and position() &lt;= number($recordsPerPage * number($pageNumber -1) + $recordsPerPage)">
                        <tr>
                            <xsl:if test="position() mod 2 = 1">
                                <xsl:attribute name="class">odd</xsl:attribute>
                            </xsl:if>
                            <td class="preview">
                                <img alt="Preview {title}" src="{$base}/lib/php/phpthumb/phpThumb.php?src={$base}{path}&amp;w=50&amp;q=50&amp;nocache" style="margin-right:10px;" width="50"/>
                                <a class="delete" href="{$base}/lib/php/ajax/delete.php?file={path}">Delete</a>
                                <xsl:text> </xsl:text>
                                <a class="rotate" href="{$base}/lib/php/ajax/rotate.php?file={path}">Rotate</a>
                            </td>
                            <td class="type">
                                <xsl:value-of select="type"/>
                            </td>
                            <td class="path">
                                <xsl:value-of select="substring(path,string-length(path)-20,21)"/>
                            </td>
                            <td class="date">
                                <xsl:call-template name="format_date">
                                    <xsl:with-param name="date" select="uploaded"/>
                                </xsl:call-template>
                            </td>
                            <td class="title">
                                <input class="text" name="details[{position()}][title]" type="text" value="{title}"/>
                            </td>
                            <td class="description">
                                <textarea cols="20" name="details[{position()}][description]" rows="2">
                                    <xsl:value-of select="description"/>
                                    <xsl:text> </xsl:text>
                                </textarea>
                            </td>
                            <input name="details[{position()}][path]" type="hidden" value="{$base}{path}"/>

                            <td class="filesize">
                                <xsl:value-of select="filesize"/>
                            </td>
                        </tr>
                        </xsl:if>
                    </xsl:for-each>
                </tbody>
            </table>
            
            <!-- Pagination -->
            <xsl:call-template name="pagination">
                <xsl:with-param name="pageNumber" select="$pageNumber"/>
                <xsl:with-param name="folder" select="$folder"/>
                <xsl:with-param name="recordsPerPage" select="$recordsPerPage"/>
                <xsl:with-param name="numberOfRecords" select="$numberOfRecords"/>
            </xsl:call-template>
            
            <input name="action" type="hidden" value="elementdetails"/>
            <input class="big submit" style="float:right;" type="submit" value="save changes"/>
            <hr class="space clear"/>
        </form>
        </div>
    </xsl:template>
</xsl:stylesheet>
