<?php
/**
 * Folderblog 4.0
 * @author Erik Pöhler
 * @version 4.0
 * @see http://folderblog.org/
 */
$profile = false;
$strict = false;
ini_set("display_errors","On");
ini_set('memory_limit', '64M');
ini_set('upload_max_filesize', '1000M');
ini_set('post_max_size', '1000M');
if($strict) error_reporting(E_STRICT);
else error_reporting(E_ALL ^ E_NOTICE ^ E_STRICT); // since php 5.4 srtict errors are part of e_all
//date_default_timezone_set("America/Mexico_City");
/**
 * set the base directory for installations that are not in document root
 * @var string FB_BASE
 */
if (!defined('FB_BASE')) {
	define('FB_BASE', str_replace($_SERVER["DOCUMENT_ROOT"],"",dirname(__FILE__)));
}

// if there is no mod_rewrite, go to server.php
if (getenv('HTTP_MOD_REWRITE') == "off") {
    header("Location: ".FB_BASE."/server.php");
    exit;
}
/**
 * Magic function __autoload()
 * @param string $class name of a class to be called.
 * @return void
 */
function __autoload($class) {
	if (is_file($_SERVER["DOCUMENT_ROOT"].FB_BASE."/lib/php/$class.php")) require_once $_SERVER["DOCUMENT_ROOT"].FB_BASE."/lib/php/$class.php";
}
define('FB_PROFILE', "off");
if (FB_PROFILE === "on") { file_put_contents($_SERVER["DOCUMENT_ROOT"] . FB_BASE . '/php-profiling.txt', "--- ".$_SERVER["REQUEST_URI"]. "---\n", FILE_APPEND); }
if (FB_PROFILE === "on") {
	$debug = new DebugUtilities();
	$debug->startDuration();
}

/**
 * Start session handling
 * @todo only use session if we are in the backend to avoid sending of cookie/session validation when not needed
 */

/**
 * Initialize the App
 * does nothing else but load a Folderblog instance
 */
Folderblog::getInstance()->run();

if (FB_PROFILE === "on") {
	echo "<pre id=\"profile\">Memory Usage: ".FilesystemFunctions::bytesToSize(memory_get_usage(),2)."\n";
	echo "Memory Peak Usage: ".FilesystemFunctions::bytesToSize(memory_get_peak_usage(),2)."\n";
	echo "Included Files: ".count(get_included_files())."\n";
	foreach (get_included_files() as $file) {
		echo "\t".str_replace($_SERVER["DOCUMENT_ROOT"],"",$file)."\n";
	}
	$debug->endDuration();
	echo $debug->showDurationStatistics();
	echo "</pre>";
}
?>