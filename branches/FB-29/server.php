<?php
ini_set("display_errors","Off");
error_reporting(E_ALL ^ E_CORE_ERROR ^ E_STRICT ^ E_NOTICE ^ E_WARNING);

register_shutdown_function( "fatal_handler" );

function fatal_handler() {
    $errfile = "error.log";
    $errstr  = "shutdown";
    $errno   = E_CORE_ERROR;
    $errline = 0;

    $error = error_get_last();

    if( $error !== NULL) {
        $errno   = $error["type"];
        $errfile = $error["file"];
        $errline = $error["line"];
        $errstr  = $error["message"];
    }
    file_put_contents("error.html",format_error( $errno, $errstr, $errfile, $errline ));
}

function format_error( $errno, $errstr, $errfile, $errline ) {
    $trace = print_r( debug_backtrace( false ), true );

    $content  = "<table><thead bgcolor='#c8c8c8'><th>Item</th><th>Description</th></thead><tbody>";
    $content .= "<tr valign='top'><td><b>Error</b></td><td><pre>$errstr</pre></td></tr>";
    $content .= "<tr valign='top'><td><b>Errno</b></td><td><pre>$errno</pre></td></tr>";
    $content .= "<tr valign='top'><td><b>File</b></td><td>$errfile</td></tr>";
    $content .= "<tr valign='top'><td><b>Line</b></td><td>$errline</td></tr>";
    $content .= "<tr valign='top'><td><b>Trace</b></td><td><pre>$trace</pre></td></tr>";
    $content .= '</tbody></table>';

    return $content;
}

if (!defined('FB_BASE')) {
    define('FB_BASE', str_replace($_SERVER["DOCUMENT_ROOT"],"",dirname(__FILE__)));
}

$messages = array();

// part 1 - test for mod_rewrite
$mod_rewrite = null;
if (function_exists('apache_get_modules')) {
    $mod_rewrite = in_array('mod_rewrite', apache_get_modules());
}
if (is_null($mod_rewrite) && isset($_SERVER["HTTP_MOD_REWRITE"])){
    $mod_rewrite = getenv('HTTP_MOD_REWRITE') == 'on' ? true : false ;
}
if (is_null($mod_rewrite)) {
    ob_start();
    phpinfo(INFO_MODULES);
    $contents = ob_get_contents();
    ob_end_clean();
    if(strpos('_'.$contents, 'mod_rewrite') > 0) {
        $mod_rewrite = true;
    }
}
if (is_null($mod_rewrite)) {
    $result = file_get_contents("http://".$_SERVER["SERVER_NAME"].FB_BASE."/skdjfjdshfjkfh");
    if (substr($result,0,14) === "<!DOCTYPE HTML>") $mod_rewrite = true;
}
// add some output
if (is_null($mod_rewrite)) {
    $messages["mod_rewrite"] = array(
        'error' => "is not available.",
        'status' => 'error',
        'solution' => "Activate the module with this line 'a2enmod rewrite' and then restart your server",
    );
} else {
    $messages["mod_rewrite"] = array('status' => 'success');
}

// part 2 - test for XSLT
if (@class_exists('XSLTProcessor')) {
    $messages["xslt"] = array('status' => 'success');
} else {
    $messages["xslt"] = array(
            'status' => 'error',
            'error' => $e->getMessage(),
            'solution' => "Activate xslt either by reconfiguring PHP or by running this command 'sudo apt-get install php5-xsl' on some distributions the command might look like this 'sudo apt-get install php-xsl'. Read more here...",
    );
}

// part 3 - test for tidy (optional)
if (extension_loaded("tidy") && function_exists('tidy_repair_string')) {
    $messages["html tidy"] = array('status' => 'success');
} else {
    $command = "tidy -version";
    exec($command, $return, $code);
    if (count($return) > 0) {
        $messages["html tidy"] = array(
            'status' => 'success',
            'error' => ' php module is not available. However, the command line version can be used.',
        );
    } else {
        $messages["html tidy"] = array(
            'status' => 'warning',
            'error' => 'is not available. There is a chance however that Folderblog can use the command line version.',
            'solution' => "You can install it like this 'apt-get install php5-tidy'",
        );
    }
}

// part 5 - test for GD/ImageMagick
$gd = false;
if (extension_loaded('gd') && function_exists('gd_info')) {
    $gd = true;
}

$imagick = false;
if (extension_loaded('imagick')) {
    $imagick = true;
}

if ($gd == false && $imagick == false) {
    // problem!
    $messages["GD"] = array(
            'status' => 'error',
            'error' => "is not installed and ImageMagick is missing, too. One of these two is required.",
            'solution' => "You can install it like this: 'sudo apt-get install php5-gd'.",
    );
    $messages["ImageMagick"] = array(
            'status' => 'error',
            'error' => "is not installed and GD is missing, too. One of these two is required.",
            'solution' => "You can install it like this: 'sudo apt-get install php5-imagick'.",
    );
} elseif ($gd == false || $imagick == false) {
    // one missing
    if ($gd) {
        $messages["GD"] = array('status' => 'success');
    } else {
        $messages["GD"] = array(
                'status' => 'warning',
                'error' => "is not installed, but ImageMagick seems to be around.",
                'solution' => "You could install it like this: 'sudo apt-get install php5-gd' (but phpThumb will prefer ImageMagick anyways).",
        );
    }
    if ($imagick) {
        $messages["ImageMagick"] = array('status' => 'success');
    } else {
        $messages["ImageMagick"] = array(
                'status' => 'warning',
                'error' => "is not installed but Folderblog can use GD instead.",
                'solution' => "ImageMagick would be preferred, though. Install it via 'sudo apt-get install php5-imagick'.",
        );
    }
} else {
    // both installed
    $messages["GD"] = array('status' => 'success');
    $messages["ImageMagick"] = array('status' => 'success');
}


// part 6 - Zip functionality
if (extension_loaded('zip')) {
    $messages["zip"] = array('status' => 'success');
} else {
    $messages["zip"] = array(
            'status' => 'error',
            'error' => "is not installed. It is optional, though.",
            'solution' => "Install it via 'sudo apt-get install zip'.",
    );
}

// @todo part 7 - htaccess
// part 4 - test for existence of folders and ownership/accessrights
if (!is_file($_SERVER["DOCUMENT_ROOT"].FB_BASE."/lib/data/folderblog.xml")) $messages["permissions"][] = array('status'=>'error','solution'=>"File <samp>/lib/data/folderblog.xml</samp> not found.");
if (!is_writeable($_SERVER["DOCUMENT_ROOT"].FB_BASE."/lib/data/folderblog.xml")) $messages["permissions"][] = array('status'=>'error','solution'=>"File <samp>/lib/data/folderblog.xml</samp> is not writeable.");

if (!is_dir($_SERVER["DOCUMENT_ROOT"].FB_BASE."/cache/data")) $messages["permissions"][] = array('status'=>'error','solution'=>"Folder <samp>/cache/data</samp> not found.");
if (!is_writeable($_SERVER["DOCUMENT_ROOT"].FB_BASE."/cache/data")) $messages["permissions"][] = array('status'=>'error','solution'=>"Folder <samp>/cache/data</samp> is not writeable.");

if (!is_dir($_SERVER["DOCUMENT_ROOT"].FB_BASE."/cache/backups")) $messages["permissions"][] = array('status'=>'error','solution'=>"Folder <samp>/cache/backups</samp> not found.");
if (!is_writeable($_SERVER["DOCUMENT_ROOT"].FB_BASE."/cache/backups")) $messages["permissions"][] = array('status'=>'error','solution'=>"Folder <samp>/cache/backups</samp> is not writeable.");

if (!is_dir($_SERVER["DOCUMENT_ROOT"].FB_BASE."/cache/min")) $messages["permissions"][] = array('status'=>'error','solution'=>"Folder <samp>/cache/min</samp> not found.");
if (!is_writeable($_SERVER["DOCUMENT_ROOT"].FB_BASE."/cache/min")) $messages["permissions"][] = array('status'=>'error','solution'=>"Folder <samp>/cache/min</samp> is not writeable.");

if (!is_dir($_SERVER["DOCUMENT_ROOT"].FB_BASE."/cache/images")) $messages["permissions"][] = array('status'=>'error','solution'=>"Folder <samp>/cache/images</samp> not found.");
if (!is_writeable($_SERVER["DOCUMENT_ROOT"].FB_BASE."/cache/images")) $messages["permissions"][] = array('status'=>'error','solution'=>"Folder <samp>/cache/images</samp> is not writeable.");

if (!is_dir($_SERVER["DOCUMENT_ROOT"].FB_BASE."/cache/source")) $messages["permissions"][] = array('status'=>'error','solution'=>"Folder <samp>/cache/source</samp> not found.");
if (!is_writeable($_SERVER["DOCUMENT_ROOT"].FB_BASE."/cache/source")) $messages["permissions"][] = array('status'=>'error','solution'=>"Folder <samp>/cache/source</samp> is not writeable.");

if (!is_dir($_SERVER["DOCUMENT_ROOT"].FB_BASE."/folders")) $messages["permissions"][] = array('status'=>'error','solution'=>"Folder <samp>/folders</samp> not found.");
if (!is_writeable($_SERVER["DOCUMENT_ROOT"].FB_BASE."/folders")) $messages["permissions"][] = array('status'=>'error','solution'=>"Folder <samp>/folders</samp> is not writeable.");


?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <title>Server Check</title>
        <link href="<?php echo rtrim(FB_BASE,'/'); ?>/lib/img/favicon.ico" rel="icon" type="image/x-icon">
        <link rel="stylesheet" type="text/css" href="<?php echo rtrim(FB_BASE,'/'); ?>/lib/css/styles.css">
    </head>
    <body class="admin jquery requirements">
        <div class="box">
            <h1>folderblog <span class="light">server check</span></h1>
<?php
echo '<ol class="requirements">';
foreach($messages as $key => $array) {
    if ($key !== "permissions") {
        echo '<li class="'.$array['status'].'"><dl><dt>'.ucfirst($key).'</dt>';
        if ($array['status'] == 'success') echo '<dd>is available. All good.</dd>';
        if (isset($array['error'])) echo '<dd>'.$array['error'].'</dd>';
        if (isset($array['solution'])) echo '<dd><em>'.$array['solution'].'</em></dd>';
        echo '</dl></li>';
    } else {
        $status = true;
        foreach($array as $entry) {
            if ($entry['status'] == 'error') $status = false;
        }

        if (!$status) {
            echo '<li class="error"><dl><dt>Permissions</dt>';
            foreach ($array as $entry) {
                echo "<dd>".$entry['solution']."</dd>";
                echo "<dd style=\"display:block;\">Make sure that this folder or file exists and that the owner is the same user as your Apache Server runs with.</dd>";
            }
            echo '</dl></li>';
        }
    }
}
if (!isset($messages["permissions"])) {
    echo '<li class="success"><dl><dt>Permissions</dt>';
    echo '<dd>Permissions are fine. All good.</dd>';
    echo '</dl></li>';
}
echo '</ol>'; ?>
        <hr class="clear" />
        <br />
        <input style="float:right;" type="submit" class="submit" value="ok" onclick="document.location = '<?php echo rtrim(FB_BASE,'/')."/"; ?>'" />
        <hr class="clear" />
    </div>
    <span class="credits"><a href="http://erikpoehler.com/folderblog/">Folderblog 4</a></span>

</body>