<?php
/**
 *
 * @package Folderbog 4.0
 * @author Erik Pöhler
 * @version 4.0
 * @see http://erikpoehler.com/folderblog/
 *
 */
class Users extends Frontend {

    public function saveUsers() {
        $this->backupData(); // save pre-state backup
        $users = $_POST["user"];
        $i = 0;
        foreach($users as $user => $values) {
            $this->xml->getElementsByTagName("user")->item($i)->setAttribute("nicename", $values["nicename"]);
            if ($values["new_password"] === $values["new_password_repeat"] && strlen($values["new_password_repeat"])>3) {
                if (sha1($values["old_password"]) === $this->xml->getElementsByTagName("user")->item($i)->getAttribute("password")) {
                    $this->xml->getElementsByTagName("user")->item($i)->setAttribute("password", sha1($values["new_password"]));
                }
            }
            $i++;
        }

        $this->saveData($this->xml->saveXML());
        header("Location: ".FB_BASE."/admin/#users");
        exit;
    }

    public function addUser() {
        // check if username already exists first
        $name = trim(strip_tags($_POST["username"]));

        $xpath = new DOMXPath($this->xml);
        $results = $xpath->query("/folderblog/users/user[@name=$name]");
        if (intval($results->length) !== 0) {
            // user already exists. stop here.
        } else {
            $this->backupData();

            $appendto = $this->xml->getElementsByTagName("users")->item(0);
            $newuser = $this->xml->createElement("user");
            $newuser->setAttribute("name",$name);
            $newuser->setAttribute("nicename",trim(strip_tags($_POST["nicename"])));
            $newuser->setAttribute("password",sha1($_POST["password"]));
            $appendto->appendChild($newuser);

            $this->saveData($this->xml->saveXML());
            header("Location: ".FB_BASE."/admin/#users");
            exit;
        }
    }

    public function deleteUser() {
        $name = trim(strip_tags($_POST["user"]));
        $xpath = new DOMXPath($this->xml);
        $results = $xpath->query("/folderblog/users/user[@name='$name']");
        if (intval($results->length) == 1) {
            $this->backupData();
            $this->xml->getElementsByTagName("users")->item(0)->removeChild($results->item(0));
            $this->saveData($this->xml->saveXML());
            if ($_SESSION["Folderblog".FB_UID]["loggedinUser"] == $name) {
                header("Location: ".FB_BASE."/logout/");
                exit;
            }
            header("Location: ".FB_BASE."/admin/#users");
        }
    }

    public function setSuperuserPassword() {
        // just checking...
        $dom = new DOMDocument();
        $dom->load($_SERVER["DOCUMENT_ROOT"].FB_BASE.'/lib/data/folderblog.xml');
        $this->backupData();

        // set the title
        $dom->getElementsByTagName("settings")->item(0)->getElementsByTagName("title")->item(0)->nodeValue = (string) $_POST["title"];
        $xpath = new DOMXPath($dom);

        // set the superuser password
        $results = $xpath->query("/folderblog/users/user[@name='superuser']");
        if ($results->length == 1) {
            $results->item(0)->setAttribute("password", sha1($_POST["superuser"]));
            $this->saveData($dom->saveXML());
        }

        // Update the htaccess file
        $str = "DirectoryIndex index.php
<IfModule mod_rewrite.c>
  SetEnv HTTP_MOD_REWRITE on
  RewriteEngine On
  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteCond %{REQUEST_FILENAME} !-d
  RewriteRule ^favicon.ico$ lib/img/favicon.ico
  RewriteRule ^image/(.*)x(.*)/folders/(.*) lib/php/phpthumb/phpThumb.php?src=".FB_BASE."/folders/$3&w=$1&h=$2&fltr[]=usm|80|0.5|3&q=95&zc=1
  RewriteRule ^thumb/(.*)x(.*)/folders/(.*) lib/php/phpthumb/phpThumb.php?src=".FB_BASE."/folders/$3&w=$1&h=$2&fltr[]=usm|80|0.5|3&q=75&zc=1
  RewriteRule ^(404|e|archive|setup|admin|logout|login|folders.*) index.php [L]
</IfModule>

<IfModule !mod_rewrite.c>
    SetEnv HTTP_MOD_REWRITE off
</IfModule>";
        if(file_put_contents($_SERVER["DOCUMENT_ROOT"].FB_BASE."/.htaccess", $str)) {
            header("Location: ".FB_BASE."/logout/");
            exit;
        } else {
            throw new FolderblogException("Couldn't update .htaccess rules. Check permissions.");
        }
    }
}