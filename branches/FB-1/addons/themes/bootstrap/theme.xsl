<?xml version="1.0" encoding="UTF-8"?>
<!-- 
Folderblog Bootstrap Theme
@since 2015-11-07
@see based on Bootstrap 4.0
@author Erik Pöhler <sayhello@erikpoehler.com>
 -->
<xsl:stylesheet 
    version="1.0" 
    xmlns="http://www.w3.org/1999/xhtml" 
    xmlns:html="http://www.w3.org/1999/xhtml" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:exsl="http://exslt.org/common" 
    extension-element-prefixes="exsl" 
    exclude-result-prefixes="exsl xsl html">
    
    <xsl:output encoding="UTF-8" indent="no" method="xml" omit-xml-declaration="yes"/>
    <xsl:strip-space elements="*"/>
    <xsl:param name="view"/>
    <xsl:param name="base"/>
    <xsl:param name="folder"/>
    <xsl:param name="element"/>
    <xsl:param name="page" />
    <xsl:variable name="current_theme" select="/folderblog/settings/theme"/>
    <xsl:variable name="theme" select="/folderblog/settings/themes/theme[@dirname=$current_theme]"/>
</xsl:stylesheet>