<?php
/**
 * error reporting, memory limits, etc.
 */
$strict = false;
ini_set("display_errors","On");
ini_set('memory_limit', '64M');
ini_set('upload_max_filesize', '1000M');
ini_set('post_max_size', '1000M');
if($strict) error_reporting(E_STRICT);
else error_reporting(E_ALL ^ E_NOTICE ^ E_STRICT); // since php 5.4 srtict errors are part of e_all

/**
 * set the base directory for installations that are not in document root
 * @var string FB_BASE
 */
if (!defined('FB_BASE')) {
	define('FB_BASE', str_replace($_SERVER["DOCUMENT_ROOT"],"",dirname(__FILE__)));
}

/**
 *  if you want to install more than one instance of Folderblog, use different UIDs here.
 *  this way Folderblog knows what session (and optionally what Memcache key to look for)
 */
if (!defined('FB_UID')) {
	define('FB_UID', 1);
}

/**
 * show profiling information or not
 */
if (!defined('FB_PROFILE')) {
    define('FB_PROFILE', "off");
}

/**
 *
 * if there appears to be no mod_rewrite, go to server.php
 */
if (getenv('HTTP_MOD_REWRITE') == "off") {
    header("Location: ".FB_BASE."/server.php");
    exit;
}