/**
 * @package Folderblog 4.0
 * @desc Javascript helper functions - ports for php programmers
 * @see http://phpjs.org/
 */

function explode(delimiter, string, limit) {

    if (arguments.length < 2 || typeof delimiter == 'undefined'
            || typeof string == 'undefined')
        return null;
    if (delimiter === '' || delimiter === false || delimiter === null)
        return false;
    if (typeof delimiter == 'function' || typeof delimiter == 'object'
            || typeof string == 'function' || typeof string == 'object') {
        return {
            0 : ''
        };
    }
    if (delimiter === true)
        delimiter = '1';

    // Here we go...
    delimiter += '';
    string += '';

    var s = string.split(delimiter);

    if (typeof limit === 'undefined')
        return s;

    // Support for limit
    if (limit === 0)
        limit = 1;

    // Positive limit
    if (limit > 0) {
        if (limit >= s.length)
            return s;
        return s.slice(0, limit - 1).concat(
                [ s.slice(limit - 1).join(delimiter) ]);
    }

    // Negative limit
    if (-limit >= s.length)
        return [];

    s.splice(s.length + limit);
    return s;
}

function trim(str, charlist) {
    // http://kevin.vanzonneveld.net
    // + original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // + improved by: mdsjack (http://www.mdsjack.bo.it)
    // + improved by: Alexander Ermolaev
    // (http://snippets.dzone.com/user/AlexanderErmolaev)
    // + input by: Erkekjetter
    // + improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // + input by: DxGx
    // + improved by: Steven Levithan (http://blog.stevenlevithan.com)
    // + tweaked by: Jack
    // + bugfixed by: Onno Marsman
    // * example 1: trim(' Kevin van Zonneveld ');
    // * returns 1: 'Kevin van Zonneveld'
    // * example 2: trim('Hello World', 'Hdle');
    // * returns 2: 'o Wor'
    // * example 3: trim(16, 1);
    // * returns 3: 6
    var whitespace, l = 0, i = 0;
    str += '';

    if (!charlist) {
        // default list
        whitespace = " \n\r\t\f\x0b\xa0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000";
    } else {
        // preg_quote custom list
        charlist += '';
        whitespace = charlist
                .replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '$1');
    }

    l = str.length;
    for (i = 0; i < l; i++) {
        if (whitespace.indexOf(str.charAt(i)) === -1) {
            str = str.substring(i);
            break;
        }
    }

    l = str.length;
    for (i = l - 1; i >= 0; i--) {
        if (whitespace.indexOf(str.charAt(i)) === -1) {
            str = str.substring(0, i + 1);
            break;
        }
    }

    return whitespace.indexOf(str.charAt(0)) === -1 ? str : '';
}

function parse_str(str, array) {
    // http://kevin.vanzonneveld.net
    // + original by: Cagri Ekin
    // + improved by: Michael White (http://getsprink.com)
    // + tweaked by: Jack
    // + bugfixed by: Onno Marsman
    // + reimplemented by: stag019
    // + bugfixed by: Brett Zamir (http://brett-zamir.me)
    // + bugfixed by: stag019
    // + input by: Dreamer
    // + bugfixed by: Brett Zamir (http://brett-zamir.me)
    // + bugfixed by: MIO_KODUKI (http://mio-koduki.blogspot.com/)
    // + input by: Zaide (http://zaidesthings.com/)
    // + input by: David Pesta (http://davidpesta.com/)
    // + input by: jeicquest
    // + improved by: Brett Zamir (http://brett-zamir.me)
    // % note 1: When no argument is specified, will put variables in global
    // scope.
    // % note 1: When a particular argument has been passed, and the returned
    // value is different parse_str of PHP. For example, a=b=c&d====c
    // * example 1: var arr = {};
    // * example 1: parse_str('first=foo&second=bar', arr);
    // * results 1: arr == { first: 'foo', second: 'bar' }
    // * example 2: var arr = {};
    // * example 2: parse_str('str_a=Jack+and+Jill+didn%27t+see+the+well.',
    // arr);
    // * results 2: arr == { str_a: "Jack and Jill didn't see the well." }
    // * example 3: var abc = {3:'a'};
    // * example 3: parse_str('abc[a][b]["c"]=def&abc[q]=t+5');
    // * results 3: JSON.stringify(abc) ===
    // '{"3":"a","a":{"b":{"c":"def"}},"q":"t 5"}';

    var strArr = String(str).replace(/^&/, '').replace(/&$/, '').split('&'), sal = strArr.length, i, j, ct, p, lastObj, obj, lastIter, undef, chr, tmp, key, value, postLeftBracketPos, keys, keysLen, fixStr = function(
            str) {
        return decodeURIComponent(str.replace(/\+/g, '%20'));
    };

    if (!array) {
        array = this.window;
    }

    for (i = 0; i < sal; i++) {
        tmp = strArr[i].split('=');
        key = fixStr(tmp[0]);
        value = (tmp.length < 2) ? '' : fixStr(tmp[1]);

        while (key.charAt(0) === ' ') {
            key = key.slice(1);
        }
        if (key.indexOf('\x00') > -1) {
            key = key.slice(0, key.indexOf('\x00'));
        }
        if (key && key.charAt(0) !== '[') {
            keys = [];
            postLeftBracketPos = 0;
            for (j = 0; j < key.length; j++) {
                if (key.charAt(j) === '[' && !postLeftBracketPos) {
                    postLeftBracketPos = j + 1;
                } else if (key.charAt(j) === ']') {
                    if (postLeftBracketPos) {
                        if (!keys.length) {
                            keys.push(key.slice(0, postLeftBracketPos - 1));
                        }
                        keys.push(key.substr(postLeftBracketPos, j
                                - postLeftBracketPos));
                        postLeftBracketPos = 0;
                        if (key.charAt(j + 1) !== '[') {
                            break;
                        }
                    }
                }
            }
            if (!keys.length) {
                keys = [ key ];
            }
            for (j = 0; j < keys[0].length; j++) {
                chr = keys[0].charAt(j);
                if (chr === ' ' || chr === '.' || chr === '[') {
                    keys[0] = keys[0].substr(0, j) + '_'
                            + keys[0].substr(j + 1);
                }
                if (chr === '[') {
                    break;
                }
            }

            obj = array;
            for (j = 0, keysLen = keys.length; j < keysLen; j++) {
                key = keys[j].replace(/^['"]/, '').replace(/['"]$/, '');
                lastIter = j !== keys.length - 1;
                lastObj = obj;
                if ((key !== '' && key !== ' ') || j === 0) {
                    if (obj[key] === undef) {
                        obj[key] = {};
                    }
                    obj = obj[key];
                } else { // To insert new dimension
                    ct = -1;
                    for (p in obj) {
                        if (obj.hasOwnProperty(p)) {
                            if (+p > ct && p.match(/^\d+$/g)) {
                                ct = +p;
                            }
                        }
                    }
                    key = ct + 1;
                }
            }
            lastObj[key] = value;
        }
    }
}

function parse_url(str, component) {
    // http://kevin.vanzonneveld.net
    // + original by: Steven Levithan (http://blog.stevenlevithan.com)
    // + reimplemented by: Brett Zamir (http://brett-zamir.me)
    // + input by: Lorenzo Pisani
    // + input by: Tony
    // + improved by: Brett Zamir (http://brett-zamir.me)
    // % note: Based on
    // http://stevenlevithan.com/demo/parseuri/js/assets/parseuri.js
    // % note: blog post at http://blog.stevenlevithan.com/archives/parseuri
    // % note: demo at
    // http://stevenlevithan.com/demo/parseuri/js/assets/parseuri.js
    // % note: Does not replace invalid characters with '_' as in PHP, nor does
    // it return false with
    // % note: a seriously malformed URL.
    // % note: Besides function name, is essentially the same as parseUri as
    // well as our allowing
    // % note: an extra slash after the scheme/protocol (to allow file:/// as in
    // PHP)
    // * example 1:
    // parse_url('http://username:password@hostname/path?arg=value#anchor');
    // * returns 1: {scheme: 'http', host: 'hostname', user: 'username', pass:
    // 'password', path: '/path', query: 'arg=value', fragment: 'anchor'}
    var key = [ 'source', 'scheme', 'authority', 'userInfo', 'user', 'pass',
            'host', 'port', 'relative', 'path', 'directory', 'file', 'query',
            'fragment' ], ini = (this.php_js && this.php_js.ini) || {}, mode = (ini['phpjs.parse_url.mode'] && ini['phpjs.parse_url.mode'].local_value)
            || 'php', parser = {
        php : /^(?:([^:\/?#]+):)?(?:\/\/()(?:(?:()(?:([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?))?()(?:(()(?:(?:[^?#\/]*\/)*)()(?:[^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
        strict : /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
        loose : /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/\/?)?((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
    // Added one optional slash to post-scheme to catch file:/// (should
    // restrict this)
    };

    var m = parser[mode].exec(str), uri = {}, i = 14;
    while (i--) {
        if (m[i]) {
            uri[key[i]] = m[i];
        }
    }

    if (component) {
        return uri[component.replace('PHP_URL_', '').toLowerCase()];
    }
    if (mode !== 'php') {
        var name = (ini['phpjs.parse_url.queryKey'] && ini['phpjs.parse_url.queryKey'].local_value)
                || 'queryKey';
        parser = /(?:^|&)([^&=]*)=?([^&]*)/g;
        uri[name] = {};
        uri[key[12]].replace(parser, function($0, $1, $2) {
            if ($1) {
                uri[name][$1] = $2;
            }
        });
    }
    delete uri.source;
    return uri;
}