<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet exclude-result-prefixes="xsl xsi html xsd" version="1.0" xmlns="http://www.w3.org/1999/xhtml" xmlns:html="http://www.w3.org/1999/xhtml" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output encoding="UTF-8" indent="no" method="xml" omit-xml-declaration="yes"/>
    <xsl:strip-space elements="*"/>
    
    <xsl:template name="format_date">
        <xsl:param name="date" />
        <xsl:value-of select="concat( substring($date, 7, 2),'.',substring($date, 5, 2), '.', substring($date, 1, 4), ', ', substring($date, 9, 2),':',substring($date, 11, 2),'h' )" />
    </xsl:template>
    
    <xsl:template name="pagination">
        <xsl:param name="folder" />
        <xsl:param name="pageNumber"/>
        <xsl:param name="recordsPerPage"/>
        <xsl:param name="numberOfRecords"/>

        <div class="pagination" id="pagination">
            <div class="wrapper">
                <xsl:if test="$pageNumber &gt; 1">
                    <a class="first" title="First" data-page="1" href="#browser">⇤<span><xsl:text> </xsl:text></span></a>
                    <a class="prev" title="Previous" data-page="{$pageNumber - 1}" href="#browser">←<span><xsl:text> </xsl:text></span></a>
                </xsl:if>
                
                <xsl:call-template name="for.loop">
                    <xsl:with-param name="i"><xsl:value-of select="$pageNumber - 3" /></xsl:with-param>
                    <xsl:with-param name="page" select="$pageNumber"/>
                    <xsl:with-param name="count" select="ceiling(count(//folder[path=$folder]//element) div $recordsPerPage)"/>
                    <xsl:with-param name="linklimit">6</xsl:with-param>
                </xsl:call-template>
                
                <xsl:if test="(($pageNumber) * $recordsPerPage) &lt; ($numberOfRecords)">
                    <a class="next" title="Next" data-page="{$pageNumber + 1}" href="#browser">→<span><xsl:text> </xsl:text></span></a>
                    <a class="last" title="Last" data-page="{ceiling(count(//folder[path=$folder]//element) div $recordsPerPage)}" href="#browser">⇥<span><xsl:text> </xsl:text></span></a>
                </xsl:if>
            </div>
        </div>
    </xsl:template>

    <xsl:template name="for.loop">
        <xsl:param name="i"/>
        <xsl:param name="count"/>
        <xsl:param name="page"/>
        <xsl:param name="linklimit" />
        
        <xsl:if test="$i &lt;= $count and $i > 0">
            <span>
                <xsl:if test="$page != $i">
                    <a data-page="{$i}" href="#browser"><xsl:value-of select="$i"/><span><xsl:text> </xsl:text></span></a>
                </xsl:if>
                <xsl:if test="$page = $i">
                    <a href="#browser" data-page="{$i}" class="active"><xsl:value-of select="$i"/><span><xsl:text> </xsl:text></span></a>
                </xsl:if>
            </span>
        </xsl:if>

        <xsl:if test="$i &lt;= $count and $linklimit &gt; 0">
            <xsl:call-template name="for.loop">
                <xsl:with-param name="i">
                    <xsl:value-of select="$i + 1"/>
                </xsl:with-param>
                <xsl:with-param name="count">
                    <xsl:value-of select="$count"/>
                </xsl:with-param>
                <xsl:with-param name="page">
                    <xsl:value-of select="$page"/>
                </xsl:with-param>
                <xsl:with-param name="linklimit">
                    <xsl:value-of select="$linklimit -1"/>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>