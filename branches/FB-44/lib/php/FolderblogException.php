<?php
/**
 *
 * @package Folderbog 4.0
 * @author Erik Pöhler
 * @version 4.0
 * @see http://erikpoehler.com/folderblog/
 *
 */
class FolderblogException extends Exception {

    public function displayError() {
        $out = '<!DOCTYPE html>
        <html xmlns="http://www.w3.org/1999/xhtml" lang="en">
        <head>
            <title>Fatal Error</title>
            <link href="/lib/img/favicon.ico" rel="icon" type="image/x-icon">
            <link rel="stylesheet" type="text/css" href="/lib/css/styles.css">
        </head>
        <body class="exception jquery">
            <div class="box" style="margin:1em;">
            	<img style="vertical-align:-3px;" src="'.FB_BASE.'/lib/img/error.gif" />
                <strong>'.$this->getMessage().'</strong>
                <hr class="separate" />
        		<pre>'.str_replace($_SERVER["DOCUMENT_ROOT"],'',$this->getTraceAsString()).'</pre>
            </div>
            <span class="credits"><a href="http://erikpoehler.com/folderblog/">Folderblog 4.0</a></span>
            <script type="text/javascript">var base = "'.FB_BASE.'";</script>
            <script type="text/javascript" src="'.FB_BASE.'/lib/js/jquery.js"></script>
            <script type="text/javascript" src="'.FB_BASE.'/lib/js/jquery.field.js"></script>
            <script type="text/javascript" src="'.FB_BASE.'/lib/js/jquery.alphanumeric.js"></script>
            <script type="text/javascript" src="'.FB_BASE.'/lib/js/folderblog.js"></script>
        </body>
        </html>';
        echo $out;
    }
}