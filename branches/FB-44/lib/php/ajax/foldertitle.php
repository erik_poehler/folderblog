<?php
/**
 *
 * @package Folderbog 4.0
 * @author Erik Pöhler
 * @version 4.0
 * @see http://erikpoehler.com/folderblog/
 * @desc AJAX Helper to generate clean folder and/or filenames
 *
 */

require_once "../../../config.php";

function __autoload($cn) {
	if (is_file($_SERVER["DOCUMENT_ROOT"] . FB_BASE . "/lib/php/$cn.php")) require_once $_SERVER["DOCUMENT_ROOT"].FB_BASE."/lib/php/$cn.php";
}
session_start();
$response = new AjaxResponse();
session_write_close();
if (isset($_GET["title"]) && strlen(trim($_GET["title"]))>0) {
	$orig = trim(strip_tags($_GET["title"]));
	$fn = StringFunctions::stringToFilename($orig);
	$response->setSuccess(true)->setData($fn);
} else {
	$response->setMessage("no string received to normalize");
}
header("Content-Type: text/xml; charset=utf8");
echo $response->createResponse();
exit;