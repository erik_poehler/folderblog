<?php
/**
 *
 * @package Folderbog 4.0
 * @author Erik Pöhler
 * @version 4.0
 * @see http://erikpoehler.com/folderblog/
 *
 */
class FilesystemFunctions {
    /**
     * Physical deletion of a media folder and its contents.
     * @todo Write a working PHP implementation of recursive folder deletion for compatibility
     * @throws FolderblogException
     * @return void
     */
    public function rrmdir($dir) {
        if(SystemFunctions::execEnabled()) {
            $command = 'rm -rf "'.$dir.'"';
            exec($command, $return, $code);
            if ($code !== 0) {
                throw new FolderblogException(sprintf("Fuck! Something went wrong: %s (%s)", join('<br />', $return), $code));
            }
        } else {
            throw new FolderblogException("Deleting folders with native php is not yet implemented.");
        }
    }

    /**
     *
     * Transform bytes into a human readable filesize with unit
     * @param integer $bytes size of the file in bytes
     * @param integer $precision after-comma precision
     * @return string
     */
    public static function bytesToSize($bytes, $precision = 2) {
        $kilobyte = 1024;
        $megabyte = $kilobyte * 1024;
        $gigabyte = $megabyte * 1024;
        $terabyte = $gigabyte * 1024;

        if (($bytes >= 0) && ($bytes < $kilobyte)) {
            return $bytes . ' B';

        } elseif (($bytes >= $kilobyte) && ($bytes < $megabyte)) {
            return round($bytes / $kilobyte, $precision) . ' KB';

        } elseif (($bytes >= $megabyte) && ($bytes < $gigabyte)) {
            return round($bytes / $megabyte, $precision) . ' MB';

        } elseif (($bytes >= $gigabyte) && ($bytes < $terabyte)) {
            return round($bytes / $gigabyte, $precision) . ' GB';

        } elseif ($bytes >= $terabyte) {
            return round($bytes / $terabyte, $precision) . ' TB';
        } else {
            return $bytes . ' B';
        }
    }

    /**
     * @desc recursive glob function
     * @param string $pattern
     * @param const $flags
     * @param string $path
     * @return array
     */
    public static function rglob($pattern, $flags = 0, $path = '') {
        if (!$path && ($dir = dirname($pattern)) != '.') {
            if ($dir == '\\' || $dir == '/') $dir = '';
            return self::rglob(basename($pattern), $flags, $dir . '/');
        }
        $paths = glob($path . '*', GLOB_ONLYDIR | GLOB_NOSORT);
        $files = glob($path . $pattern, $flags);
        foreach ($paths as $p) $files = array_merge($files, self::rglob($pattern, $flags, $p . '/'));
        return $files;
    }
}