<?php
/**
 *
 * @package Folderbog 4.0
 * @author Dominik Fettel, Erik Pöhler
 * @version 4.0
 * @see http://erikpoehler.com/folderblog/
 *
 */
class DebugUtilities {
	private $durationStart;
	private $durationEnd;
	public static function startOutputBuffer() {
		ob_start();
	}
	public static function stopOutputBuffer($height="300px", $width="100%", $id="") {
		self::showInTextarea(ob_get_clean(), $height, $width, $id);
	}
	public static function print_rInTextarea($input, $height="300px", $width="100%", $id="") {
		$idXHTML = "";
		if($id != "") $idXHTML = ' id="'.$id.'" ';
		echo "<textarea $idXHTML style=\"width:$width;height:$height\">";
		if(ob_start()) {
			print_r($input);
			$res = ob_get_clean();
			echo htmlentities($res);
		}
		echo "</textarea>";
	}
	public static function showInTextarea($input, $height="300px", $width="100%", $id="", $name="") {
		if($id != "") $idXHTML = ' id="'.$id.'" ';
		if($name != "") $idXHTML = ' name="'.$name.'" ';
		echo "<textarea $idXHTML style=\"width:$width;height:$height\">".htmlentities($input)."</textarea>";
	}
	public static function showIframe($url, $height="300px", $width="100%", $id="") {
		if($id != "") $idXHTML = ' id="'.$id.'" ';
		echo '<iframe src="'.$url.'" height="'.$height.'" width="'.$width.'" '.$idXHTML.' ></iframe>';
	}
	public static function print_rInPreTags($input) {
		echo "<pre>";
		print_r($input);
		echo "</pre>";
	}
	public static function showInPre($input) {
		echo "<pre>$input</pre>";
	}
	public static function showSession() {
		self::print_rInPreTags($_SESSION);
	}
	public static function showPost() {
		self::printInH1("\$_POST");
		self::print_rInPreTags($_POST);
	}
	public static function showGet() {
		self::print_rInPreTags($_GET);
	}
	public static function showCookies() {
		self::print_rInPreTags($_COOKIE);
	}
	public static function showRequest() {
		self::print_rInPreTags($_REQUEST);
	}
	public static function showServer() {
		self::print_rInPreTags($_SERVER);
	}
	public static function showTextFile($filename, $height="300px", $width="100%", $id="") {
		$input = file_get_contents($filename);
		self::showInTextarea($input, $height, $width, $id);
	}
	public static function showEnvironment() {
		foreach(array("Session","Post","Get","Cookies","Server") as $part) {
			echo "<h1>$part</h1>";
			eval ("self::show$part();");
		}
	}
	/**
	 * Show relevant configuring
	 *
	 * @see http://de2.php.net/manual/de/ref.errorfunc.php
	 *
	 */
	public static function showErrorReportingConfig() {
		echo ini_get("display_errors");
		echo error_reporting();
	}
	/**
	 * Show relevant configuring
	 *
	 * @see http://de2.php.net/manual/de/ref.errorfunc.php
	 *
	 */
	public static function setErrorReportingOnAnyway($strict=true) {
		ini_set("display_errors","On");
		if($strict) error_reporting(E_STRICT);
		else error_reporting(E_ALL);
	}
	/**
	 * Checks HTTP-Request if there is a paramter called $key. If it reads
	 * a "true" or "1" it's interpreded as there should be a debug mode.
	 *
	 * This is registered as Session-Configuration.
	 *
	 * If the parameter reads a "false" or a "1" it's interpreded, that there should
	 * be no session mode and that's registered in Session-Variables too.
	 *
	 * @param string $key
	 * @return boolean isInDebugMode
	 */
	public static function isDebugMode($key="DEBUG") {
		if(!is_array($_SESSION)) @session_start();
		if(is_array($_SESSION)) {
			if(array_key_exists($key, $_REQUEST)) {
				$_SESSION[$key] = self::requestParamMeansTrue($key);
			}
			return $_SESSION[$key];
		} else {
			return self::requestParamMeansTrue($key);
		}
	}
	private static function requestParamMeansTrue($key) {
		return eregi("TRUE|1|on", $_REQUEST[$key]);
	}
	public static function arrayToTable(array $input) {
		$csv = new CSV();
		$csv->setData($input);
	}
	public static function printArrayAsList(array $array, $numbered = true) {
		if($numbered) $tag = "ol";
		else $tag = "ul";
		echo "<$tag><li>".implode("</li><li>", $array)."</li></$tag>";
	}
	public static function showFileInTextarea($filename, $height="300px", $width="100%") {
		self::showInTextarea(file_get_contents($filename), $height, $width);
	}
	public function startDuration() {
		$this->durationStart = microtime(true);
	}
	public function endDuration() {
		$this->durationEnd = microtime(true);
	}
	public function endDurationAndPrint() {
		$this->endDuration();
		$this->showDurationStatistics();
	}
	public function showDurationStatistics() {

		$out = "Start: ".$this->getMicrotimeLine($this->durationStart)."\n";
		$out .= "End: ".$this->getMicrotimeLine($this->durationEnd)."\n";
		$out .= "Rendered in: ".number_format(($this->durationEnd - $this->durationStart)*1000,1) . " ms\n";
		return $out;
	}
	public static function backtraceInPreTags() {
		if(ob_start()) {
			debug_print_backtrace();
			self::print_rInPreTags(ob_get_clean());
		}
	}
	public static function backtraceShortInPreTags() {
		foreach(debug_backtrace() as $data) {
			$c .= $data["file"].":".$data["line"]."\t".$data["object"].":".$data["class"]."::".$data["function"]."(".implode(",", $data["args"]).")\n";
		}
		self::print_rInPreTags($c);
	}
	private function getMicrotimeLine($microtime) {
		return date("H:i:s", intval($microtime)).", ".substr($microtime, 10);
	}
	public static function showMatrixAsTable($matrix, $addStandardStylesheet=true) {
		$t = new CSV();
		$t->setData($matrix);
		echo $t->show($addStandardStylesheet);
	}
	public static function printInH1($input) {
		echo "<h1>$input</h1>";
	}
	public static function printInH2($input) {
		echo "<h2>$input</h2>";
	}
	public static function printInH3($input) {
		echo "<h3>$input</h3>";
	}
	public static function printInH4($input) {
		echo "<h4>$input</h4>";
	}
	public static function hashArrayAsLine(array $array) {
		$parts = array();
		foreach($array as $k=>$v) {
			$parts[] = "<strong>$k:</strong>$v";
		}
		echo implode(" | ",$parts)."<hr>";
	}
	public static function showDOMDocInIframe($exp, $height="300px", $width="100%", $id="", $filename="") {
		if($filename=="") {
			$filename = "temp$id.xml";
			$url = $filename;
		} else {
			$url = dirFunctions::cutDocRoot($filename);
		}
		$doc = DOMDocumentCreator::create($exp);
		$doc->save($filename);
		self::showIframe($url."?tmp=".uniqid(time()),$height, $width, $id);
	}
	public static function printCMD($cmd, $height="300px", $width="100%", $id="", $name="") {
		if(SystemFunctions::execEnabled()) {
		    exec($cmd, $m);
		    $height = $height.";background-color:Black;color:White;font-family:Courier";
		    self::print_rInTextarea($cmd.chr(13).implode(chr(13), $m),$height,$width, $id);
		} else {
			throw new Exception("exec() disabled.");
		}
	}
	public static function printBacktrace($inTextArea=false, $height="300px", $width="100%", $id="") {
		ob_start();
		debug_print_backtrace();
		$res = ob_get_clean();
		if($inTextArea) self::print_rInTextarea($res, $height, $width, $id);
		else self::print_rInPreTags($res);
	}
	public static function debugXSLT(DOMDocument $xml, DOMDocument $xsl, array $params=array(), $outPutIsXSL=false) {
//		self::printInH1("XML &amp; XSL");
		self::showDOMDocInIframe($xml, "300px","48%","xml");
		self::showDOMDocInIframe($xsl, "300px","48%","xsl");
		self::print_rInTextarea($params, "100px");
		$xsl = new xslt($xml, $xsl, $params, false);
		if($outPutIsXSL) {
			$xml = $xsl->process();
			self::showDOMDocInIframe($xml, "300px");
			self::printInH1("Result as HTML");
			$xsl->result->saveHTMLFile("temp.html");
		} else {
			$out = $xsl->transform();
			file_put_contents("temp.html", $out);
		}
		self::showIframe("temp.html", "300px");
	}
}
?>